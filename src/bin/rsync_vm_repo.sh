#!/bin/bash
rsync -aP src/* root@vuln-vms:/var/www
ssh root@vuln-vms chown www-data:www-data -R  /var/www 
ssh root@vuln-vms sudo -u www-data composer install --working-dir=/var/www/vuln/main
