<html>
<head>
	<title>XSS contact form</title>
</head>
<body>
	<h1>Send a message to me!</h1>
	<form action="xss.php" method="POST">
	Your Name:
		<input name="name" type="text" size="50">
	<br>
	</br>
	Email:
		<input name="email" type="email" size="50">
	<br>
	</br>
	Message:
	<br>
	</br>
	<textarea id="message" rows="5" cols="33" placeholder="Send message..."></textarea>
	<br>
	</br>
	<input type="submit" value="Send!">
	</form>
	<hr>
	<h2>
		<?php
			$name = $_POST['name'];
			$email = $_POST['email'];
			$message = $_POST['message'];
			if ($_POST) {
				echo "$name, your message was sent!";
				echo "we have sent a copy of the message to $email";
			}
		?>
	</h2>
</body>
</html>
