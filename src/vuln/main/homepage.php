<?php
  require 'vendor/autoload.php'; 
  session_start();
  $client = new MongoDB\Client("mongodb://localhost:27017");
  $collection = $client->blog->admin;
  $user = $collection->find(array("username"=>$_POST['username'],"password"=>$_POST['password']));
  
  foreach($user as $obj) {
    $_SESSION['username'] = $obj['username'];
    header('location: home1.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <link rel="mask-icon" type="" href="https://static.codepen.io/assets/favicon/logo-pin-8f3771b1072e3c38bd662872f6b673a722f4b3ca2421637d5596661b4e2132cc.svg" color="#111" />
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <title>Mango | Sweet & Juicy</title>
    <style>
      * {
        box-sizing: border-box;
      }

      body {
        font-family: 'Rubik', sans-serif;
        margin: 0;
        padding: 0;
      }

      .container {
        display: flex;
        height: 100vh;
      }

      .left-section {
        overflow: hidden;
        display: flex;
        flex-wrap: wrap;
        flex-direction: column;
        justify-content: center;
        -webkit-animation-name: left-section;
        animation-name: left-section;
        -webkit-animation-duration: 1s;
        animation-duration: 1s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation-delay: 1s;
        animation-delay: 1s;
      }

      .right-section {
        flex: 1;
        background: linear-gradient(to right, #f50629 0%, #fd9d08 100%);
        transition: 1s;
        background-image: url(/mango.jpg);
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
      }

      .header > h1 {
        margin: 0;
        color: #f50629;
      }

      .header > h4 {
        margin-top: 10px;
        font-weight: normal;
        font-size: 15px;
        color: rgba(0, 0, 0, 0.4);
      }

      .form {
        max-width: 80%;
        display: flex;
        flex-direction: column;
      }

      .form > p {
        text-align: right;
      }

      .form > p > a {
        color: #000;
        font-size: 14px;
      }

      .form-field {
        height: 46px;
        padding: 0 16px;
        border: 2px solid #ddd;
        border-radius: 4px;
        font-family: 'Rubik', sans-serif;
        outline: 0;
        transition: .2s;
        margin-top: 20px;
      }

      .form-field:focus {
        border-color: #0f7ef1;
      }

      .form > button {
        padding: 12px 10px;
        border: 0;
        background: linear-gradient(to right, #f50629 0%, #fd9d08 100%);
        border-radius: 3px;
        margin-top: 10px;
        color: #fff;
        letter-spacing: 1px;
        font-family: 'Rubik', sans-serif;
      }

      .animation {
        -webkit-animation-name: move;
        animation-name: move;
        -webkit-animation-duration: .4s;
        animation-duration: .4s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation-delay: 2s;
        animation-delay: 2s;
      }

      .a1 {
        -webkit-animation-delay: 2s;
        animation-delay: 2s;
      }

      .a2 {
        -webkit-animation-delay: 2.1s;
        animation-delay: 2.1s;
      }

      .a3 {
        -webkit-animation-delay: 2.2s;
        animation-delay: 2.2s;
      }

      .a4 {
        -webkit-animation-delay: 2.3s;
        animation-delay: 2.3s;
      }

      .a5 {
        -webkit-animation-delay: 2.4s;
        animation-delay: 2.4s;
      }

      .a6 {
        -webkit-animation-delay: 2.5s;
        animation-delay: 2.5s;
      }

      @keyframes move {
        0% {
          opacity: 0;
          visibility: hidden;
          -webkit-transform: translateY(-40px);
          transform: translateY(-40px);
        }
        100% {
          opacity: 1;
          visibility: visible;
          -webkit-transform: translateY(0);
          transform: translateY(0);
        }
      }
      @keyframes left-section {
        0% {
          opacity: 0;
          width: 0;
        }
        100% {
          opacity: 1;
          padding: 20px 40px;
          width: 440px;
        }
      }
    </style>
    <script>
      window.console = window.console || function(t) {};
    </script>
    <script>
      if (document.location.search.match(/type=embed/gi)) {
        window.parent.postMessage("resize", "*");
      }
    </script>
  </head>
  <body translate="no">
    <div class="container">
    <div class="left-section">
    <div class="header">
    <h4 class="animation a2">Log in admin panel</h4>
    </div>
    <form action="" method="POST">
    <div class="form">
    <input type="username" name="username" class="form-field animation a3" placeholder="Username">
    <input type="password" name="password" class="form-field animation a4" placeholder="Password">
    <p class="animation a5"><a href="#">Forgot Password</a></p>
    <button class="animation a6" value="login" name="login">LOGIN</button>
    </form>
    </div>
    </div>
    <div class="right-section"></div>
    </div>
  </body>
</html>