<?php

setcookie("logs", "content_logs");

class Logger {
    function __construct($filename, $content) {
        $this->filename = $filename . ".log";
        $this->content = $content;
    }
    
    function __destruct() {
        file_put_contents($this->filename, $this->content);
    }
}

unserialize(base64_decode($_COOKIE["logs"]));
;?>
