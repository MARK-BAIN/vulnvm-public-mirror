<?php
    // /src/vuln/deserialization/deserialize.php
    class Course { // definicja klasy o nazwie Kurs
        public $name; // atrybut klasy Kurs
        
        function __construct($name) { // Konstruktor klasy Kurs
            $this->name = $name; // przypisanie do atrybutu $name zawartości podanej podczas tworzenia obiektu
        }
    }
    $obj = new Course("Test"); // Tworzenie obiektu korzystając ze zdefiniowanego konstruktora
    $serobj = serialize($obj);
    echo "$serobj\n\n"; // Serializacja obiektu do ciągu bajtów oraz jego wyświetlenie
    var_dump(unserialize($serobj)); // a to niebezpieczne użycie funkcji unserialize, var_dump pozwala dowiedzieć się trochę więcej o danych deserializowanych
?>