import pickle, datetime # Importowanie potrzebnych modułów

now = datetime.datetime.now() # Zwracanie obecnej daty
serialized_object = pickle.dumps(now) # Serializowanie obiektu

print(pickle) # Wyświetlenie zserializowanego obiektu
print(serialized_object)
unserialized_object = pickle.loads(serialized_object) # Deserializacja obiektu

print(unserialized_object) # Wyświetlenie zdeserializowanego obiektu