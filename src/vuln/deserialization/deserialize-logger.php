<?php
// /src/vuln/deserialization/deserialize-logger.php
class Logger {
    function __construct($filename, $content) {
        $this->filename = $filename . ".log";
        $this->content = $content;
    }
    
    function __destruct() {
        file_put_contents($this->filename, $this->content);
    }
}

unserialize($_GET['data']);
?>
