from flask import Flask, request
from jinja2 import Template
app = Flask(__name__)
@app.route("/")
def index():
	name = request.args.get('name', 'guest')
	t = Template("Hi, " + name + " try exploit SSTI!")
	return t.render()
if __name__== "__main__":
	app.run()
