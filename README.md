# VulnVM project

This project was created to serve as educational materials for __Advanced web apps’ security__ courses organized inclass, online or in any other form, under a proprietary license.
> © Copyrights MrCertified sp. z o.o., Warsaw Poland

The project is currently hosted under: http://vuln.vms.perkmylife.com

- To setup the local environment and kick-start the use of this project, please read:
	- [01-Introduction-to-the-training-and-related-technology](./docs/A-fundamentals-web-apps-sec/en/01-Introduction-to-the-training-and-related-technology.md)
- To read more about the project, please read:
	- [00-About-the-course-and-the-instructors.md](./docs/A-fundamentals-web-apps-sec/en/00-About-the-course-and-the-instructors.md)

##### Available language versions:
At the moment there are only 2 language versions available: English (lang=en), Polish (lang=pl).
You can use any of the above versions. They shall be in sync. However, we treat the English version as the master one.


## The materials are split into 2 blocks and 20+1 sections:
The blocks are divided into 2 blocks: fundamental concepts and advanced concepts of web apps' security:

- [A. Fundamentals of web apps’ client-side security](./docs/A-fundamentals-web-apps-sec)
- [B. Advanced web apps’ security](./docs/B-advanced-web-apps-sec)

The 20+1 sections look as follows:
```
├── A-fundamentals-web-apps-sec
│   ├── {lang}
│   │   ├── 00-About-the-course-and-the-instructors.md
│   │   ├── 01-Introduction-to-the-training-and-related-technology.md
│   │   ├── 02-Cross-Site-Scripting---the-most-important-vulnerability-of-the-client-side-world.md
│   │   ├── 03-Other-client-side-attacks.md
│   │   ├── 04-Security-issues-of-HTML5-API-elements.md
│   │   ├── 05-Content-Security-Policy---a-remedy-to-security-problems-or-implementational-challenges.md
│   │   ├── 06-Ways-to-enhance-front-end-security.md
│   │   ├── 07-JS-libraries-(jQuery,-Angular,-React,-Knockout).md
│   │   ├── 08-Bug-bounty.md
│   │   ├── 09-Wrap-up-hacking-exercise---exploiting-a-client-side-vulnerability-to-extract-data-from-other-domains.md
│   │   └── 10-Summary:-a-brief-summary-of-the-attack-methods-and-the-client-side-defence-techniques-discussed-during-the-training.md
└── B-advanced-web-apps-sec
    ├── {lang}
    │   ├── 11-Server-Side-Template-Injection.md
    │   ├── 12-Data-Deserialization-Errors.md
    │   ├── 13-XML-world-and-security-flaws,-Server-Side-Request-Forgery.md
    │   ├── 14-Filters-in-Applications-and-Web-Application-Firewall-(WAF).md
    │   ├── 15-Cryptography.md
    │   ├── 16-Defense-mechanisms-in-web-applications.md
    │   ├── 17-Other-attacks-on-web-applications.md
    │   ├── 18-Exercise-summarizing-the-training.md
    │   └── 19-Training-summary---a-brief-summary-of-all-discussed-attack-and-defense-methods.md
```

## Ports, sockets, Services, Web appliactions
Within this repo, there are multiple independent web applications, which require different ports and services. Below is a short summary of the configuration we use:

| Port | app & source code | APP_DIR on VulnVM | Service running the app | Comment |
| --- | --- | --- | --- | --- |
| :80 | [main](/src/vuln/main) | /var/www/vuln/main | Apache2 | VirtualHost 001-main.conf |
| :81 | [ssti](/src/vuln/ssti) | /var/www/vuln/ssti | python3 flask | Systemctl ssti.service |
| :82 | [deserialization](/src/vuln/deserialization) | /var/www/vuln/deserialization | Apache2 | VirtualHost 003-deserialization.conf |
| :83 | [xxe](/src/vuln/xxe) | /var/www/vuln/xxe | Apache2 | VirtualHost 004-xxe.conf |
| localhost:91 | [ssrf](/src/vuln/ssrf) | /var/www/vuln/ssrf | Apache2 | VirtualHost 005-ssrf-local.conf |
| localhost:100 | [ssti](/src/vuln/advanced_summary_app) | /var/www/vuln/advanced_summary_app | python3 flask | Systemctl flask.service |
| :8080 | [mutillidae](https://github.com/webpwnized/mutillidae.git) | docker ps -a | Docker | Runs as a docker container owasp17 |
| :8081 | [advanced_summary_app](/src/vuln/advanced_summary_app) | /var/www/vuln/advanced_summary_app | Apache2 | VirtualHost 006-advanced-summary.conf |
| :8123 | [crypto](/src/vuln/crypto/var/www) | /var/www/vuln/crypto/var/www | Apache2 | VirtualHost 007-cryptography.conf |

## Exercises index:
- [A. Fundamentals of web apps’ client-side security](./docs/A-fundamentals-web-apps-sec)
	- 01-1 Accessing publically available VulnVM image | [details](./docs/A-fundamentals-web-apps-sec/en/01-Introduction-to-the-training-and-related-technology.md)
		- http://vuln.vms.perkmylife.com/
	- 01-2 Downloading and installing the VulnVM .ova image | [details](./docs/A-fundamentals-web-apps-sec/en/01-Introduction-to-the-training-and-related-technology.md)
	- 01-3 Building the VulnVM on your own | [details](./docs/A-fundamentals-web-apps-sec/en/01-Introduction-to-the-training-and-related-technology.md)
	- 02-1 Reflected XSS | [details](./docs/A-fundamentals-web-apps-sec/en/02-Cross-Site-Scripting---the-most-important-vulnerability-of-the-client-side-world.md)
		- http://vuln.vms.perkmylife.com/xss.php | /src/vuln/xss.php
	- 02-2 Stored XSS | [details](./docs/A-fundamentals-web-apps-sec/en/02-Cross-Site-Scripting---the-most-important-vulnerability-of-the-client-side-world.md)
		- http://vuln.vms.perkmylife.com:8080/mutillidae/index.php?page=add-to-your-blog.php
	- 02-3 Reflected XSS | [details](./docs/A-fundamentals-web-apps-sec/en/02-Cross-Site-Scripting---the-most-important-vulnerability-of-the-client-side-world.md)
		- http://vuln.vms.perkmylife.com:8080/mutillidae/index.php?page=dns-lookup.php
	- 03-1 CSRF - exercise | [details](./docs/A-fundamentals-web-apps-sec/en/03-Other-client-side-attacks.md)
		- http://vuln.vms.perkmylife.com:8080/mutillidae/index.php?page=add-to-your-blog.php | http://vuln.vms.perkmylife.com/csrf-exploit.php
	- 03-2 Clickjacking - exercise | [details](./docs/A-fundamentals-web-apps-sec/en/03-Other-client-side-attacks.md)
		- http://vuln.vms.perkmylife.com:8080/mutillidae/index.php?page=framing.php
	- 05-1 Bypassing CSP | [details](./docs/A-fundamentals-web-apps-sec/en/05-Content-Security-Policy---a-remedy-to-security-problems-or-implementational-challenges.md)
		- https://chrome.google.com/webstore/detail/csp-evaluator/fjohamlofnakbnbfjkohkbdigoodcejf?hl=en
	- 09-1 Wrap-up Exercise | [details](./docs/A-fundamentals-web-apps-sec/en/09-Wrap-up-hacking-exercise---exploiting-a-client-side-vulnerability-to-extract-data-from-other-domains.md)
		- http://vuln.vms.perkmylife.com:8080/mutillidae/index.php?page=user-poll.php

- [B. Advanced web apps’ security](./docs/B-advanced-web-apps-sec)
	- 11-1 SSTI Exercise | [details](./docs/B-advanced-web-apps-sec/en/11-Server-Side-Template-Injection.md)
		- http://vuln.vms.perkmylife.com:81
	- 12-1 Deserialization in PHP | [details](./docs/B-advanced-web-apps-sec/en/12-Data-Deserialization-Errors.md)
		- http://vuln.vms.perkmylife.com:82
	- 12-2 Deserialization in Python | [details](./docs/B-advanced-web-apps-sec/en/12-Data-Deserialization-Errors.md)
		- /src/vuln/deserialize.py & /src/vuln/deserialize2.py
	- 13-1 XXE + SSRF Exercise | [details](./docs/B-advanced-web-apps-sec/en/13-XML-world-and-security-flaws,-Server-Side-Request-Forgery.md)
		- http://vuln.vms.perkmylife.com:83
	- 14-1 Bypassing app & WAF filters | [details](./docs/B-advanced-web-apps-sec/en/14-Filters-in-Applications-and-Web-Application-Firewall-(WAF).md)
	- 15-1 Padding Oracle/bit flipping attack | [details](./docs/B-advanced-web-apps-sec/en/15-Cryptography.md)
	- 16-1 Log analysis | [details](./docs/B-advanced-web-apps-sec/en/16-Defense-mechanisms-in-web-applications.md)
	- 17-1 ZIP symlinks attack | [details](./docs/B-advanced-web-apps-sec/en/17-Other-attacks-on-web-applications.md)
		- http://vuln.vms.perkmylife.com/zip.php
	- 17-2 MongoDB NoSQL Injection attack | [details](./docs/B-advanced-web-apps-sec/en/17-Other-attacks-on-web-applications.md)
		- http://vuln.vms.perkmylife.com/homepage.php
	- 18-1 Wrap-up exercise | [details](./docs/B-advanced-web-apps-sec/pl/18-Exercise-summarizing-the-training.md)
		- http://vuln.vms.perkmylife.com:8081/

## Slides:
If you prefer you can find corresponding course materials in a form of slide decks:

- English (en)
	- [A. Fundamentals of web apps’ client-side security](https://docs.google.com/presentation/d/16wPMvBQZIEwek5U_KqcPoK5xOs8-ytBZjchARUv30SQ)
	- [B. Advanced web apps’ security](https://docs.google.com/presentation/d/1JFePvr_hpr3lj1OCeGN9rKBjmela9dYoYUGVVFYo7NU)
- Polish (pl)
	- [A. Fundamentals of web apps’ client-side security](https://docs.google.com/presentation/d/18JJuA3C8myruC-uNPgb5bXFU6YRVyOOmtX1bXbBWmJw)
	- [B. Advanced web apps’ security](https://docs.google.com/presentation/d/1Mme5m7u0E5yEwt7NR9XpC1WeSJylIDnMm7jUn8OTVfg)

Please note, that although slides are useful for lecture-style theory learning or teaching, the markdowns available in the [/docs](./docs) folder are the recommended way of learning practical concepts.