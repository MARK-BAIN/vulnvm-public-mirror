# Filtry w aplikacjach i firewalle w aplikacjach webowych

## Filtry w aplikacjach
Filtry w aplikacjach to dość różnorodny temat ze względu na takie czynniki jak funkcja aplikacji, czynniki ryzyka, wymagane zabezpieczenia, technologia w której jest napisana aplikacja oraz sposób komunikacji.
Filtry mogą działać na różne sposoby, ale ja postaram się wytłumaczyć te które najczęściej można spotkać w kontekście bezpieczeństwa aplikacji internetowych.

Jednym z najbardziej znanych błędów w tych aplikacjach jest SQL Injection, podatność ta występuje kiedy dane pochodzące od użytkownika nie są w odpowiedni sposób filtrowane.
Kiedy aplikacja nie filtruje w odpowiedni sposób danych możliwe jest wstrzyknięcie składni SQL do zapytania. I tutaj jest słowo klucz, filtrowanie danych w tym przypadku oznacza: wykonanie niezbędnych działań w celu uniemożliwienia wykorzystania wstrzykiwania składni. 
Jeżeli aplikacja przyjmuje tylko parametry liczbowe (np. kolejne liczby w bazie danych to) można zrzutować dane na typ całkowitoliczbowy, a następnie tak zmodyfikowane zapytanie wysłać do bazy danych. Można skorzystać też z innych mechanizmów jak prepared statement.
Innym przykładowym błędem są podatności związane z XML. Tutaj nie wystarczy filtrować danych od użytkownika. Należy ograniczyć rozmiar pliku wynikowego, liczbę zagnieżdżeń, a także dokładnie filtrować, a najlepiej wyłączyć encje.
W przypadku SSTI należy stworzyć whiteliste adresów.
Jeżeli w aplikacji występuje SSTI należy odpowiednio przesyłać parametry do aplikacji oraz ograniczyć możliwość wywoływania niektórych klas. Jak widać możliwości jest wiele. Zaryzykowałbym stwierdzenie, że możliwości filtrowania jest tyle co sytuacji. Też bardzo często w testach penetracyjnych można spotkać się z aplikacjami, które wdrażają różne metody filtrowania, ale gdzieś w tym filtrowaniu jest jakaś luka. Najczęściej tak właśnie wyglądają błędy bezpieczeństwa.

## WAF
Web Application Firewall są to zapory chroniące przed różnymi atakami webaplikacje. Najczęściej pełnią kilka funkcji. Pierwszą z nich jest filtrowanie danych. Sam WAF nie jest dobrym rozwiązaniem na filtrowanie danych, ale pomaga często wykrywać szkodliwe ładunki np. w postaci złośliwego SQL i pomimo obejścia filtrów WAF wycina taki ruch, bo widzi, że coś jest z nim nie tak. W tym kontekście WAF pełni rolę środka dodatkowej ochrony. Najważniejszą funkcją WAFa jest ochrona aplikacji przed różnymi atakami takimi jak DoS, DDoS, wysyłanie danych, które mogą zaszkodzić aplikacji (lub ochrona przed wyciekami danych). W skrócie jest to taki strażnik, który utrudnia wykorzystywanie wielu błędów. WAF mógłbym też zablokować atak SSRF przy wykorzystaniu SSRF. Jak widać zarówno zwykłe filtry jak i WAF doskonale się uzupełniają i nie można stworzyć bezpiecznej aplikacji nie korzystając z tych dwóch rozwiązań.

## Omijanie WAFa
Oczywiście nie zawsze jest możliwe, ale zarówno w filtrach jak i w WAF zdarzają się luki, które nie uwzględniają pewnych przypadków i otwierają nową drogę do ataku.

### Przykład pierwszy:
PHP zastępuje spacje w nazwach parametrów, albo pustym stringiem, albo znakiem podkreślenia `_`. Jeżeli WAF filtruje dane pod kątem nazw parametrów np.: `name=test` jest filtrowany i zabroniony w użytku można spróbować skorzystać z:
`name=test`, czyli parametru ze spacją. Wtedy WAF przefiltruję dane i stwierdzi, że parametr jest inny, a kiedy PHP otrzyma takie dane przekształci nazwę parametru ze spacją na nazwę bez spacji.

### Przykład drugi:
Analogiczny przykład występuje ze znakiem procenta w ASP, który jest prefixem przed wartością heksadecymalną. Jeżeli taka wartość nie zostanie podana to ASP po prostu usuwa znak "%".

### Przykład trzeci:
WAF filtruje dokładnie dane i nie pozwala na szkodliwy ruch. Okazuje się, że aplikacja korzysta z nagłówka:
```sh
X-Forwarded-For:
```
Tym razem wystarczy zmienić wartość nagłówka na 127.0.0.1, a WAF stwierdzi, że ruch pochodzi z maszyny lokalnej i nie będzie blokował zapytań.

### Przykład czwarty:
Wysłanie tego samego parametru wiele razy. Jeżeli WAF filtruje pewne znaki specjalne np. znak cudzysłowu lub myślnika w treści parametru, można spróbować podać ten sam parametr dwa razy np.:
```sh
name=-&name-
```
Pierwszy parametr zostanie odrzucony, a drugi już nie lub na odwrót. Takie zachowanie nie występuje tak rzadko, ale jest powiązane z samą aplikacją więc każdy przypadek jest inny.

### Przykład piąty:
Często też można ominąć WAFa korzystając z techniki związanej z wycinaniem danych. Jeżeli np. WAF filtruje parametr "name" i wycina znaki ukośnika można skonstruować np. takie zapytanie:
```sh
n/a/m/e
```
WAF wykrył złośliwe ukośniki wytnie je i przepuści zapytanie dalej pomimo, że nowy ciąg jest już ciągiem przez niego zabronionym. Chodzi o czas przetwarzania, po wycięciu ukośników WAF uznaje, że wykonał już wszystko. Podobną technikę stosowano kiedyś, żeby ukryć charakterystyczne ciągi znaków w złośliwych programach takich jak malware.
Przypadków jest oczywiście wiele, ale z reguły każdy jest inny, a celem tej sekcji było zaprezentowanie jak wygląda omijanie WAFów, żeby dać ogólne pojęcie do wdrożenie w rzeczywistości
