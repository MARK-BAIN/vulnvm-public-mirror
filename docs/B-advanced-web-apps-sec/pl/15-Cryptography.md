# Kryptografia
Kryptografia obejmuje wiedzę o szyfrowaniu informacji w celu przekazywania ich w sposób bezpieczny. Jest w stanie zapewnić poufność, integralność (potwierdzenie zgodności z oryginałem), uwierzytelnienie (potwierdzenie pochodzenia informacji), niezaprzeczalność (nadawca nie może wyprzeć się nadanej wiadomości, a odbiorca otrzymania jej)

Terminologia:
- tekst jawny - tekst możliwy do odczytania przez ludzi
- tekst utajniony - tekst niezrozumiały dla ludzi
- szyfr - para algorytmów szyfrujący + deszyfrujący

Jednymi z najstarszych znanych szyfrów są na przykład:
- szyfr Cezara - polega na podstawianiu znaków przesuniętych o 13 pozycji (ROT13). 
- szyfr Vigenère - szyfr podstawienny z dodatkowym przesunięciem dla każdego kolejnego symbolu  https://inteng-storage.s3.amazonaws.com/images/JULY/sizes/code_Caesar_vigniere_resize_md.jpg
- szyfr masoński - przypisuje każdej literze alfabetu odpowiedni symbol 
https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Pigpen_cipher_key.svg/1920px-Pigpen_cipher_key.svg.png

Niestety, pomimo tego, że powyższe szyfry nie stanowią dzisiaj żadnego wyzwania dla już przestarzałych maszyn, to wciąż czasami można spotkać ich implementacje lub obecność np. W komentarzach w testowanych aplikacjach.

## Funkcje haszujące
Haszowanie to nieodwracalny proces polegający na przekształceniu dowolnej wartości w krótki stałej długości ciąg znaków nazywany haszem lub skrótem. Hasze wykorzystywane są do przechowywania haseł w sposób nieczytelny oraz do potwierdzania zgodności danych, ponieważ nawet najdrobniejsza zmiana początkowej wartości spowoduje, że hasz będzie zupełnie inny. 
Do najpopularniejszych algorytmów haszujących należą:
LM - najstarszy algorytm haszujący wykorzystywany przez Windows
NTLM - algorytm obecnie wykorzystywany przez Windows
MD5 - popularny algorytm generujący 128 bitowy skrót
SHA1 - rodzina algorytmów produkująca 160 bitowy skrót
SHA2 - rodzina algorytmów produkująca różnej długości skróty 224/256/386/512 czasami nazywana jest również SHA-długość_skrótu

Wszystkie wymienione powyżej algorytmy, za wyjątkiem SHA2 nie powinny być stosowane w nowoczesnych aplikacjach, ze względu na występowanie kolizji oraz znikomą obronę przed siłowymi atakami (brutforcing) mającymi na celu przetestowanie wszystkich dostępnych kombinacji.

Kolizja to sytuacja w której dwie różne wartości produkują ten sam hasz.

### Łamanie haszy
Łamanie haszy to zbiór metod mających na celu uzyskanie pierwotnej wartości, metody te są wykorzystywane podczas prób odzyskania haseł, zaliczają się do nich:
- Rozwiązanie siłowe - polega na zahaszowaniu wszystkich kolejnych możliwych kombinacji, do uzyskania właściwej wartości, rozwiązanie to jest bardzo czasochłonne
- Atak słownikowy - polega na haszowaniu listy najpopularniejszych haseł w nadziei, że lista zawiera owe hasło. Jednym z najpopularniejszych słowników jest lista rockyou.txt
- Atak z użyciem tęczowych tablic - tęczowe tablice przypominają słownik z powyższej metody, w którym każdy wpis posiada hasz oraz odpowiadającą mu wartość, co znacznie przyspiesza proces
- Atak mieszany - może łączyć w sobie elementy ataku siłowego oraz słownikowego

Przykładowe oprogramowanie wykorzystywane do łamania haseł:
- John the Ripper - narzędzie potrafiące rozpoznać algorytm haszujący wykorzystujące CPU do ataków offline
- HashCat - potrafi wykorzystać GPU do ataków offline
- Hydra - wykorzystywana do przeprowadzania ataków online, na przykład na formularzach logowania lub serwerach ssh
- Cain and Abel - program występujący jedynie na systemach Windows, posiada kilka ciekawych funkcji do przeprowadzania ataków sieciowych
- Aircrack - narzędzie służące do odzyskiwania haseł sieci bezprzewodowych
- crackingstation - popularne narzędzie sieciowe https://crackingstation.com/

### Algorytmy specjalne
W odpowiedzi na wyżej wymienione ataki oraz narzędzia powstały algorytmy wykorzystujące Blowfish w celu opóźnienia odzyskania hasła, dla przykładu:
- bcrypt - stanowi domyślny sposób na zabezpieczanie haseł w systemach OpenBSD oraz SUSE Linux
- scrypt - implementacja zaproponowana w 2009 roku

Powyższe algorytmy wykonują operację haszowania kilkukrotnie co znacząco wydłuża czas uzyskiwania ostatecznego skrótu, ma to na celu utrudnienie ataków siłowych.
Ponadto, możliwość wybrania ilości powtórzeń sprawia, że tęczowe tablice nawet dla najpopularniejszych haseł mogłyby zajmować nawet zettabajty przestrzeni dyskowej.
Innym algorytmem wartym uwagi jest wyłoniony w ramach konkursu SHA3, jego wydajność na procesorach ARM wydanych po 2018 roku jest szczególnie obiecująca.

## Szyfrowanie symetryczne
Szyfrowanie symetryczne wykorzystuje pojedynczy klucz prywatny zarówno do szyfrowania jak i deszyfrowania danych. Jest to wykorzystywane głównie do szyfrowania dużych ilości danych takich jak bazy danych lub strumienie danych, ponieważ metoda ta nie pochłania zbyt wiele zasobów.
- AES (Advanced Encryption Standard)
- DES (Data Encryption Standard)
- 3DES (Triple DES)
- IDEA (International Data Encryption Algorithm)
- Blowfish (Może być łatwo wykorzystany zamiast DES lub IDEA)
- RC4 (Rivest Cipher 4)
- RC5 (Rivest Cipher 5)
- RC6 (Rivest Cipher 6)

## Szyfrowanie asymetryczne
Szyfrowanie asymetryczne wykorzystuje dwa klucze, prywatny oraz publiczny. W przypadku najpopularniejszego algorytmu RSA (Rivesta-Shamira-Adlemana), dane zaszyfrowane kluczem prywatnym mogą zostać odszyfrowane przez każdego komu został udostępniony klucz publiczny, ale dane zaszyfrowane kluczem publicznym mogą zostać rozszyfrowane jedynie przez posiadacza klucza prywatnego.
Proces ten jest zdecydowanie bardziej zasobożerny, ale umożliwia wykonywanie podpisów cyfrowych, tą funkcjonalność wykorzystują między innymi certyfikaty zapewniające bezpieczeństwo protokołu HTTPS, czy SSH.
RSA jako klucze wykorzystuje pary dużych liczb pierwszych, co przy odpowiedniej długości klucza sprawia, że nawet jeżeli posiadamy jeden z kluczy, to obliczenie drugiego nie jest wykonalne w rozsądnym czasie przy użyciu tradycyjnych komputerów.

Oprogramowanie do szyfrowania asymetrycznego:
- PGP (Pretty Good Privacy) - oprogramowanie komercyjne
- GPG (GNU Privacy Guard) - oprogramowanie open source

## Certyfikat klucza publicznego
Certyfikat klucza publicznego zapewnia niepodrabialny podpis dzięki podpisaniu przez zaufaną trzecią stronę.

Certyfikaty PGP i SPKI/SDSI wykorzystują zdecentralizowaną sieć zaufania, natomiast X.509 opiera się na hierarchii urzędów certyfikacji, dzięki czemu po spełnieniu pewnych wymagań może być wykorzystywany jako podpis kwalifikowany dzięki któremu możliwe jest na przykład zawieranie umów.

## PKI
Infrastruktura klucza publicznego - to zbiór procedur, zadań, sprzętu, polityk oraz oprogramowania niezbędna do tworzenia, unieważniania, dystrybucji, przechowywania, oraz wykorzystywania szyfrowania klucza publicznego oraz cyfrowych certyfikatów.
Taka infrastruktura umożliwia między innymi SSO (single sign on - logowanie jednym hasłem do wielu miejsc w sposób bezpieczny), jest również szeroko wykorzystywana przez technologię blockchain na której opierają się wszystkie kryptowaluty.

## Diffie-Hellman
Diffie-Hellman - to algorytm służący do wymiany klucza prywatnego pomiędzy dwoma użytkownikami końcowymi w sposób bezpieczny w sieci publicznej. Użytkownik A udostępnia swój klucz publiczny użytkownikowi B, który szyfruje nim swój klucz prywatny tworząc sekret współdzielony. Sekret współdzielony może zostać wykorzystany do symetrycznego szyfrowania komunikacji, lub odtworzenia klucza prywatnego.

## Błędy kryptograficzne i przykłady

Głównym powodem błędów kryptograficznych są własne implementacje algorytmów. Krótko mówiąc, kryptografia nie jest łatwa, nawet dla ekspertów i zdecydowanie lepiej jest wykorzystać gotowe biblioteki. Dla przykładu, AES-128 szyfruje dane w 128 bitowych (16 bajtowych) blokach, jeżeli chcemy zaszyfrować więcej niż 16 bajtów, to 
musimy użyć jednego z trybów blokowych: 
- ECB - Electronic Cookbook mode 
- CTR - Counter Mode 
- CBC - Cipher Block Chaining mode 
- CFB - Cipher Feedback mode 
- OFB -Output Feedback mode 
- PCBC - Propagating cipher-block chaining mode 
- GCM - Galois/Counter Mode 
- CCM - Counter with CBC
- MAC mode Tux zaszyfrowany metodą ECB. 

### Przykłady
1. Szyfrowanie bitmapy za pomocą bardzo wydajnego trybu ECB przynosi następujące skutki:
https://blog.filippo.io/content/images/2015/11/Tux_ecb.jpg

2. Jednym z najprostszych przykładów błędnej implementacji algorytmu szyfrującego jest poniższy kod podatny na ataki czasowe. 
``` js
i=0; 
while (i <= length(hpassword1)) {
 if (hpassword1[i] != hpassword2[i]) {
 return false; i=i+1;
 }
 return true; 
```
3. Wykorzystywanie „sekretu” jako klucza jednorazowego pozwala na:
    - odnalezienie pospolitych symboli oraz ich kombinacji
    - ustalenie długości „sekretu”
    - im częściej „sekret” jest powtarzany, tym łatwiej go odszyfrować
    Przykład można znaleźć tutaj
http://cryptosmith.org/archives/70

4. Używanie eksperymentalnych algorytmów i implementacji

5. Przechowywanie haseł wykorzystanych do szyfrowania danych w postaci tekstów jawnych np. w pliku na pulpicie o nazwie „hasła.txt”

## Padding Oracle/Bit-Flipping
Ataki Padding Oracle oraz Bit-Flipping są stosowane przeciwko trybie szyfrowania CBC.

Wspomniany wcześniej tryb szyfrowania CBC dzieli dane do zaszyfrowania na bloki 8, 16 lub 32 bajtowe, dla przykładu posłużymy się 16-bajtowymi blokami. Pierwszy blok zostanie zaszyfrowany za pomocą wektora inicjalizującego(IV), ten szyfrogram zostanie poddany operacji XOR z następnym blokiem i ta wartość zostanie zaszyfrowana. Wszystkie następne bloki zostaną zaszyfrowane w ten sposób. Algorytm deszyfrujący oczekuje paddingu znajdującego się na  końcu szyfrogramu a najczęściej stosowanym schematem jest PKCS#7, polega on na dodaniu do ostatniego bloku bajtów o wartości równej ilości bajtów brakujących do ustalonej długości. Jeżeli długość danych jest podzielna przez 16 to na końcu zostanie dodany blok 16 bajtów \x10.

### Padding Oracle 
Padding Oracle pozwala na rozszyfrowanie szyfrogramu bez znajomości klucza szyfrującego. Podatne aplikacje pozwalają użytkownikowi na podanie własnego szyfrogramu oraz sygnalizują błędy paddingu.
Do wykorzystania tej podatności służą następujące narzędzia:
- [Bletchley](https://github.com/ecbftw/bletchley)
- [PadBuster](https://github.com/AonCyberLabs/PadBuster)
- [Padding Oracle Exploitation Tool (POET)](http://netifera.com/research/)
- [python-paddingoracle](https://github.com/mwielgoszewski/python-paddingoracle)


Padding Oracle pozwalał na przejęcie uprawnień dowolnego użytkownika w aplikacjach napisanych przy użyciu ASP.NET. W 2014 roku atak POODLE(Padding Oracle On Downgraded Legacy Encryption) sprawił, że protokół SSLv3 nie jest już uważany za bezpieczny.


### Bit-Flipping
Bit-Flipping pozwala na zmodyfikowanie zaszyfrowanych danych bez konieczności rozszyfrowania ich.

Ćwiczenie można znaleźć pod adresem:
http://vuln.vms.perkmylife.com:8080/mutillidae/

W zakładce OWASP2017 > A1-Injection(Other) >CBC-bit Flipping
Adres URL wskazuje na zawartość IV jako parametru metody POST.
W tym przypadku wartość IV jest następująca:
> 6b c2 4f c1 ab 65 0b 25 b4 11 4e 93 a9 8f 1e ba

Metodą prób i błędów jesteśmy w stanie szybko ustalić, że za uprawnienia użytkownika odpowiedzialne są bajty  5-10 o wartościach:

ab 65 0b 25 b4 11

Jako, że chcemy zmienić tylko jedną cyfrę wartości identyfikatorów grupy i użytkownika, możemy zawęzić nasze próby do 5 i 8 bajtu.
Metodą prób i błędów bez problemu możemy dotrzeć do interesujących nas wartości:

aa 65 0b 24 b4 11

Ostateczne IV będzie miało postać:

>6bc24fc1aa650b24b4114e93a98f1eba


## Hash length extension attack
Szczegółowy opis podatności wraz z narzędziem automatyzującym jej wykorzystanie można znaleźć pod poniższym linkiem:
https://github.com/iagox86/hash_extender

W skrócie, podatność ta występuje jeżeli zostaną spełnione następujące warunki:
- do tekstu jawnego dodawana jest tajna wartość
- wykorzystywany jest jeden z podatnych algorytmów
- użytkownik ma dostęp zarówno do tekstu jawnego jak i hasha

W tej sytuacji atakujący może przygotować prawidłowy hash dla następującej struktury danych:
[sekret | dane | złośliwe_dane]

Atak polega na rozpoczęciu procesu hashowania w miejscu w którym aplikacja go zakończyła, większość algorytmów podaje wszystkie potrzebne informacje w ramach wyniku końcowego, nam pozostaje jedynie kontynuować ten proces.

## Obrona
Najskuteczniejszym sposobem obrony przed atakami Padding Oracle, 
Bit-Flipping oraz hash length extension jest wykorzystanie 
algorytmów wykorzystujących HMAC(Hash-based Message Authentication Code).
Zapewnia to integralność i autentyczność danych.

