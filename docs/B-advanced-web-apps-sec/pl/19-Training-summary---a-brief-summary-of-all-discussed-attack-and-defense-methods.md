# Podsumowanie kursu z zaawansowanego bezpieczeństwa aplikacji webowych

## SSTI
Bardzo poważna luka, która w dzisiejszych czasach gdzie aplikacje internetowe są coraz większe ma jeszcze większe znaczenie. Ochrona przed nią nie jest taka łatwa ze względu na mnogość klas zarówno ze strony aplikacji korzystającej z silnika szablonów, jak i samego języka pozwala w wielu przypadkach skorzystać tak z klas, żeby wykonać kod na serwerze. Nawet jeżeli wykonanie kodu nie jest możliwe, to często dzięki niej można chociażby wywołać XSS. 

__Rekomendacja__: Metodą obrony przed tymi atakami jest odpowiednie filtrowanie danych i blacklistowanie klas.

## Niebezpieczna deserializacja
Jest to szczególnie poważny błąd o czym świadczy ponad milion dolarów zarobione przez jednego z Bug hunterów na tylko samym tym błędzie. Bardzo ciężko się przed nią zabezpieczyć. Dodatkowo w większości przypadków powoduje zdalne przejęcie serwera. 

__Rekomendacja__: W tym przypadku warto ograniczyć użycie serializacji do minimum. Dokładnie sprawdzać dane od użytkownika i używać bezpiecznych metod serializujących.

## XXE i SSRF
XXE oraz SSRF to nowoczesne ataki, które zyskują na popularności. W przypadku XXE bardzo często można czytać pliki lub przeprowadzać ataki DoS. Najlepszym sposobem obrony przed tego typu atakami byłoby wyłączenie encji zewnętrznych. Jeżeli wyłączenie encji jest niemożliwe należy skupić szczególną uwagę na odpowiednim filtrowaniu danych od użytkownika oraz na kontroli dostępu do plików. SSRF jest szczególnie poważny w kontekście bezpieczeństwa serwerów niewystawionych do Internetu, które znacznie odbiega od poprzednika. 

__Rekomendacja__: W przypadku SSRF najlepiej whitelistować odpowiednie adresy.

## WAF oraz filtrowanie
Ta część pokazała, że zarówno filtrowanie w ramach aplikacji jak i używanie firewalla warstwy aplikacyjnej nie daje gwarancji bezpieczeństwa. Dodatkowo nawet jeżeli jeden z tych elementów jest wdrożony to bardzo często sama aplikacja jest i tak podatna.

__Rekomendacja__: Warto zwrócić uwagę, że kombinacja wielu elementów bezpieczeństwa zarówno tych podstawowych jak i zaawansowanych pomoże odeprzeć zdecydowanie więcej ataków.

## Kryptografia
Szczególnie w kontekście ostatnich wydarzeń, gdzie kryptografia zajmuje zaszczytne miejsce w OWASP Top Ten widać, że temat odżywa. 
- Podstawowym problemem z kryptografią w aplikacjach internetowych jest używanie słabych algorytmów haszujących bądź szyfrujących. 
- Po drugie trzymanie klucza prywatnego w niezabezpieczonym miejscu. 
- Po trzecie, błędy konfiguracji. 

__Rekomendacja__: Przy implementacji algorytmu kryptograficznego należy raczej skupić się na wybraniu rekomendowanego algorytmu, niż polegać na własnych rozwiązaniach, dokładnie zaimplementować oraz zabezpieczyć klucz prywatny.

## Mechanizmy obronne
To krótka część, która pokazuje z jakich nagłówków HTTP należy korzystać i że jest to absolutnie minimum. Wreszcie omówiliśmy stosowanie CSP, które jest tak istotne w obecnych czasach ze względu na ataki typu XSS. Z kolei analiza logów, czyli sprawdzanie co poszło nie tak, kto i jak korzystał z aplikacji dają świetne możliwości monitorowania bezpieczeństwa - możemy o logach myśleć jak o kamerach CCTV.

__Rekomendacja__: Stosujmy nagłówki bezpieczeństwa, CSP oraz analizę logów w celu monitorowania bezpieczeństwa.

## Inne ataki na aplikacje webowe
Ta część skupia się na trochę mniej znanych, ale istotnych podatnościach. 
- NoSQL Injection, które nie jest tak oczywiste, a zupełnie tak niebezpieczne jak SQL Injection. 
- Ataki typu ReDoS, które występują znacznie częściej niż można się spodziewać. 
- W końcu, omówiliśmy także atak typu zip symlinks o którym wie mało osób i na który nie jest zabezpieczona większość stron, która wypakowuje pliki użytkownika i je mu udostępnia.

## Ćwiczenie podsumowujące
Ćwiczenie podsumowujące doskonale pokazuje, że atakujący najczęściej musi zebrać kilka możliwości ataków, żeby zrobić z niego coś większego, czyli zbiór ataków, które w ostateczności w połączeniu ze sobą umożliwiają kompromitacje serwera.
Tak też najczęściej działa testowanie stron. Na początku zaczyna się od małego błędu, którego skutkiem jest ujawnienie informacji, które są przyczyną większych i bardziej złożonych ataków.

## Zakończenie
To już wszystko przygotowane w tym kursie. 
Dziękuję Ci za skorzystanie z materiałów. 
Wraz z całym zespołem, który wspierał mnie w tworzeniu tego opracowania, 
mamy nadzieję że były one pomocne i rozwinęły Twoje umiejętności.
