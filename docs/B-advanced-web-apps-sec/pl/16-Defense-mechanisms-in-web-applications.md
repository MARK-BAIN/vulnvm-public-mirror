# Sposoby na zwiększenie bezpieczeństwa front-endu

## Nagłówki HTTP odpowiedzialne za bezpieczeństwo
Jednym z podstawowych sposobów bezpieczeństwa przed różnymi atakami, które jednocześnie wymagają małego nakładu pracy są zazwyczaj różnorakie nagłówki HTTP, które mogą zwiększyć bezpieczeństwo.
Jednymi z najważniejszych nagłówków są:
``` haskell
Content-Security-Policy
```
Ten nagłówek potrafi znacząco podnieść bezpieczeństwo przed atakami XSS, ale jest dosyć trudny do właściwej implementacji.
``` haskell
X-XSS-Protection
```
Jak nazwa wskazuje chorni przed XSS, ale tylko w teorii. W praktyce filtruje wiele różnych groźnych payloadów XSSów, ale lepiej dodatkowo wyposażyć się w specjalne biblioteki, które najpewniej zrobią to jeszcze lepiej jak np. `DOMPurify`.
``` haskell
HTTP Strict Transport Security
```
Inaczej HSTS to bardzo prosty nagłówek, który wymusza połączenia HTTPS zamiast HTTP. Dzięki niemu ma się zawsze pewność, że aplikacja używa szyfrowanego połączenia, które nie może być zdeszyfrowane.
```
X-Frame-Options
```
Chroni przed atakami clickjacking - nie pozwalając na wyświetlanie iframe'ów.
``` haskell
Expect-CT
```
W skrócie nagłówek wymusza bezpieczeczne ustawienia certyfikatów dla stron TLS.
Nagłówków jest więcej, a jedna z tych list znajduje się na tej stronie:
https://www.keycdn.com/blog/http-security-headers#2-x-xss-protection

HPKP czyli HTTP Public Key Pinning to nagłówek odpowiadający za powiązanie klucza publicznego z daną domenę. Ma to zapobiec możliwości podszywania się atakującego korzystającego z metody fałszowania certyfikatów klucza publicznego.

## CSP
W celu pokazania mechanizmów obronnych postaram się pokazać wtyczkę CSP Evaluator.
### Sprawdzanie CSP za pomocą wtyczki CVP Evaluator
Wtyczka CSP Evaluation pozwala na sprawdzenie poprawności dyrektyw w CSP oraz wykorzystywanych zewnętrznych plików. Często jest tak, że to zewnętrzne pliki jakiegoś frameworka są źródłem błędów bezpieczeństwa ze względu na to, że ogromna masa mechanizmów daje dużo potencjalnych możliwości ominięcia restrykcyjnej z pozoru polityki CSP.
```sh
https://chrome.google.com/webstore/detail/csp-evaluator/fjohamlofnakbnbfjkohkbdigoodcejf?hl=en
```
Wtyczka dla każdej strony sprawdzi jakość implementacji CSP i jeżeli znajdzie coś interesującego zaznaczy to na czerwono. W ramach zadania polecam wejść na kilka stron z tą wtyczką, żeby przekonać się jak wiele stron ma problemy z właściwą implementacją CSP.

## Dobre praktyki CSP
Do głównych praktyk implementacyjnych CSP należą:
- Sprawdzanie każdego pliku z osobna
- Uproszczenie reguł
- Sprawdzenie możliwości wdrożenia bez niebezpiecznych słów kluczowych jak 'unsafe-line'
- Skorzystanie z CSP Evaluation w celu sprawdzenia poprawności polityki
- Korzystanie z base-uri 'nonce'
- Korzystanie z nonce-hash

## Ćwiczenie: Analiza logów
Zazwyczaj strony Internetowe korzystają z Apache, albo Nginx. Postawienie serwera z wykorzystaniem tych technologii ułatwia wiele rzeczy i skraca czas pracy do czasu pisania strony.
Ponadto oba serwery rejestrują logi z przeprowadzonych przez użytkowników pewnych akcji. Jest to o tyle ważne, że w przypadku włamania bądź niepoprawnego działania łatwo można określić gdzie coś nie działa.
Najpopularniejszymi ścieżkami do plików z logami dla Apache są:
```sh
/var/log/apache/access.log
/var/log/apache2/access.log
/etc/httpd/logs/access_log
/var/log/httpd/error_log
/var/log/apache2/error.log
/var/log/httpd-error.log
```
i dla Nginx
```sh
/var/log/nginx/error.log
/var/log/nginx/access.log
```
Jak widać są tu pliki access.log i error.log. Te pierwsze gromadzą informacje o dostępie do konkretnych zasobów przez użytkowników, a te drugie rejestrują informacje o błędach w dostępie.
Przyjrzyjmy się logom serwera Apache jako dobry przykład metod logowania.
Poniżej fragment pliku error.log
```sh
rm: cannot remove '/var/www/html/uploads/test.zip': Permission denied
rm: cannot remove '/var/www/html/uploads/test2.zip': Permission denied
[Fri Nov 19 16:14:10.814626 2021] [php7:warn] [pid 7736] [client IP:PORT] PHP Warning:  move_uploaded_file(uploads/test.zip): failed to open stream: Permission denied in /var/www/html/zip.php on line 19, referrer: http://vuln.vms.perkmylife.com/zip.php
```
W miejsce IP:PORT jest wycięty publiczny adres IP.
Jak widać jest tutaj informacja o tym, że ktoś próbował usunąć plik test.zip i test2.zip z serwera. Dodatkowo widać, że funkcja która ma przenieść plik test.zip nie działa ponieważ nie ma uprawnień do pliku.
Widać też, że użycie tej funkcji dotyczy pliku zip.php w katalogu głównym na porcie 80. Inne logi mogą wyglądać tak:
```sh
[Fri Nov 19 16:11:32.057016 2021] [php7:error] [pid 7737] [client IP:PORT] PHP Parse error:  syntax error, unexpected '$path' (T_VARIABLE) in /var/www/html/zip.php on line 18, referrer: http://vuln.vms.perkmylife.com/zip.php
```
Jest to komunikat o błędzie składni w pliku zip.php
W error logach bardzo często znajduje się mnóstwo cennych informacji.
Przejdźmy do access.log:
```sh
[19/Nov/2021:16:49:53 +0100] "GET /asdasdsadasdad HTTP/1.1" 404 502 "-" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36"
```
Jak widać po pierwsze mamy datę dostępu do pliku, metodę dostępu, konkretny plik bądź folder, kod odpowiedzi HTTP oraz User-Agent osoby chcącej dostać się do tego zasobu.
Bardzo często WAF blokuje różne User-Agenty jak ffuf czy nmap, które domyślnie mają niestandardowe User-Agent. Warto przy każdym skanowaniu zmieniać User-Agent np. tak, żeby ffuf podszywał się pod przeglądarkę.
Z logami związanymi jest też ciekawy atak wykraczający poza treść kursu:
```sh
https://shahjerry33.medium.com/rce-via-lfi-log-poisoning-the-death-potion-c0831cebc16d#:~:text=What%20is%20log%20poisoning%20%3F,input%20to%20the%20server%20log.
```