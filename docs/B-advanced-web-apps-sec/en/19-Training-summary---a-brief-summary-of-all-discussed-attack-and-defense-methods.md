# Summary of the advanced web application security course

## SSTI
A very serious vulnerability, which nowadays where the Internet applications are getting bigger and bigger, is even more important. It is not easy to protect against due to the multitude of classes, both from the side of the application using the template engine and the language itself, which in many cases allows you to use classes to execute the code on the server. Even if code execution is not possible, you can often even call XSS thanks to it. 

__Recommendation__: The method of defense against these attacks is appropriate data filtering and class blacklisting.

## Dangerous deserialization
This is a particularly serious mistake, as evidenced by over a million dollars earned by one of the Bug Hunters on the same error alone. It is very hard to protect yourself against it. Additionally, in most cases it causes remote server takeover. 

__Recommendation__: In this case, it’s a good idea to keep the use of serialization to a minimum. Examine user data thoroughly and use safe serialization methods.

## XXE and SSRF
XXE and SSRF are modern attacks that are getting popular. In the case of XXE, it is very common to read files or conduct DoS attacks. The best way to defend against these types of attacks would be to disable external entities. If it is impossible to disable an entity, special attention should be paid to appropriate filtering of data from the user and file access control. SSRF is particularly serious in the context of the security of servers not exposed to the Internet, which differs significantly from its predecessor. 

__Recommendation__: In the case of SSRF, it is best to whitelist the appropriate addresses.

## WAF and filtering
This section showed that neither filtering within the application nor using an application layer firewall can guarantee security. In addition, even if one of these elements is implemented, the application itself is very often vulnerable.

__Recommendation__: It is worth noting that the combination of many security elements, both basic and advanced, will help to fend off many more attacks.

# Cryptography
Especially in the context of recent events, where cryptography takes pride of place in the OWASP Top Ten, you can see that the topic is being revived. 
- The main problem with cryptography in web applications is the use of weak hashing or encryption algorithms. 
- Second, keeping the private key in an unsecured place. 
- Third, configuration errors. 

__Recommendation__: When implementing a cryptographic algorithm, you should rather focus on selecting the recommended algorithm than rely on your own solutions, carefully implement and secure the private key.

## Defense mechanisms
This is the short section that shows which HTTP headers you should use and that this is the absolute minimum. Finally, we discussed the use of CSP, which is so important these days due to XSS attacks. On the other hand, the analysis of logs, i.e. checking what went wrong, who and how was using the application, provide great opportunities for security monitoring - we can think of logs as CCTV cameras.

__Recommendation__: We use security headers, CSP and log analysis to monitor security.

## Other attacks on web applications
This part focuses on slightly less known but important vulnerabilities. 
- NoSQL Injection, which is not as obvious and completely as dangerous as SQL Injection. 
- ReDoS attacks that occur much more frequently than you might expect. 
- Finally, we also covered a zip symlinks attack that few people know about and which most sites that unpack and share user files with are not protected against.

## Debriefing Exercise
The debriefing exercise shows perfectly well that an attacker most often has to combine several attack possibilities to make something bigger out of it, that is, a set of attacks that, when combined with each other, allow to compromise the server.
This is also how page testing works most often. It starts out with a small bug that exposes information that causes larger and more complex attacks.

## Conclusion
That’s all we have prepared in this course. 
Thank you for using the materials. 
Together with the entire team that supported me in creating this study, 
we hope they were helpful and developed your skills.
