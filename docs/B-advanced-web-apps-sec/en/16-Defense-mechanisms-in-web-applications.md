# Ways to increase the security of the front-end

## HTTP headers responsible for security
One of the basic security measures against various attacks, which simultaneously require little effort, is usually various HTTP headers, which can increase security.
Some of the most important headlines are:
``` haskell
Content-Security-Policy
```
This header can significantly increase security against XSS attacks, but is quite difficult to implement properly.
``` haskell
X-XSS-Protection
```
As the name suggests, it protects us against XSS, but only in theory. In practice, it filters many different dangerous XSS payloads, but it is better if you additionally equip yourself with special libraries that will probably do it even better, such as `DOMPurify`.
``` haskell
HTTP Strict Transport Security
```
Otherwise, HSTS is a very simple header that forces HTTPS connections instead of HTTP. Thanks to it, you can always be sure that the application uses an encrypted connection that cannot be decrypted.
```
X-Frame-Options
```
It protects against clickjacking attacks - by not allowing displaying of iframes.
``` haskell
Expect-CT
```
In short, the header enforces secure certificate settings for TLS pages.
There are more headlines, and one of these lists is on this page:
https://www.keycdn.com/blog/http-security-headers#2-x-xss-protection

HPKP or HTTP Public Key Pinning is a header responsible for associating a public key with a given domain. This is to prevent the possibility of spoofing an attacker using the method of spoofing public key certificates.

## CSP
In order to show the defense mechanisms, I will try to show the CSP Evaluator plugin.
### Checking CSP with CVP Evaluator plugin
The CSP Evaluation plugin allows you to validate the CSP directives and the external files used. Often it is the external files of some framework that are the source of security bugs due to the fact that a huge mass of mechanisms provides plenty potential opportunities to bypass the seemingly restrictive CSP policy.
```sh
https://chrome.google.com/webstore/detail/csp-evaluator/fjohamlofnakbnbfjkohkbdigoodcejf?hl=en
```
A plugin for each page will check the quality of the CSP implementation and mark it in red if it finds something interesting. To practice it, I recommend visiting several pages with this plugin to see how many pages have problems with the proper implementation of the CSP.

## Good CSP practices
The main CSP implementation practices include:
- Checking each file separately
- Simplification of the rules
- Checking the possibility of implementation without hazardous keywords like 'unsafe-line'
- Using CSP Evaluation to validate the policy
- Using base-uri 'nonce'
- Using a nonce-hash

## Exercise: Log analysis
Typically, websites use Apache or Nginx. Setting up a server with the use of these technologies makes many things easier and shortens the work time until the page is written.
Moreover, both servers record logs from certain actions carried out by users. This is important because in the event of a break-in or incorrect operation, it is easy to determine where something is not working.
The most popular paths to log files for Apache are:
```sh
/var/log/apache/access.log
/var/log/apache2/access.log
/etc/httpd/logs/access_log
/var/log/httpd/error_log
/var/log/apache2/error.log
/var/log/httpd-error.log
```
and for Nginx
```sh
/var/log/nginx/error.log
/var/log/nginx/access.log
```
As you can see, there are access.log and error.log files here. The former collect information about access to specific resources by users, and the latter record information about access errors.
Let’s look at the Apache server logs as a good example of login methods.
Below is a fragment of the error.log file
```sh
rm: cannot remove '/var/www/html/uploads/test.zip': Permission denied
rm: cannot remove '/var/www/html/uploads/test2.zip': Permission denied
[Fri Nov 19 16:14:10.814626 2021] [php7:warn] [pid 7736] [client IP:PORT] PHP Warning:  move_uploaded_file(uploads/test.zip): failed to open stream: Permission denied in /var/www/html/zip.php on line 19, referrer: http://vuln.vms.perkmylife.com/zip.php
```
In place of IP:PORT, the public IP address is cut out.
As you can see, it says that someone tried to remove test.zip and test2.zip from the server. In addition, you can see that the function that is to move the test.zip file is not working because it does not have permission to the file.
You can also see that the use of this function is for the zip.php file in the root directory on port 80. Other logs might look like this:
```sh
[Fri Nov 19 16:11:32.057016 2021] [php7:error] [pid 7737] [client IP:PORT] PHP Parse error:  syntax error, unexpected '$path' (T_VARIABLE) in /var/www/html/zip.php on line 18, referrer: http://vuln.vms.perkmylife.com/zip.php
```
This is a syntax error in the zip.php file
There is often a lot of valuable information in error logs.
Let’s move on to the access.log:
```sh
[19/Nov/2021:16:49:53 +0100] "GET /asdasdsadasdad HTTP/1.1" 404 502 "-" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36"
```
As you can see, first of all, we have the file access date, access method, a specific file or folder, the HTTP response code and the User-Agent of the person who wants to access this resource.
Very often WAF blocks various User-Agents like ffuf or nmap, which have a custom User-Agent by default. It makes sense to change the User-Agent every time you scan, e.g. so that ffuf pretends to be the browser.
There is also an interesting attack related to the logs that goes beyond the content of the course:
```sh
https://shahjerry33.medium.com/rce-via-lfi-log-poisoning-the-death-potion-c0831cebc16d#:~:text=What%20is%20log%20poisoning%20%3F,input%20to%20the%20server%20log.
```