# Cryptography
Cryptography involves the knowledge of encrypting information in order to transmit it securely. It can ensure confidentiality, integrity (confirms compliance with the original), authentication (confirms the origin of information), non-repudiation (the sender cannot deny sending the message, and the recipient receiving it)

Terminology:
- visible text - human-readable text
- classified text - text incomprehensible to humans
- cipher - pair of encryption and decryption algorithms

Some of the oldest known ciphers are, for example:
- Caesar cipher - consists in substituting characters shifted by 13 positions (ROT13). 
- Vigenère cipher - substitution cipher with an additional offset for each subsequent symbol https://inteng-storage.s3.amazonaws.com/images/JULY/sizes/code_Caesar_vigniere_resize_md.jpg
- Masonic cipher - assigns each letter of the alphabet an appropriate symbol 
https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Pigpen_cipher_key.svg/1920px-Pigpen_cipher_key.svg.png

Unfortunately, despite the fact that the above ciphers do not pose any challenge today for already obsolete machines, sometimes you can still find their implementations or presence, for example, in the comments of the tested applications.

## Hash functions
Hashing is the irreversible process of converting any value into a short, fixed-length string called a hash or abbreviation. Hashes are used to store passwords in an unreadable way and to confirm data compliance, because even the slightest change of the initial value will cause the hash to be completely different. 
The most popular hashing algorithms include:
LM - the oldest hashing algorithm used by Windows
NTLM - an algorithm currently used by Windows
MD5 - popular algorithm that generates 128 bit hash
SHA1 - a family of algorithms that produces a 160 bit hash
SHA2 - a family of algorithms that produces hashes of different lengths 224/256/386/512 sometimes also called SHA-hash_length

All the above-mentioned algorithms, except SHA2, should not be used in modern applications due to the occurrence of collisions and negligible defense against brute-forcing aimed at testing all available combinations.

A collision is when two different values produce the same hash.

### Hash cracking
Hash cracking is a set of methods to obtain the original value, these methods are used when trying to recover passwords, they include:
- Force solution - involves hashing all subsequent possible combinations to obtain the correct value, this solution is very time-consuming
- Dictionary attack - hashes a list of the most popular passwords, hoping that the list contains the password. One of the most popular dictionaries is the rockyou.txt list
- Rainbow table attack - rainbow tables are similar to the dictionary from the above method, in which each entry features a hash and corresponding value, which significantly speeds up the process
- Mixed attack - can combine elements of brute force and dictionary attacks

Sample software used to crack passwords:
- John the Ripper - a tool that can recognize the hashing algorithm using the CPU for offline attacks
- HashCat - can use the GPU for offline attacks
- Hydra - used to launch online attacks, for example on login forms or ssh servers
- Cain and Abel - a program available only on Windows systems, it has some interesting functions for carrying out network attacks
- Aircrack - a tool for recovering passwords for wireless networks
- crackingstation - popular network tool https://crackingstation.com/

### Special algorithms
In response to the above-mentioned attacks and tools, algorithms were created that use Blowfish to delay password recovery, for example:
- bcrypt - is the default password protection on OpenBSD and SUSE Linux
- scrypt - implementation proposed in 2009

The above algorithms perform the hashing operation several times, which significantly extends the time of obtaining the final hash, which is to make brute force attacks more difficult.
In addition, the ability to select the number of repetitions means that rainbow tables, even for the most popular passwords, could take up even zettabytes of disk space.
Another noteworthy algorithm is the result of the SHA3 competition, its performance on ARM processors released after 2018 is particularly promising.

## Symmetric encryption
Symmetric encryption uses a single private key to both encrypt and decrypt data. It is mainly used to encrypt large amounts of data such as databases or data streams as this method is not very resource intensive.
- AES (Advanced Encryption Standard)
- DES (Data Encryption Standard)
- 3DES (Triple DES)
- IDEA (International Data Encryption Algorithm)
- Blowfish (Can be easily used in place of DES or IDEA)
- RC4 (Rivest Cipher 4)
- RC5 (Rivest Cipher 5)
- RC6 (Rivest Cipher 6)

## Asymmetric encryption
Asymmetric encryption uses two keys, private and public. In the case of the most popular RSA algorithm (Rivest-Shamir-Adleman), data encrypted with the private key can be decrypted by anyone who has gained access to the shared public key, but data encrypted with the public key can only be decrypted by the owner of the private key.
This process is definitely more resource-consuming, but it allows you to perform digital signatures, this functionality is used, among others, by certificates that ensure the security that comes with the HTTPS or SSH protocol.
RSA uses pairs of large prime numbers as keys, which, given the appropriate key length, means that even if we have one of the keys, crunching the other is not feasible in a reasonable time using traditional computers.

Asymmetric encryption software:
- PGP (Pretty Good Privacy) - commercial software
- GPG (GNU Privacy Guard) - open source software

## Public key certificate
A public key certificate provides a forgery-proof signature by being signed by a trusted third party.

PGP and SPKI/SDSI certificates use a decentralized trusted network, while X.509 is based on the hierarchy of certification authorities, thanks to which, after meeting particular requirements, it can be used as a qualified signature, thanks to which it is possible, for example, to conclude contracts.

## PKI
Public key infrastructure - is a set of procedures, tasks, hardware, policies and software necessary to create, revoke, distribute, store, and use public key encryption and digital certificates.
Such infrastructure enables, amongst other things, SSO (single sign on - logging in with one password to many places in a secure manner), it is also widely used by the block-chain technology on which all cryptocurrencies are based.

## Diffie-Hellman
Diffie-Hellman - is an algorithm used to exchange the private key between two end users in a safe manner in a public network. User A shares his public key with user B, who encrypts his private key with it, creating a shared secret. The shared secret can be used to symmetrically encrypt communication, or to recover the private key.

## Cryptographic bugs and examples

The main reason for cryptographic bugs is own algorithm implementations. In short, cryptography is not easy, even for experts, and it is much better to use ready-made libraries. For example, AES-128 encrypts data in 128 bit (16 byte) blocks, if we want to encrypt more than 16 bytes, then 
we need to use one of the block modes: 
- ECB - Electronic Cookbook mode 
- CTR - Counter Mode 
- CBC - Cipher Block Chaining mode 
- CFB - Cipher Feedback mode 
- OFB - Output Feedback mode 
- PCBC - Propagating cipher-block chaining mode 
- GCM - Galois/Counter Mode 
- CCM - Counter with CBC
- MAC mode Tux encrypted by the ECB method. 

### Examples
1. Encrypting a bitmap with the very efficient ECB mode translates into the following effects:
https://blog.filippo.io/content/images/2015/11/Tux_ecb.jpg

2. One of the simplest examples of incorrect implementation of the encryption algorithm is the following code vulnerable to time attacks. 
``` js
i=0; 
while (i <= length(hpassword1)) {
 if (hpassword1[i] != hpassword2[i]) {
 return false; i=i+1;
 }
 return true; 
```
3. Using a "secret" as a one-time key allows you to:
    - find common symbols and their combinations
    - determine the length of the "secret"
    - the more the "secret" gets repeated, the easier it can be decoded
    Here’s an example
http://cryptosmith.org/archives/70

4. Use of experimental algorithms and implementations

5. Storage of passwords used for data encryption in the form of plaintexts, e.g. in a file on the desktop called "password.txt"

## Padding Oracle/Bit-Flipping
Oracle Padding and Bit-Flipping attacks are used against the CBC encryption mode.

The aforementioned CBC encryption mode divides the data to be encrypted into blocks of 8, 16 or 32 bytes; let's use 16-byte blocks, as an example. The first block will be encrypted with an initialization vector (IV), this ciphertext will be XORed with the next block, and this value will be encrypted. All subsequent blocks will be encrypted this way. The decryption algorithm expects padding at the end of the ciphertext and the most commonly used scheme is PKCS#7; it’s about adding bytes to the last block that are equal to the number of bytes missing to the fixed length. If the data length is divisible by 16, a block of 16 bytes \x10 will be added to the end.

### Padding Oracle 
Padding Oracle allows you to decode a ciphertext without knowing the encryption key. Affected applications allow the user to enter their own ciphertext and signal padding errors.
The following tools are used to exploit this vulnerability:
- [Bletchley](https://github.com/ecbftw/bletchley)
- [PadBuster](https://github.com/AonCyberLabs/PadBuster)
- [Padding Oracle Exploitation Tool (POET)](http://netifera.com/research/)
- [python-paddingoracle](https://github.com/mwielgoszewski/python-paddingoracle)


Padding Oracle allowed to take over the permissions of any user in applications written with ASP.NET. In 2014, a POODLE (Padding Oracle On Downgraded Legacy Encryption) attack made the SSLv3 protocol no longer considered secure.


### Bit-Flipping
Bit-Flipping allows you to modify encrypted data without having to decrypt it.

The exercise can be found at:
http://vuln.vms.perkmylife.com:8080/mutillidae/

In the tab OWASP2017 > A1-Injection(Other) >CBC-bit Flipping
The URL points to the contents of IV as a parameter to the POST method.
In this case, the value of IV is as follows:
> 6b c2 4f c1 ab 65 0b 25 b4 11 4e 93 a9 8f 1e ba

By trial and error, we are able to quickly determine that bytes 5-10 with the following values are responsible for user permissions:

ab 65 0b 25 b4 11

As we want to change only one digit of the group and user ID values, we can narrow down our attempts to bytes 5 and 8.
By trial and error, we can easily reach the values we are interested in:

aa 65 0b 24 b4 11

Final IV will be as follows:

>6bc24fc1aa650b24b4114e93a98f1eba


## Hash length extension attack
A detailed description of the vulnerability along with a tool automating its use can be found at the following link:
https://github.com/iagox86/hash_extender

In short, this vulnerability exists if the following conditions are met:
- a secret value is added to the plaintext
- one of the vulnerable algorithms is used
- the user has access to both the plaintext and the hash

In this situation, the attacker can prepare a valid hash for the following data structure:
[secret | data | malicious_data]

The attack is based on starting the hashing process in the place where the application ended it, most algorithms provide all the necessary information as part of the final result, we only need to continue the process.

## Defense
The most effective way to defend against Padding Oracle attacks, 
Bit-Flipping and the hash length extension is taking advantage of 
algorithms using HMAC(Hash-based Message Authentication Code).
This ensures data integrity and authenticity.

