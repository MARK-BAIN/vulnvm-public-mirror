# Filters in applications and firewalls in web applications

## Filters in applications
Filters in applications are quite a diverse topic due to factors such as application function, risk factors, required security, technology in which the application is written and the way of communication.
Filters can work in various ways, but I will try to explain the most common ones in the context of web application security.

One of the best known bugs in these applications is SQL Injection, this vulnerability occurs when user data is not properly filtered.
When the application is not filtering the data properly, it is possible to inject SQL syntax into the query. And here’s a keyword, filtering data in this case means taking the necessary actions to prevent syntax injection from being used. 
If the application accepts only numeric parameters (e.g. further numbers in the database then you can cast data to an integer type, and then send thus modified query to the database. You can also use other mechanisms like the prepared statement.
XML-related vulnerabilities are another example of a bug. Here it is not enough to filter data from the user. You should limit the size of the output file, the number of nests, and filter carefully, preferably turning off entities.
In the case of SSTI, you need to create a white list of addresses.
If there is SSTI in the application, the parameters should be properly transferred to the application and the possibility of calling certain classes should be limited. As you can see, there are many possibilities. I would venture to say that there are as many filtering options as there are situations. Also, very often in penetration testing you can find applications that implement various filtering methods, but somewhere in this filtering there is a loophole. Most often, this is what security bugs look like.

## WAF
Web Application Firewalls are firewalls that protect web applications against various attacks. Most often they perform several functions. The first is data filtering. WAF itself is not a good solution for data filtering, but it often helps to detect malicious payloads, e.g. in the form of malicious SQL, and despite bypassing the WAF filters, it cuts out such traffic because it sees that something is wrong with it. In this context, WAF acts as a bonus protection measure. The most important function of WAF is to protect the application against various attacks such as DoS, DDoS, sending data that may harm the application (or protection against data leaks). In short, it acts as a gatekeeper that prevents exploitation of numerous bugs. WAF could also block an SSRF attack using the SSRF. As you can see, both ordinary filters and WAF complement each other perfectly and you cannot create a safe application without using these two solutions.

## Avoiding WAF
Of course, it is not always possible, but in both filters and WAF there are vulnerabilities that do not take certain cases into account and open up a new avenue for attack.

### First example:
PHP replaces spaces in parameter names with either an empty string or an underscore `_`. If WAF filters data in terms of parameter names, e.g.: `name=test` is filtered and forbidden to use, then you can try:
`name=test`, i.e. a parameter with a space. Then WAF will filter the data and find that the parameter is different, and when PHP receives such data, it will transform the parameter name with a space into a name without a space.

### Second example:
A similar example occurs with a percent sign in the ASP that is a prefix before the hexadecimal value. If no such value is given, ASP simply removes the "%" sign.

### Third example:
WAF filters the data thoroughly and does not allow malicious traffic. It turns out that the application uses the header:
```sh
X-Forwarded-For:
```
This time, just change the header value to 127.0.0.1 and WAF will determine that the traffic is coming from the local machine and won’t be blocking queries.

### Fourth example:
Sending the same parameter multiple times. If WAF filters some special characters, e.g. the quotation mark or dash in the parameter content, you can try to enter the same parameter twice, e.g.:
```sh
name=-&name-
```
The first parameter will be rejected unlike the second parameter, or vice versa. This behavior is not so rare, but it is related to the application itself, so each case is different.

### Fifth example:
You can also often bypass WAF by using a technique related to data cutting. For example, if WAF filters the "name" parameter and cuts out slash characters, you can construct e.g. a query like this:
```sh
n/a/m/e
```
WAF detects malicious slashes and will pass the query on even though the new string has already been forbidden by it. It’s about processing time, after cutting the slashes, WAF decides that its job is done. A similar technique was once used to hide characteristic strings of characters in malicious programs such as malware.
Obviously, there’s a plethora of cases, but as a rule, each one is different, and the aim of this section was to present how WAF bypassing looks like, to give a general idea how to implement it in reality
