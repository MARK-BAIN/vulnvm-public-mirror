# Bezpieczeństwo HTML5 oraz API

## postMessage()
Funkcja postMessage umożliwia przesyłanie informacji np. między stroną, a wyskakującym okienkiem. Taka funkcjonalność może prowadzić do różnego wycieku danych, które nie powinny wyciec w kontekście różnych ramek. Bardzo często wykorzystuje się ją przy atak na CORS, o których będzie dalej w tym kursie.
Więcej o tej funkcji można znaleźć w dokumentacji mozilli:
https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage

## Cross-Origin Resource Sharing
CORS powstał w odpowiedzi na zbyt restrykcyjne reguły dotyczące SOP. Przede wszystkim Origin czyli serwer identyfikujący się w trzech aspektach: URL, port oraz protokoł miał pewne wady. Szybko się okazało, że po wwdrożeniu SOP żadne interakcje nie są możliwe. Ani serwer A, ani serwer B, nie mogą pobierać i udostępniać między sobą informacji. To silna restrykcja, która niestety nie może funkcjonować w świeicie obecnych serwisów Internetowych, które korzystają z wielu zzewnętrznych zasobów czy to w postaci zupełnie niezależnych serwerów czy to nawet w postaci pod serwerów lub subdomen. CORS jest odpowiedzią na te problemy, bo pozwala poluzować odpowiednio restrykcje polityki SOP. Niestety, z takim luzowaniem reguł wiąże się wysoka odpowiedzialność, która nie raz jak się okazuje zostało nadszarpnięta przez ogromne serwisy. Reguły CORS przede wszystkim mają ochronić przed atakami skryptów międzydomenowych i tym podobnych czyli np. XSS oraz CSRF. Świetnym przykładem potrzeby poluzowania ograniczeń są płatności Internetowe. Takie płatności mmuszą być bardzo dobrze zabezpieczone. W tym celu wiele serwisów korzysta z już gotowych formularzy, które są bardzo dobrze sprawdzone. Gdyby SOP był silnie egzekwowany wymiany informacji między bramką płatności, a serwisem nie mogłaby nastąpić.

## Działanie CORS
Reguły CORS pozwalają na pewne wyjątki w komunikacji dzięki temu strony mogą dobrze funkcjonowac jednocześnie egzekwując odpowiednie bezpieczeństwo
W związku z nową funkcjonalność CORS udostępnia szereg nagłówków z czego niektóre z nich są wymagane:
```html
Origin: # Tutaj jest origin strony, która chce wydać zapytanie
```
```html
Access-Control-Allow-Origin: # Można podać albo Origin z którym może się kontaktować strona, albo "*", która oznacza, że każda strona może wymieniać informacje. Gwiazdka z punktu widzenia bezpieczeństwa oznacza brak jakichkolwiek zasad bezpieczeństwa w tym SOP.
```
## Service workers
Service workes powstał w odpowiedzi na zapotrzebowania skorzystania z stron WWW w przypadku braku Internetu. Podobnym mechanizmem jest Appcache, który pozwala na zdefiniowanie plików, które mogą być przenoszone do pamięci cache i dzięki temu dostępne nawet przy braku informacji. Warto zaznaczyć, że Service Workes działają tylko w przypadku połączenia HTTPS. W celu skorzystania z mechanizmu Service Workers należy go najpewierw zdefiniować:
```js
if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/actions.js').then(function(r) {
    console.log('Good')
  }).catch(function(e) {
    console.log('Error! '+e)
  });
}
```
Odtąd wszystkie akcje javascriptu typu fetch lub install będą rejestrowane do pliku actions.js
Mechanizmy Service workers posiadają kilka funkcjonalności.
```js
self.addEventListener('fetch', function(ev)
self.addEventListener('activate', function(ev)
```
oraz inne. Fetch wywoływany jest w przypadku pobrania dowolnych zasobów niezależnie czy są to zasoby offilne czyli już scachowane czy online. Activate jest wywoływany wtedy kiedy cache należy zmienić zastąpić go nowszą wersją.
Problem występuje kiedy pobierany jest zasób, który nie istnieje. Nieważne od jego stanu zostanie wykonana funkcja fetch z jej ciałem. Jeżei użytkownik np, w skutek wykorzystania błędu typu XSS zmieni odpowiedź funkcji fetch to dowolna osoba wpisująca niepoprawny adres URl stanie się ofiarą ataku złośliwej odpowiedzi np. XSS wykradającego dane sesji.

## Web sockets
Web sockets jest odpowiedzią na bezstanowość protokołu HTTP. Po pierwsze każde żądanie jest ze sobą niepowiązane przez co utrudniona jest komunikacja w czasie rzeczywistym np. w formie chatu Internetowego. Kolejnym problemem są zapytania i odpowiedzi HTTP. Jeżeli klient nic nie wyślę to serwer nawet nie wyślę pustej odpowiedzi HTTP. Co jest zjawiskiem niepożądanym np. w komunikatorach.
Web sockets jest często stosowany w komunikatorach i innych aplikacji opartych na podobnym działaniu. Przede wszystkim jeog zaletą jest asynchorniczność tj, nieważne kto wysyła zapytanie lub odpowiedzień komunikacja jest ciągła, a nie tak jak w przypadku HTTP zawieszona. 
W celu nawiązania komunikacji z Web sockets do serwera jest wysyłany tzw. handshake
``` haskell
GET /webchat HTTP/1.1
Host: example.com
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Key: asjfksanjfnsakf==
Sec-WebSocket-Protocol: chat
Sec-WebSocket-Version: 1337
```
Ciekawym nagłówkiem jest Sec-WebSocket-Key, który niestety z punktu widzenia bezpieczeństwa ma małe znaczenia. Po pierwsze pomimo słowa kluczowego "klucz", wartośc nagłówka nie ma nic z nim wspólnego, a przynajmniej w popularnym rozumieniu. Jest to po prostu ciąg znaków zakodowany w Base64 ( w celach poprawnego czytania przez HTTP), którego zadaniem jest unikanie cachowania. Jeżeli z każdym żądaniem komunikacji klucz będzie inny nikt nie będzie miał dostępu do cachu innej osoby. 
Jeżeli serwer wspiera komunikacje Web sockets wysyła odpowiedź
``` haskell
HTTP/1.1 101 Switching Protocols
```
czyli przełączanie protokołów.
Podstawowym częstym błędem w przypadku tej funkcjonalności jest nadmierne zaufanie użytkownikowi. Zmiana protokołu można wzbudzić prawdziwe np:
Użytkownik wysyła wiaodmość Hi, I'm John
``` json
{"message":"Hi, I'm John"}
```
Taka odpowiedź jest odbierana przez serwer i wyświetlana np, w okienku czatu:
```html
<h1>Hi, I'm John</h1>
```
Jeżeli serwer nie poprawnie filtruje dane to z łatwością można wstrzyknac kod XSS
``` json
{"message":"<script>alert('XSS');</script>"}
```