# Sposóby na zwiększenie bezpieczeństwa front-endu

## Nagłówki HTTP odpowiedzialne za bezpieczeństwo
Jednym z podstawowych sposób bezpieczeństwa przed różnymi atakami, które jednocześnie wymagają małego nakładu pracy są zazwyczaj różnorakie nagłówki HTTP, które mogą zwiększyć bezpieczeństwo.
Jednymi z najważniejszych nagłówków są:
``` haskell
Content-Security-Policy
```
Ten nagłówek potrafi znacząco podnieść bezpieczeństwo przed atakmi XSS, ale jest dosyć trudny do właściwej implementacji.
``` haskell
X-XSS-Protection
```
Jak nazwa wskazuję chorni przed XSS, ale tylko w teorii. W praktyce filtruje wiele różnych groźnych payloadów XSSów, ale lepiej dodatkowo wyposażyć się w specjalane biblioteki, które najpewniej zrobią to jeszcze lepiej jak np. `DOMPurify`.
``` haskell
HTTP Strict Transport Security
```
Inaczej HSTS to bardzo prosty nagłówek, który wymusza połączenia HTTPS zamiast HTTP. Dzięki niemu ma się zawsze pewność, że aplikacja używa szyfrowanego połączenia, które nie może być zdeszyfrowane.
```
X-Frame-Options
```
Chroni przed atakami clickjacking - nie pozwalając na wyświetlanie infreamów.
``` haskell
Expect-CT
```
W skrócie nagłówek wymusza bezpieczeczne ustawienia certyfikatów dla stron TLS.
Naglówków jest więcej, a jedna z tych list znajduje się na tej stronie:
``` haskell
https://www.keycdn.com/blog/http-security-headers#2-x-xss-protection
```
HPKP czyli HTTP Public Key Pinning to nagłówek odpowiadający za powiązanie klucza publicznego z daną domenę. Ma to zapobiec możlwiości podszywania się atakującego korzystającego z metody fałszowania certyfikatów klucza publicznego.
## Flagi dla ciasteczek
W swojej standardowej formie można korzystać z ciasteczek na wiele różnych sposóbów jak pobieranie ciasteczek przy użyciu javascriptu. Ten temat zajmuuje się zabezpieczaniem ciasteczek przed odczytaniem przez nieuprawnionych użytkowników.
Poruszane tutaj techniki są podstawwowe co nie oznacza, że nie należy ich stosować, wręcz przeciwnie. Należy jednak pamiętać, że jest to tylko fundament.
```sh
SameSite
```
W zależności od ustawień ciasteczka mogą zostać wysyłane między witryną z której pochodzą, albo między pewnymi konkretynmi witrynami. Dzięki tej opcji atakujący pownienien nie mieć możliwości wysłania ciasteczek na swój serwer.
```
HTTPOnly
```
Jeżeli ta flaga jest obecna, kod javascriptu nie może pobrać ciasteczka z przeglądarki. Obniża to zdecydowanie ryzyko wykorzystania ataku typu XSS do przejęcia sesji użytkowników.
## Dobre rady dla programistów
Postaram się wymienić kilka ogólnych rad, które powinny zostać wdrożone przez programistów:
- Zapoznanie się z Owasp Top ten
- Jeżeli jakaś funkcja ma w swojej nazwie danger, lub podobny przymotnik należy jej unikać
- Należy wdrozyć nagłówki bezpieczeństwa
- Powinno się wymusić tylko połączneie przez HTTPs
- Nigdy nie ufać użytkownikowi
- Sprawdzić wiele razy dokładnie jakie dane może przesłać użytkownik i wdrożyc techniki obrony przed tym
- Zwracać dużą uwagę na korzystanie z machanizmów serializacji
- Zwracać uwagę na błędy mniejszego kalibru jak Clickjacking lub CSRF i wdrożyć odpowiednie metody ochrony