# Podsumowanie metod ataku i obrony przed atakami po stronie klienta
W tej części postaram się krótko podsumować atak jak i obronę w kontekście tego co było przekazane na kursie.

## Same-Origin Policy
W teorii Same-Origin Policy zabezpiecza stronę przed różnymi atakami w szczególności przed XSS. W praktyce przez tak duże restrykcje niemalże każda strona decyduje się na poluzowanie zabezpieczeń co wpływa na bezpieczeństwo. Niestety Same-Origin Policy jest zbyt mało elastycznym mechanizmem obrony, żeby sam był wystarczający. W praktyce SOP jest kompletną podstawą, którą należy dobrze zaimplementować, ale nie zapominając o innych błędach bezpieczeństwa.

## XSS - najważniejsza podatność client-side na świecie
To jeden z najpopularniejszych błędów. Praktyka pokazuje, że metod na obchodzenie zabezpieczeń przed tym atakiem jest naprawdę wiele i trzeba zastosować wielowarstwową kontrolę - łącznie z CORS, SOP, CSP oraz WAFem, żeby można cokolwiek zdziałać w kierunku pełnego zabezpieczenia aplikacji. Zdecydowanie warto skupić się na tym błędzie i szukać go chociażby w ramach programu Bug Bounty.

## Inne ataki po stronie klienta
Często ataki z tej zakładki są ignorowane, a jest to podstawowy błąd. Wszystkie te ataki są bardzo skuteczne szczególnie w atakach na konkretne cele. CSRF może wyrządzić ogromne szkody, o ile zostanie dobrze użyty. Dodatkowo dangling markup może ułatwić wiele rzeczy w kontekście omijania CSRF. Clickjacking jest bieżącym problemem, który czasami jest używany nawet dziś do kradzieży różnych danych. JSON Hijacking to stosunkowo nowy temat, ale praktyka pokazała że jest to temat którym naprawdę warto się zająć. RPO jest dosyć specyficzne, ale czasami warto z niego skorzystać o ile jest możliwość.

## Problem z HTML5
Problemy te ujawniają się na wiele sposobów. Warto pamiętać, że nawet nowe rozwiązania jak Websockets nadal posiadają stare błędy. Okazuje się, że CORS nie jest wystarczający więc warto go wdrażać, ale jako dodatek mający budować bezpieczeństwo, a nie być jego głównym składnikiem.

## Content Security Policy
Elastyczność CSP z pozoru bardzo pożądana stała się przyczyną często złego wdrożenia, które jest przyczyną błędów bezpieczeństwa. Mnogość opcji CSP pokazuje, że trzeba naprawdę dobrze sprawdzić jakie dyrektywy i w jaki sposób się definiuje.

## Sposóby na zwiększenie bezpieczeństwa front-endu
W tej części wymieniłem kilka niezbędnych nagłówków, które są absolutną podstawową i powinny stanowić pierwsze kroki, a na pewno nie ostatnie ku bezpieczeństwu.

## JS Libraries
Ten rozdział bardzo dobrze pokazuje, że nawet te bardzo znane biblioteki cierpią na różnego rodzaju błędy bezpieczeństwa oraz że bezgraniczne zaufanie twórcom tych programów może być ślepą uliczką.

## Bug Bounty
Wreszcie całą zdobytą wiedzę można przećwiczyć w ramach Bug Bounty i jeszcze na tym zarobić.

## Ćwiczenia podsumowujące
Ćwiczenie XSS + CSRF pokazało, że bardzo często w normalnych sytuacjach jakieś filtry są, ale pomimo to atakujący łącząc różne fakty i luki jest w stanie tak czy inaczej podatność wykorzystać.