# Biblioteki JS (jQuery, Angular, React, Knockout)
## Problemy z bibliotekami JS
Ze względu na korzystanie przez te biblioteki z Javascriptu istnieją pewne implikacje związane z bezpieczeństwem. Przede wszystkim należy zauważyć, że są one bardzo często rozbudowane co zwiększa ilosć danych, które może wprowadzić w różne miejsca do nich użytkownik. Takie zachowanie może doprowadzić do błędów bezpeiczeństwa jak XSS. Bardzo często jednym z sposób omijania CSP jest możliwość wstrzyknięcia XSS w bibliteke zewnętrzną Javascriptu, która jest podatna. Z racji tego, że CSP pozwala na ładownaie plików tych bibliotek jeżeli aplikacja je wykorzystuję, omijana jest też ochrona CSP,

# Wstrzykiwanie szablonów JS
Template Injection to znany błąd bezpieczeństwa, ale w przypadku tzw. Server-Side Temlate Injection. Mało osób zdaje sobię sprawę, że wstrzyknięcie do szablonu można wykonać po stornie klienta. Wtedy takie wstrzykniecie jest związane z korzystaniem z błędów po stornie klienta jak np. XSS.
Można rozpatrzyć przykładowy kod w AngularJS:
```js
{{constructor.constructor('alert(1)')()}}
```
Tutaj przykład prostego wstrzyknięcia w mechanizm szabkonów, który wrzuca w treść strony kod JS.

## CVE związane z bibliotekami JS
Tak jak z każdym oprogramowaniem czy funkcjonalnością błędy zawsze będą istnieć dlatego nieuchronną rzzeczywistością są błędy z CVE czyli błędy, które wystepują w penwych aplikacjach, które dostarczają ją do wielu użytkowników. Jeżeli ktoś znalazł błąd w bibliotece jQuery gdzie jest XSS to od tej pory wszystkie strony używające tej wersji są podatne na taki atak. Dlatego warto śledzić na bieżąco  podatności CVE. Postaram się podać jeden z przykkładów błędów CVE w bibliotekach JS, który rzutuje na wszystkich używających podatnej wersji biblioteki.
Podatność została odkryta przez jednego z badaczy w ramach programów Bug Bounty o których będzie mowa w dalszych częściach kursu.
https://hackerone.com/reports/141463

Podatność tyczy się bardzo znanej i często używanej biblioteki Angular JS i pozwala przeprowadzić atak Stored XSS. 
W celu jej wykorzystania należy stowrzyć konto doktora. Następnie odwiedzić podaną podstronę
http://example.com/messages/referrals/contacts/

Dodać kontakt, a w miejsce adresu wkleić podany ładunek:
```js
[[constructor.constructor('alert(1)')()]]
```
Widać tutaj proste wstrzyknięcie przez template injection. Od teraz każdy użytkownik wyświetlający stronę z kontaktami będzie podatny np. na kradzież ciasteczek. 
To dobrze pokazuje, że nawet tak znane biblioteki nie zawsze są bezpieczne, a bezpieczeństwo należy zagwarantować na wielu poziomach i skutecznie przewidywać i zapobiegać takim rodzajom błędow np. minimalizując ryzyko przez CSP.