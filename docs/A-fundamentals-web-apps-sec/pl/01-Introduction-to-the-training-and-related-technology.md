# Zanim zaczniesz, przygotowanie środowiska
W tym kursie przygotowanych zostało wiele ćwiczeń, które dostępne są w ramach specjalnie stworzonej przez nas maszyny wirtualnej __VulnVM__. Zawiera ona m.in. autorsko stworzone małe, podatne aplikacje webowe, a także kontener Docker z obrazem w pełni działającej aplikacji [OWASP Mutillidae](https://github.com/webpwnized/mutillidae).

W celu wykonania ćwiczeń możesz skorzystać z trzech opcji:
1. Korzystanie z publicznie dostępnego obrazu VulnVM
2. Pobranie i instalacja obrazu VulnVM w .ova
3. Samodzielne zbudowanie VulnVM

## 1. Korzystanie z publicznie dostępnego obrazu VulnVM
W przypadku korzystania z naszego publicznego adresu IP nie trzeba nic robić. Wirtualna maszyna jest dostępna pod adresem:
- IP: `161.35.221.103` lub 
- URL: http://vuln.vms.perkmylife.com

Jednak, prosimy abyś miał na uwadze, że z powodu iż pod tym adresem dostępnych jest wiele bardzo podatnych aplikacji webowych, nie możemy zagwarantować, że powyższy host będzie działał poprawnie cały czas.

## 2. Pobranie i instalacja obrazu VulnVM w .ova
At the moment, we have temporarily disabled the download of a VM image. However, you can either refer to the next section or you may find and use a certain number of additional publically available VulnVMs under:
http://01.vuln.vms.perkmylife.com
http://02.vuln.vms.perkmylife.com
...
http://XY.vuln.vms.perkmylife.com

> @todo This section requires information on how to download the VulnVM image/snapshot from a public url and how to install the .ova image via VirutalBox or other tool

## 3. Samodzielne zbudowanie VulnVM
Tylko w przypadku gdy chciał(a)byś wykonać ćwiczenia na własnej lub samodzielnie zbudowanej maszynie bez pobierania obrazu VulnVM, możesz zainstalować Mutillidae i pozostałe aplikacje lokalnie.

W pierwszej kolejności, upewnij się, że masz zainstalowane repozytoria Dockera. Jeżeli nie to zamieszczam krótką instrukcję instalacji odpowiednich zależności dla systemu Debian i pochodnych.

#### Instalacja Docker'a
```sh
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

#### Instalacja Mutillidae lokalnie, jako kontener Docker - opcja rekomendowana
Na potrzeby konfiguracji lokalnej, rekomendujemy zainstalowanie Mutillidae jako kontener docker korzystając z następującej komendy:
```sh
sudo docker run -d -p 8080:80 -p 8443:443 --name owasp17 bltsec/mutillidae-docker
```
Jeśli wszystko poszło dobrze, powinieneś być w stanie zobaczyć witrynę działającą pod:
http://127.0.0.1:8080/mutillidae

Teraz powinieneś zobaczyć stronę główną podobną do:
![image info](https://miloserdov.org/wp-content/uploads/2018/06/06.jpg "mutillidae")

#### Instalacja Mutillidae lokalnie, z repozytorium git - tylko dla zaawansowanych użytkowników
Tylko jeśli jesteś bardziej zaawansowanym użytkownikiem i podczas szkolenia chciał(a)byś również analizować backend niektórych ćwiczeń (lub edytować kod backendowy), możesz skonfigurować Mutillidae lokalnie, ale jako repozytorium git.
W każdym razie, jeśli zdecydujesz się to zrobić, zalecamy najpierw uruchomienie go jako kontenera dockera i powrót do tej sekcji później w trakcie kursu.
```sh
sudo git clone https://github.com/webpwnized/mutillidae.git
cd mutillidae
sudo service apache2 start # lub sudo apt install apache2
sudo service mysql start # lub sudo apt install mysql
```
Następnie, konfiguracja bazy danych
```sh
sudo mysql -u root
use mysql;
# możesz zmienić hasło root lub stworzyć nowego użytkownika mutillidae i dodać mu stosowne uprawnienia
update user set authentication_string=PASSWORD('password') where user='root';
update user set plugin='mysql_native_password' where user='root';
flush privileges;
quit;
```
Teraz zmień dwa parametry in mutillidae/classes/MySQLHandler.php:
```sh
static public mMySQLDatabaseUsername = 'root'; # lub inny użytkownik mysql
static public mMySQLDatabasePassword = 'password'; # lub inne hasło mysql
```
I przejdź do: 
```sh
http://127.0.0.1/mutillidae/
```
Kliknij "setup/reset the DB".

#### Instalacja pozostałych aplikacji
Pozostałe aplikacje są dostępne z naszego repozytorium:
- public: https://gitlab.perk.ninja/public-repos/course_advanced_web_apps_security
- private: https://gitlab.perk.ninja/mrc/course_advanced_web_apps_security

```sh
git clone git@gitlab.perk.ninja:public-repos/course_advanced_web_apps_security.git
# or in case you have access to our private repo
# git clone git@gitlab.perk.ninja:mrc/course_advanced_web_apps_security.git
cd course_advanced_web_apps_security
ls docs  # materiały szkoleniowe wraz z wyjaśnieniami
ls src/vuln # kod źródłowy pozostałych aplikacji
```

You need to create VirtualHost configuration files and enable the apps running via either Apache http server or flask. You may consider enabling them at boot via systemctl

> @todo This section requires additionall installation procedure and testing/making sure that the steps executed locally are the same as the ones on the VulnVM

# Krótkie wprowadzenie do technologii client-side
## Co to jest HTTP?
HTTP czyli Hypertext Transfer Protocol jest bezstanowym protokołem wymiany informacji bazującym na requestach i responsach. Do swojej komunikacji korzysta z różnego rodzaju nagłówków, które są zbudowane w formie:
```sh
Nazwa_Nagłówka: Wartość_Nagłówka
# Na przykład:
Content-Type: application/json
```

## Bezstanowość HTTP
HTTP jest protokołem bezstanowym co oznacza, że każde kolejne żądanie ( request) jest niezależne od poprzedniego. W celu podtrzymania sesji np. na popularnych portalach korzysta się z ciasteczek zapisywanych na serwerze, które jednoznacznie powiązane są z konkretnym odbiorcą strony. W ten sposób można ominąć bezstanowość protokołu.

## Metody HTTP
- GET - Wszystkie informacje przesyłane są w formie nagłówków i w URL
- POST - Wszystkie bądź niektóre informacje pobierane są z ciała żądania
- PUT - Pozwala przesyłać pliki na serwer
- DELETE - Usuwa pliki z serwera
- HEAD - Zwraca tylko nagłówki żądania (metoda bardzo często wykorzystywana w skanowaniu plików/podstron na serwerze ze względu dużo mniejsze zużycie danych i tym samym przyspieszenie skanowania.

Więcej o metodach HTTP
https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods

## HTTPS
HTTPS jest odmianą HTTP z dopiskiem Secure. Ten protokół umożliwia wszystko co HTTP, ale informacje przesyła specjalnie zaszyfrowanym kanałem dzięki czemu nie ma możliwości odszyfrowania takiej komunikacji.
Więcej o HTTPS:
https://www.cloudflare.com/learning/ssl/what-is-https/

## Javascript
Javascript jest językiem programowania używanym bardzo często w stronach www. Odpowiada głównie za kwestie wizualne i interakcyjne. Taki kod jest dostępny dla każdego odwiedzającego stronę. Szczególnie istotną rolę odgrywa w takich atakach jak XSS czy CSRF.

## PHP
PHP jest językiem programowania, który wykonuje się po stronie serwera. Użytkownik nie jest w stanie zobaczyć kodu PHP. PHP najczęściej używa się do programowania różnego rodzaju mechanizmów stron takich jak logowanie.

## Inne technologie
Często strony tworzy się przy pomocy wyspecjalizowanych frameworków takich jak np. Symfony/Laravel (PHP), Ruby on Rails (Ruby) czy Django (Python), ASP.NET (C#), czy node.js (JS). Budowa tych stron pod kątem kodu może się trochę różnić, ale w ogólnym rozrachunku działają bardzo podobnie jak czyste PHP i Javascript.
Więcej można przeczytać o nich na ich stronach głównych:
- https://symfony.com/  `PHP`
- https://laravel.com/ `PHP`
- https://rubyonrails.org/ `Ruby`
- https://www.djangoproject.com/ `Python`
- https://dotnet.microsoft.com/apps/aspnet `C#`
- https://expressjs.com/ `Node.js`

W praktyce, lista stosowanych frameworków i języków programowania po stronie serwerów aplikacji www jest o wiele dłuższa.