# O kursie
Kurs został przeznaczony dla osób zaawansowanych w dziedzinie IT, które mają już podstawową wiedzę z bezpieczeństwa stron internetowych popartą praktyką ćwiczeń OWASP Top Ten. W kursie starałem poruszyć się wiele błędów, które nie wchodzą w skład OWASP Top Ten, ale nie zabrakło również niektórych podstawowych popularnych błędów z osławionego projektu Top Ten, które w tym kursie są prezentowane, w bardziej nietypowych formach.

Kurs ten składa się z dwóch modułów. Pierwszy - podstawowy, ma na celu omówienie popularnych problemów bezpieczeństwa www. Drugi moduł zawiera wybrane zaawansowane zagadnienia.

Po tym kursie uczestnik powinien zdawać sobie sprawę z większego spektrum zagrożeń niż dotychczas oraz możliwości ich wykorzystania.

# O instruktorze
Nazywam się Rafał Wójcicki. Od bardzo dawna zajmuję się sprzętem, a od kilku lat bezpieczeństwem informatycznym. Na co dzień pracuję w firmie EXATEL jako Pentester. Przed i po pracy realizuję się na różnych platformach takich jak Tryhackme i Hackthebox gdzie próbuję hakować specjalnie przygotowane maszyny. 
Tworzę oraz sprawdzam kursy z dziedziny cyberbezpieczeństwa, okazyjnie prowadzę webinary z hakowania.
