# Bug Bounty
Programy bug bounty to programy dzięki którym specjaliści od bezpieczeństwa mogą zgłaszać róznego rodzaju błędy jednoczesnie bedąc karanymi za szukanie podatności.
W skrócie "Spróbuj mnie shakować, a jak Ci się to uda, dostaniesz nagrodę".
Programy Bug bounty cieszą się coraz większą popularnością i nie bez powodu, niektórzy potrafi dzięki nim zarobić miliony dolarów. Praca nie jest normowana, nie ma wymaganych godzin pracy oraz ilości, dodatkowo do testowania wystarczy zwykły laptop lub komputer.
Najpopularniejszym platformami do programów Bug bounty są:
- HackerOne
- Bugcrowd

Obie platformy umożliwiają szybką wymianę formalnościami. Bug hunter ( osoba, która szuka błędów w programach Bug Bounty ) po znalezeniu błędu musi się skontakować z daną firmą. Ustalić szczegóły przejśc przez papierkową robotę i nierzadko się trochę namęczyć. Programy takie jak HackerOne czy Bugcrowd starają się zminimalizować formalności dla bug hunterów jednocześnie maksymalizujać skupienie się na szukaniu tylko podatności.
Nawet jeżeli nie bierzesz udziału w programach Bug bounty warto śledzić:
```sh
https://hackerone.com/hacktivity
```
który zawiera informacje o znalezionych podatnościach w naprawdę wielu aplikacjach. Można tam często przeczytać jak wykorzystać błąd. Jaką ma skalę CVE o ile ma, a także jaka była za niego nagroda.
Niestety programy Bug bounty mają też swoje wady. Przede wszystkim:
- Duplikaty - czasami się zdarza, że ktoś przed nami znalazł już te błędy co my. W takim wypadku tylko ona dostanie nagrodę. Mało tego czasami firmy nie naprawiają błędów, które są juz kilka miesięcy co znacznie zwiększa prawdopodbieństwo na znalezienie duplikatu
- Termin płatności - Niektóre firmy nie płacą w terminie i to jest bardzo często przypadłość
- Niskie nagrody - większość firm oferuje małe sumy za znalezione błędy. Tylko te największe firmy oferują naprawdę duże pieniądze
- Wymaga dużych umiejętności - niewątpliwe podczas testów trzeba się mierzyć nawet z tysiącami bug hunterów, którzy już sprawdzili daną stonę. To znacznie utrudnia szukanie nowych błędów

Mimo to warto uczestnić w programach Bug Bounty:
- Szukanie błędów często może się bardzo opłacić, niektórzy dostawali nawet do 100 tyś dolarów za błąd. Więc czasami jeden błąd może zastąpić wypłatę za cały rok
- W miarę doświadczenia można dostać się do prywatnych programów Bug Bounty, które są znacznie mniej esplotowane, a szansa na znalezienie błędów jest dużo większa
- Czasami nawet tam gdzie ktoś znalazł przed chwilą błąd może czaić się kolejny
- To świetna zabawa, która bardzo rozwija

Mówiąc o programach Bug Bounty warto wspomnieć o scopie czyli zakresie testów. Prawie nigdy nie jest tak, że firma udostępnia całą domenę do testowania w każdy możliwy sposób. Najczęsciej są pewne ograniczenia np do konkretnej subdomeny. Dodatkowo wiele programów Bug Bounty nie uznaje błędów typu DDoS, DoS, albo CSRF. Jeżeli ktoś wyjdzie poza scope może zaistnieć ryzyko konsekwencji prawnych.

Warto przyjrzeć się programowi Google VRP, który jest jednym z najpopularniejszych programów i może stanowić wzor dla innych firm, które chcą taki program uruchomić
```sh
https://bughunters.google.com/about/rules/6625378258649088
```
Jak widać Google nie uznaje wszystkich błędów oraz domen. Np błąd URL Riderection w przypadku Google nie jest błędem, bo wyszukiwarka standardowo przekierowuje użytkowników na różne strony.
