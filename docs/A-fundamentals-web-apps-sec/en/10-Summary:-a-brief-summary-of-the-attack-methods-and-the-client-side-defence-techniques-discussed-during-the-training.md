# Summary of attack and defense methods against client-side attacks
In this part, I will try to briefly summarize the attack and defense in the context of what has been conveyed in the course.

## Same-Origin Policy
In theory, Same-Origin Policy protects the website against various attacks, especially against XSS. In practice, due to such large restrictions, almost every website decides to loosen the security, which affects its security. Unfortunately, Same-Origin Policy is too inflexible a defense mechanism to be sufficient alone. In practice, the SOP is total basics that should be well implemented, but not forgetting other security bugs.

## XSS - the most important client-side vulnerability in the world
This is one of the most popular bugs. Practice shows that there are many methods to bypass security against this attack, and you need to apply multi-layer control - including CORS, SOP, CSP and WAF, so that you can do anything towards full application security. It is definitely worth focusing on this error and looking for it, for example, as part of the Bug Bounty program.

## Other client-side attacks
Often, attacks from this tab are ignored, and this is a basic mistake. All these attacks are very effective especially in attacks against specific targets. CSRF can do enormous damage if used correctly. On top of that, dangling markup can make many things easier in the context of bypassing CSRF. Clickjacking is a contemporary problem that is sometimes used to steal various data even today. JSON Hijacking is a relatively new topic, but practice has shown that it is a topic that is really worth taking care of. The RPO is quite specific, but sometimes makes sense to use it if possible.

## Problem with HTML5
These problems manifest in many ways. Bear in mind, that even new solutions like Websockets still contain old bugs. Apparently, CORS is not enough, so it makes sense to implement it, but as an add-on to enhance security, and as its main component.

## Content-Security Policy
The seemingly desirable flexibility of CSP has become the cause of frequent poor implementation, which promotes security bugs. The multitude of CSP options shows that you have to really check what directives and how you define them.

## Ways to enhance the front-end security
In this section, I have listed a few essential headers that are absolutely basic and should be your first and certainly not last steps toward your security.

## JS Libraries
This chapter shows very well that even these very famous libraries suffer from all sorts of security flaws and that unlimited trust in the developers of these programs can be a dead end.

## Bug Bounty
Finally, all the knowledge you have gained you can practice as part of the Bug Bounty and even make money on it.

## Debriefing Exercise
The XSS + CSRF exercise proved that very often in normal situations there are some filters, but even then the attacker, by combining various facts and vulnerabilities, is able to exploit the vulnerability anyway.