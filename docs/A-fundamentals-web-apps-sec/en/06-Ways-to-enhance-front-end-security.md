# Ways to increase the security of the front-end

## HTTP security headers
One of the basic security measures against various attacks that simultaneously require little effort is usually various HTTP headers, which can increase security.
Some of the most important headlines are:
```haskell
Content-Security-Policy
```
This header can significantly increase security against XSS attacks, but is quite difficult to implement properly.
```haskell
X-XSS-Protection
```
As the name suggests, it protects us against XSS, but only in theory. In practice, it filters many different dangerous XSS payloads, but it is better to additionally equip oneself with special libraries that will probably do it even better, such as `DOMPurify`.
```haskell
HTTP Strict Transport Security
```
Otherwise, HSTS is a very simple header that forces HTTPS connections instead of HTTP. Thanks to it, you can always be sure that the application uses an encrypted connection that cannot be decrypted.
```haskell
X-Frame-Options
```
It protects against clickjacking attacks - by not allowing display of inframes.
```haskell 
Expect-CT
```
In short, the header enforces secure certificate settings for TLS pages.
There are more headlines, and one of these lists is on this page:
```haskell
https://www.keycdn.com/blog/http-security-headers#2-x-xss-protection
```
HPKP or HTTP Public Key Pinning is a header responsible for associating a public key with a given domain. This is to prevent the possibility of spoofing an attacker using the method of spoofing public key certificates.
## Flags for cookies
In their standard form, cookies can be used in many different ways, such as downloading cookies using JavaScript. This topic deals with securing cookies against being read by unauthorized users.
The techniques discussed here are basic, which does not mean that they should not be used, quite the contrary. However, it should be remembered that this is only the foundation.
```sh
SameSite
```
Depending on the settings, cookies may be sent between the website they come from, or between certain specific websites. Thanks to this option, the attacker should not be able to send cookies to his server.
```sh
HTTPOnly
```
If this flag is present, the JavaScript code cannot retrieve the cookie from the browser. This significantly lowers the risk of using an XSS attack to hijack user sessions.
## Good advice for developers
I will try to specify some general pieces of advice that should be implemented by developers:
- Familiarize yourself with the Owasp Top ten
- If any function’s name contains danger or a similar adjective, it should be avoided
- You should implement security headers
- You should only force HTTPS connection
- Never trust a user
- Check many times over what specific data a user can send and implement techniques to defend against it
- Pay close attention if serialization mechanisms are used
- Watch out for minor bugs such as Clickjacking or CSRF and implement appropriate protection measures