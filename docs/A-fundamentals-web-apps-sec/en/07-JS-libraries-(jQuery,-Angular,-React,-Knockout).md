# JS libraries (jQuery, Angular, React, Knockout)
## JS libraries issues
Because these libraries use JavaScript, there are some security implications. First of all, it should be noted that they are very often extensive, which increases the amount of data that the user can enter in various places. This behavior can lead to security vulnerabilities like XSS. Very often, one of the ways to bypass CSP is the ability to inject XSS into an external JavaScript library that is vulnerable. Due to the fact that CSP allows you to load files of these libraries if an application uses them, the CSP protection is also bypassed.

# JS Template Injection
Template Injection is a known security bug, but in the case of the so-called Server-Side Template Injection. Few people realize that the injection into the template can be done on the client’s side. Then such an injection is due to taking advantage of errors on the client side, such as XSS.
You can consider example code in AngularJS:
```js
{{constructor.constructor('alert(1)')()}}
```
Here is an example of a simple injection into the templates mechanism, which inserts JS code into the page content.

## CVE related to JS libraries
As it is the case with any software or functionality, bugs will always exist, therefore the CVE bugs are inevitable, i.e. bugs that occur in some applications that deliver it to many users. If someone found a bug in library jQuery with XSS, then all pages using this version are vulnerable to such an attack ever since. Therefore, it is worth keeping track of CVE vulnerabilities. Let me try to give you an example of CVE errors in JS libraries that affects everyone using a vulnerable version of the library.
The vulnerability was discovered by one of the researchers as part of the Bug Bounty program, which will be discussed later in the course.
https://hackerone.com/reports/141463

This vulnerability concerns a very well-known and frequently used Angular JS library and allows for a Stored XSS attack. 
In order to use it, you need to create a doctor's account. Then visit the given subpage
http://example.com/messages/referrals/contacts/

Add a contact, and replace the address with the payload below:
```js
[[constructor.constructor('alert(1)')()]]
```
You can see a simple template injection here. From now on, every user displaying the page with contacts will be vulnerable, for example, to cookies theft. 
This clearly illustrates that even such well-known libraries are not always safe, and security should be guaranteed on many levels and effectively foresee and prevent such types of errors, e.g. by minimizing the risk with CSP.