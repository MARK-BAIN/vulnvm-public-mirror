# About the course 
The course is designed for advanced IT professionals with basic web security knowledge backed with practice on OWASP Top Ten. The material covers multiple errors that are not a part of  OWASP Top Ten, but there are a few less common examples of vulnerabilities from the famous Top Ten project. 

This training consists of 2 modules. 
A. Fundamentals of web apps’ client-side security - aims to cover the most popular web apps’ concerns.
B. Advanced web apps’ security - contains more advanced topics.

After the course, the participant should be familiar with a wider range of vulnerabilities and ways to exploit them.

# About the instructor 
My name is Zsombor Kovács. I am an IT Security Consultant & Instructor, Penetration Tester, with around 12 years of hands-on experience in practical security testing, with the main focus on application and infrastructure evaluation, conducting penetration tests on a daily basis.
### Certificates:
> OSCP, CEH, OSWP, CISSP, OSCE
