# HTML5 and API security

## postMessage()
The postMessage function allows you to send information, for example, between a website and a popup. Such functionality can lead to various data leaks which should not leak in the context of different frames. It is very often used to attack CORS, which will be discussed later in this course.
More about this feature can be found in the mozilla documentation:
https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage

## Cross-Origin Resource Sharing
CORS was created in response to overly restrictive SOP rules. First of all, Origin, i.e. a server that identifies itself in three aspects: The URL, port and protocol had some drawbacks. It quickly turned out that after the implementation of the SOP, no interactions are possible. Neither server A nor server B can download and share information with each other. This is a heavy restriction that, unfortunately, cannot function in the world of current websites that use many external resources, whether in the form of completely independent servers or even in the form of servers or subdomains. CORS is the answer to these problems because it allows you to properly loosen the SOP policy restrictions. Unfortunately, such loosening of the rules is associated with high responsibility, which, as it turns out, has been compromised more than once by huge websites. The CORS rules are primarily intended to protect against attacks by cross-domain scripts and the like, such as XSS and CSRF. Internet payments are a great example of the need to loosen the restrictions. Such payments must be very well secured. For this purpose, many websites use ready-made forms that are very well tested. If the SOP were strictly enforced, the exchange of information between the payment gateway and the service could not take place.

## CORS action
The CORS rules allow certain exceptions in communication so that the parties can function well while enforcing appropriate security
Due to the new functionality, CORS provides a number of headers, some of which are required:
```html
Origin: # Here is the origin of the page that wants to issue a query
```
```html
Access-Control-Allow-Origin: # You can specify either Origin that the party can contact, or "*" which means each party can exchange information. An asterisk from the safety point of view means the lack of any safety rules, including SOP.
```
## Service workers
The service workers was created in response to the demand to use websites when the Internet is down. Appcache is a similar mechanism, which allows you to define files that can be cached and thus available even in the absence of information. It is worth noting that Service Workers only work with an HTTPS connection. In order to use the Service Workers mechanism, you must first define it:
```js
if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/actions.js').then(function(r) {
    console.log('Good')
  }).catch(function(e) {
    console.log('Error! '+e)
  });
}
```
From now on, all fetch or install JavaScript actions will be logged to the actions.js file
Service workers have several functionalities.
```js
self.addEventListener('fetch', function(ev)
self.addEventListener('activate', function(ev)
```
and other. Fetch is triggered when any resources are downloaded, regardless of whether they are offline resources, i.e. already cached or online. Activate is called when the cache needs to be changed and replaced with a newer version.
The problem occurs when a resource that does not exist is retrieved. Regardless of its state, the fetch function will be performed with its body. If, for example, the user changes the response of the fetch function as a result of using an XSS error, any person entering an incorrect URl address will become a victim of a malicious response attack, e.g. XSS stealing session data.

## Web sockets
Web sockets is the answer to the statelessness of the HTTP protocol. First of all, each request is not related to each other, which makes it difficult to communicate in real time, e.g. as an Internet chat. Another problem is HTTP requests and responses. If the client does not send anything, the server will not even send an empty HTTP response. Which is an undesirable phenomenon, e.g. in instant messaging.
Web sockets is often used in instant messaging and other similarly based applications. First of all, asynchronicity is its advantage, i.e. it does not matter who sends the query or response, the communication is continuous, and not suspended, as in the case of HTTP. 
In order to establish communication with Web sockets, the so-called handshake is sent
``` haskell
GET /webchat HTTP/1.1
Host: example.com
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Key: asjfksanjfnsakf==
Sec-WebSocket-Protocol: chat
Sec-WebSocket-Version: 1337
```
An interesting header is the Sec-WebSocket-Key, which unfortunately is of little importance from the security point of view. First, despite the keyword "key", the value of the header has nothing to do with it, or at least in the popular sense. It is simply a Base64-encoded string (for correct HTTP reading) that is designed to avoid caching. If the key is different with each communication request, no one will have access to the other person’s cache. 
If the server supports Web sockets communication, it sends a response
``` haskell
HTTP/1.1 101 Switching Protocols
```
czyli przełączanie protokołów.
The main common mistake with this functionality is over-trusting the user. Changing the protocol can make you really astonished???, e.g .:
User sends me a message Hi, I'm John
``` json
{"message":"Hi, I'm John"}
```
Such a response is received by the server and displayed, for example, in the chat window:
```html
<h1>Hi, I'm John</h1>
```
If the server does not filter the data properly, you can easily inject XSS code
``` json
{"message":"<script>alert('XSS');</script>"}
```