# Before you begin, environment preparation
In this course, many exercises have been prepared, which are available within a specially created virtual machine __VulnVM__. It includes, among others proprietary small, vulnerable web applications, as well as a Docker container with an image of a fully functioning [OWASP Mutillidae](https://github.com/webpwnized/mutillidae) application.

In order to do the exercises, you can use any of the three options:
1. Accessing publically available VulnVM image
2. Downloading and installing the VulnVM .ova image
3. Building the VulnVM on your own

## 1. Accessing publically available VulnVM image
In the case you use our public IP address you don't have to do anything. The virtual machine is available under the following address:
- IP: `161.35.221.103` or 
- URL: http://vuln.vms.perkmylife.com

However, please note that since it hosts multiple very vulnerable web applications, we cannot guarantee it's going to be available and working smoothly all the time.

## 2. Downloading and installing the VulnVM .ova image
At the moment, we have temporarily disabled the download of a VM image. However, you can either refer to the next section or you may find and use a certain number of additional publically available VulnVMs under:
http://01.vuln.vms.perkmylife.com
http://02.vuln.vms.perkmylife.com
...
http://XY.vuln.vms.perkmylife.com

> @todo This section requires information on how to download the VulnVM image/snapshot from a public url and how to install the .ova image via VirutalBox or other tool

## 3. Building the VulnVM on your own
Only in the case you prefer to do exercises on your own or personally built machine without downloading and installing the VulnVM image, you can install Mutillidae and other apps locally.

Before you begin, first make sure that you have Docker installed. In the case you don't have it, below I enclose instructions on how to install it on Debian based systems.

#### Docker installation
```sh
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

#### Installing Mutillidae locally, as a Docker container - recommended option
For a local setup, we recommend to install Mutillidae as a docker container with the following command:
```sh
sudo docker run -d -p 8080:80 -p 8443:443 --name owasp17 bltsec/mutillidae-docker
```
If all went well, you should be able to see the site running under: 
http://127.0.0.1:8080/mutillidae

Now, you should see a homepage similar to:
![image info](https://miloserdov.org/wp-content/uploads/2018/06/06.jpg "mutillidae")

#### Installing Mutillidae locally, from a git repo - for advanced users only
Only if you are a more advanced user and during the training you'd like to also analyze the back-end code of some exercises (or play with the back-end code), you can configure the Mutillidae locally, but as a git repo.
In any case, if you decide to do so, we recommend running it as a docker container first and getting back to this section later on during the training.
```sh
sudo git clone https://github.com/webpwnized/mutillidae.git
cd mutillidae
sudo service apache2 start # or sudo apt install apache2
sudo service mysql start # or sudo apt install mysql
```
Next, setup database
```sh
sudo mysql -u root
use mysql;
# you can change root password or create a new mutillidae user with granted permissions
update user set authentication_string=PASSWORD('password') where user='root';
update user set plugin='mysql_native_password' where user='root';
flush privileges;
quit;
```
Now, change two strings in mutillidae/classes/MySQLHandler.php:
```sh
static public mMySQLDatabaseUsername = 'root'; # or other mysql user
static public mMySQLDatabasePassword = 'password'; # or other mysql password
```
And go to: 
```sh
http://127.0.0.1/mutillidae/
```
Click "setup/reset the DB".

#### Installing other applications
Other applications are available from our git repo:
- public: https://gitlab.perk.ninja/public-repos/course_advanced_web_apps_security
- private: https://gitlab.perk.ninja/mrc/course_advanced_web_apps_security

```sh
git clone git@gitlab.perk.ninja:public-repos/course_advanced_web_apps_security.git
# or in case you have access to our private repo
# git clone git@gitlab.perk.ninja:mrc/course_advanced_web_apps_security.git
cd course_advanced_web_apps_security
ls docs # the training materials including explanations
ls src/vuln # the source code of other applications
```

You need to create VirtualHost configuration files and enable the apps running via either Apache http server or flask. You may consider enabling them at boot via systemctl

> @todo This section requires additionall installation procedure and testing/making sure that the steps executed locally are the same as the ones on the VulnVM

# A short client-side technology introduction
## What is HTTP?
HTTP - Hypertext Transfer Protocol is a stateless data exchange protocol based on requests and responses. It uses various headers for communication in the following form:
```sh
Header_Name: Header_Value
#For example:
Content-Type: application/json
```

## Stateless HTTP
HTTP is a stateless protocol, which means that each request is independent from the previous one. In order to maintain session state, cookies are stored on a server and a client side to uniquely correlate two end points.

## Common HTTP Methods
- GET - All information is sent in headers and URL
- POST - Some or all data is downloaded from the body of a request
- PUT - Used to upload files on a server
- DELETE - Deletes files from a server
- HEAD - Returns only the headers of a request. It is often used to speed up the scanning phase due to lower network footprint.

More information about HTTP methods can be found here:
https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods

## HTTPS
HTTPS is a Secured form of HTTP. It has the same capabilities, but it also creates an encrypted communication tunnel. It prevents sniffing and some other basic attempts to steal user data.
More about HTTPS:
https://www.cloudflare.com/learning/ssl/what-is-https/

## Javascript
Javascript is a programming language that is interpreted by a browser, and it is used on all modern websites. It is used to modify the content of the website that is being displayed by the browser engine. It can be viewed by anyone with inspect function on all modern browsers. Its misuse often introduces XSS and CSRF vulnerabilities.

## PHP
PHP is a programming language executed by a server, thus the user cannot see the underlying PHP code. It can be used to create login forms, connect to databases or execute commands on the server.

## Other technologies
Most of the time, specialised frameworks are used to create websites. Some of them are:
- https://symfony.com/  `PHP`
- https://laravel.com/ `PHP`
- https://rubyonrails.org/ `Ruby`
- https://www.djangoproject.com/ `Python`
- https://dotnet.microsoft.com/apps/aspnet `C#`
- https://expressjs.com/ `Node.js`

The code varies depending on the language used, but they all produce HTML and JavaScript code adhering to the same principles.
If you can think of a programming language, most likely there are web development frameworks written in it.