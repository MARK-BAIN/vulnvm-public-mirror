# Bug Bounty

Bug bounties programmes are open deals made by organizations that offer compensation for sharing detailed information on discovered bugs, with a strong emphasis on those that introduce vulnerabilities.
In other words, "try to hack me, and get a reward if you manage to do so".

Bug bounty programmes are not gaining popularity without a reason. Some people made millions of dollars this way. This job is not standardised and offers loads of freedom in terms of work hours and most of the time, it also can be done with average hardware.
The most popular platforms for Bug bounty programmes are:
- HackerOne
- Bugcrowd

Both platforms allow a faster approach to paperwork. Normally, once a bug is found, the hunter has to contact the company and go through tedious formalities before receiving a reward. Programmes such as HackerOne and Bugcrowd are speeding up the non-technical part of the process and thus allowing to focus on the job.
Even if you don't take a part in such programmes, it is nice to follow posted bug reports:
```sh
https://hackerone.com/hacktivity
```
This URL contains information about vulnerabilities that have been found in multiple applications. It often contains a proof of concept, CVE level and the bounty.
Unfortunately, Bug bounty programmes have their cons, most importantly:
- Duplicates - sometimes, someone else posts a report about the same bug that we found and in that case, whoever was first to post the report will get paid. Furthermore, companies might not fix some issues for months and it increases the likelihood of a duplicate bug report.
- Payment time - companies often don't pay on time
- Low rewards - most companies offer small rewards. Only the biggest companies like FAANG offer big rewards.
- High skill cap - since anyone can participate in those programmes, we have to compete with thousands of bug hunters that may have already tested the same piece of software, thus making it much more challenging.

Regardless, here are some reasons why it is worth trying:
- Some of the rewards are very high, for example [$1.000.000 for a serious iOS vulnerabilities](https://www.forbes.com/sites/thomasbrewster/2019/08/08/apple-confirms-1-million-reward-for-hackers-who-find-serious-iphone-vulnerabilities/), but rewards around $50k-$100k are more common. Which is more than the average annual salary in any developed country.
- Once we gain some experience, we might get invited to private programmes, where a chance to be the first to find a bug is much higher.
- Sometimes, even if somebody finds a bug, there might be another one right next to it.
- It is great fun that provides loads of experience

It is worth mentioning the scope of these programmes. Companies almost never provide the entire domain for testing in every possible way. Most of the time it is limited to a subdomain and attacks like DoS or CSRF are excluded. If the scope of the test is exceeded, there is a risk of legal consequences.

Google VRP is worth taking a look at and it might be a good reference for companies that would like to start their own programmes.
```sh
https://bughunters.google.com/about/rules/6625378258649088
```
As you can see, Google does not allow bugs and domains to be tested. For example, URL Redirection is not a bug in this case, because the search engine redirects users to various websites by default.
